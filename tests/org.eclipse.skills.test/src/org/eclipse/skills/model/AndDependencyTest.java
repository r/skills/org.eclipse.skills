/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.model;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AndDependencyTest {

	private IAndDependency fDependency;

	@BeforeEach
	public void setupFixture() {
		fDependency = ISkillsFactory.eINSTANCE.createAndDependency();
	}

	@Test
	public void emptyDependencyIsFulfilled() {
		fDependency.activate();

		assertTrue(fDependency.isFulfilled());
	}
}
