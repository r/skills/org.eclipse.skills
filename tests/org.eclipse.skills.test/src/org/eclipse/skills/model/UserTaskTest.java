/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class UserTaskTest {

	private IUserTask fUserTask;

	@BeforeEach
	public void setupFixture() {
		fUserTask = ISkillsFactory.eINSTANCE.createUserTask();

		final ITask task = ISkillsFactory.eINSTANCE.createTask();
		task.setGoal(ISkillsFactory.eINSTANCE.createAndDependency());
		fUserTask.setTask(task);
	}

	@Test
	@DisplayName("create a UserTask")
	public void createUserTask() {
		assertNotNull(fUserTask);

		assertNotNull(fUserTask.getTask());
	}

	@Test
	@DisplayName("reveal next hint does nothing for a task without hints")
	public void revealNextHintWithoutAvailableHints() {
		assertEquals(0, fUserTask.getHintsDisplayed());
		fUserTask.revealNextHint();
		assertEquals(0, fUserTask.getHintsDisplayed());
	}

	@Test
	@DisplayName("reveal next hint for a task with an available hint")
	public void revealNextHintWith1AvailableHint() {
		fUserTask.getTask().getHints().add(ISkillsFactory.eINSTANCE.createHint());

		assertEquals(0, fUserTask.getHintsDisplayed());
		fUserTask.revealNextHint();
		assertEquals(1, fUserTask.getHintsDisplayed());
	}

	@Test
	@DisplayName("reveal last hint increments hints count for a task with multiple hints")
	public void revealLastHint() {
		fUserTask.getTask().getHints().add(ISkillsFactory.eINSTANCE.createHint());
		fUserTask.getTask().getHints().add(ISkillsFactory.eINSTANCE.createHint());
		fUserTask.getTask().getHints().add(ISkillsFactory.eINSTANCE.createHint());

		assertEquals(0, fUserTask.getHintsDisplayed());

		fUserTask.revealNextHint();
		assertEquals(1, fUserTask.getHintsDisplayed());

		fUserTask.revealNextHint();
		assertEquals(2, fUserTask.getHintsDisplayed());

		fUserTask.revealNextHint();
		assertEquals(3, fUserTask.getHintsDisplayed());
	}

	@Test
	@DisplayName("reveal next hint does nothing when all hints are shown")
	public void revealNextHintWhenAllHintsAreShown() {
		fUserTask.getTask().getHints().add(ISkillsFactory.eINSTANCE.createHint());
		fUserTask.getTask().getHints().add(ISkillsFactory.eINSTANCE.createHint());
		fUserTask.getTask().getHints().add(ISkillsFactory.eINSTANCE.createHint());

		fUserTask.setHintsDisplayed(3);

		assertEquals(3, fUserTask.getHintsDisplayed());
		fUserTask.revealNextHint();
		assertEquals(3, fUserTask.getHintsDisplayed());
	}

	@Test
	@DisplayName("open task is not started, and not completed")
	public void openTaskIsInReadyState() {
		assertFalse(fUserTask.isStarted());
		assertFalse(fUserTask.isCompleted());
	}

	@Test
	@DisplayName("started task is marked started, but not completed")
	public void startedTaskIsInRunningState() {
		fUserTask.setStarted(new Date());
		assertTrue(fUserTask.isStarted());
		assertFalse(fUserTask.isCompleted());
	}

	@Test
	@DisplayName("finshed task is marked as completed")
	public void finishedTaskIsCompleted() {
		fUserTask.setStarted(new Date());
		fUserTask.setFinished(new Date());
		assertTrue(fUserTask.isStarted());
		assertTrue(fUserTask.isCompleted());
	}

	@Test
	@DisplayName("activate usertask does not set started flag")
	public void activateDoesNothingWhenNotStarted() {
		assertFalse(fUserTask.isStarted());

		fUserTask.activate();

		assertFalse(fUserTask.isStarted());
	}

	@Test
	@DisplayName("activate usertask detects changes on goal when task is started")
	public void detectGoalChangesAfterActivation() {
		fUserTask.getTask().setGoal(ISkillsFactory.eINSTANCE.createAndDependency());

		fUserTask.setStarted(new Date());
		assertFalse(fUserTask.isCompleted());

		fUserTask.activate();
		assertTrue(fUserTask.isCompleted());
	}

	@Test
	@DisplayName("activate usertask ignores changes on goal when task is not started")
	public void detectNoGoalChangesAfterActivationWehnNotStarted() {
		fUserTask.getTask().setGoal(ISkillsFactory.eINSTANCE.createAndDependency());

		fUserTask.activate();
		assertFalse(fUserTask.isCompleted());
	}
}
