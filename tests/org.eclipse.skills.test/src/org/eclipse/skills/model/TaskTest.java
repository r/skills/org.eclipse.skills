/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.skills.BrokerTools;
import org.eclipse.skills.service.ISkillService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TaskTest {

	private ITask fTask;
	private ISkillService fSkillService;

	@BeforeEach
	public void setupFixture() {
		fTask = ISkillsFactory.eINSTANCE.createTask();
		fSkillService = mock(ISkillService.class);
	}

	@Test
	public void createTask() {
		assertNotNull(fTask);
	}

	@Test
	@DisplayName("Send broker event when requirement is met")
	public void sendEventWhenRequirementsAreMet() {
		final IEventBroker broker = mock(IEventBroker.class);
		BrokerTools.setCustomBroker(broker);

		final IAndDependency requirement = ISkillsFactory.eINSTANCE.createAndDependency();

		fTask.setSkillService(fSkillService);
		fTask.setRequirement(requirement);
		requirement.activate();

		verify(broker, times(1)).post(ISkillService.EVENT_TASK_READY, fTask);
	}

	@Test
	@DisplayName("getId() for task without quest")
	public void getIdForTaskWithoutQuest() {
		fTask.setTitle("Simple task");

		assertEquals("_Simple_task", fTask.getId());
	}

	@Test
	@DisplayName("getId() with special characters")
	public void getIdWithSpecialCharacters() {
		fTask.setTitle("Special stuff: !§%&$/");

		assertEquals("_Special_stuff________", fTask.getId());
	}

	@Test
	@DisplayName("getId() for task within quest")
	public void getIdForTaskWithinQuest() {
		final IQuest quest = ISkillsFactory.eINSTANCE.createQuest();
		quest.setTitle("My quest");
		quest.getTasks().add(fTask);

		fTask.setTitle("Simple task");

		assertEquals("My_quest_Simple_task", fTask.getId());
	}

	@Test
	@DisplayName("getId() for subtask within quest")
	public void getIdForSubtaskWithinQuest() {
		final IQuest quest = ISkillsFactory.eINSTANCE.createQuest();
		quest.setTitle("My quest");

		final ITask mainTask = ISkillsFactory.eINSTANCE.createTask();
		mainTask.setTitle("Main Task");
		mainTask.getTasks().add(fTask);

		quest.getTasks().add(mainTask);

		fTask.setTitle("Simple task");

		assertEquals("My_quest_Simple_task", fTask.getId());
	}
}
