/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.eclipse.jface.resource.ImageDescriptor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SkillTest {
	private ISkill fSkill;

	@BeforeEach
	public void setupFixture() {
		fSkill = ISkillsFactory.eINSTANCE.createSkill();
	}

	@Test
	@DisplayName("Default skill can be created")
	public void createSkill() {
		assertNotNull(fSkill);
	}

	@Test
	@DisplayName("getImageDescriptor() == null when imageLocation == null")
	public void noImageDescriptorWhenLocationIsNull() {
		fSkill.setImageURI(null);
		assertNull(fSkill.getImageDescriptor());
	}

	@Test
	@DisplayName("getImageDescriptor() == null when imageLocation is empty")
	public void noImageDescriptorWhenLocationIsEmpty() {
		fSkill.setImageURI("");
		assertNull(fSkill.getImageDescriptor());
	}

	@Test
	@DisplayName("getImageDescriptor() == missingDescriptor when imageLocation is invalid")
	public void missingImageDescriptorWhenLocationIsInvalid() {
		fSkill.setImageURI("not there");
		assertEquals(ImageDescriptor.getMissingImageDescriptor(), fSkill.getImageDescriptor());
	}

	@Test
	@DisplayName("getImageDescriptor() == valid when imageLocation is valid")
	public void getImageDescriptorWhenLocationIsValid() {
		fSkill.setImageURI("platform:/plugin/org.eclipse.skills.test/icons/test_skill_image.png");

		assertNotNull(fSkill.getImageDescriptor());
		assertNotEquals(ImageDescriptor.getMissingImageDescriptor(), fSkill.getImageDescriptor());
	}
}
