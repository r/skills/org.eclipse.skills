/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.model;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SequenceDependencyTest {

	private ISequenceDependency fDependency;

	@BeforeEach
	public void setupFixture() {
		fDependency = ISkillsFactory.eINSTANCE.createSequenceDependency();
	}

	@Test
	@DisplayName("Empty dependency is fulfilled")
	public void emptyDependencyIsFulfilled() {
		fDependency.activate();

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Not fulfilled child dependency")
	public void containsNotFulfilledDependency() {

		fDependency.getDependencies().add(ISkillsFactory.eINSTANCE.createNotDependency());
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Fulfilled child dependency")
	public void containsFulfilledDependency() {
		fDependency.getDependencies().add(ISkillsFactory.eINSTANCE.createAndDependency());
		fDependency.activate();

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Child dependency changes fulfill state after activation")
	public void childDependencyFulfillsAfterActivation() {

		final DummyDependency child = new DummyDependency();
		fDependency.getDependencies().add(child);
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		child.setFulfilled(true);
		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Child dependencies fulfill in correct sequence")
	public void childDependenciesFulfillInCorrectSequence() {

		final DummyDependency child1 = new DummyDependency();
		final DummyDependency child2 = new DummyDependency();
		fDependency.getDependencies().add(child1);
		fDependency.getDependencies().add(child2);
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		child1.setFulfilled(true);
		assertFalse(fDependency.isFulfilled());

		child2.setFulfilled(true);
		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Child dependencies fulfill in inverse sequence")
	public void childDependenciesFulfillInInverseSequence() {

		final DummyDependency child1 = new DummyDependency();
		final DummyDependency child2 = new DummyDependency();
		fDependency.getDependencies().add(child1);
		fDependency.getDependencies().add(child2);
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		child2.setFulfilled(true);
		assertFalse(fDependency.isFulfilled());

		child1.setFulfilled(true);
		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Child dependencies toggle")
	public void childDependenciesToggle() {

		final DummyDependency child1 = new DummyDependency();
		final DummyDependency child2 = new DummyDependency();
		fDependency.getDependencies().add(child1);
		fDependency.getDependencies().add(child2);
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		child1.setFulfilled(true);
		assertFalse(fDependency.isFulfilled());

		child1.setFulfilled(false);
		child2.setFulfilled(true);
		assertTrue(fDependency.isFulfilled());
	}
}
