/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.model;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class NotDependencyTest {
	private INotDependency fDependency;

	@BeforeEach
	public void setupFixture() {
		fDependency = ISkillsFactory.eINSTANCE.createNotDependency();
	}

	@Test
	@DisplayName("Empty dependency evaluates to false")
	public void emptyDependencyEvaluatesToFalse() {
		assertFalse(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Empty dependency can be activated and evaluates to false")
	public void emptyDependencyCanBeActivated() {
		fDependency.activate();
		assertFalse(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Is fulfilled when child is not fulfilled")
	public void fulfilledWhenChildIsNotFulfilled() {
		final DummyDependency child = new DummyDependency();

		fDependency.getDependencies().add(child);
		fDependency.activate();
		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Is not fulfilled when child is fulfilled")
	public void notFulfilledWhenChildIsFulfilled() {
		final DummyDependency child = new DummyDependency();
		child.setFulfilled(true);

		fDependency.getDependencies().add(child);
		fDependency.activate();
		assertFalse(fDependency.isFulfilled());
	}
}
