/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TaskDependencyTest {

	private ITaskDependency fDependency;

	@BeforeEach
	public void setupFixture() {
		fDependency = ISkillsFactory.eINSTANCE.createTaskDependency();
	}

	@Test
	@DisplayName("getUser() uses custom user")
	public void getUserUsesCustomUser() {
		final IUser user = ISkillsFactory.eINSTANCE.createUser();

		fDependency.setUser(user);
		assertEquals(user, fDependency.getUser());
	}

	@Test
	@DisplayName("getUser() has a default value")
	public void getUserHasADefaultValue() {
		assertNotNull(fDependency.getUser());
	}

	@Test
	@DisplayName("Without task dependency cannot be fulfilled")
	public void emptyDependencyIsNotFulfilled() {
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Is not fulfilled when waiting for completion of open task")
	public void notFulfilledWhenWaitingForOpenTask() {
		final ITask task = ISkillsFactory.eINSTANCE.createTask();

		fDependency.setTask(task);
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Is not fulfilled when waiting for completion of scheduled task")
	public void notFulfilledWhenWaitingForScheduledTask() {
		final ITask task = ISkillsFactory.eINSTANCE.createTask();

		final IUser user = ISkillsFactory.eINSTANCE.createUser();
		user.addTask(task);

		fDependency.setUser(user);
		fDependency.setTask(task);
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Is not fulfilled when waiting for completion of running task")
	public void notFulfilledWhenWaitingForRunningTask() {
		final ITask task = ISkillsFactory.eINSTANCE.createTask();
		task.setGoal(ISkillsFactory.eINSTANCE.createNotDependency());

		final IUser user = ISkillsFactory.eINSTANCE.createUser();
		final IUserTask userTask = user.addTask(task);
		userTask.setStarted(new Date());

		fDependency.setUser(user);
		fDependency.setTask(task);
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Is fulfilled when waiting for completion of completed task")
	public void fulfilledWhenWaitingForCompletedTask() {
		final ITask task = ISkillsFactory.eINSTANCE.createTask();

		final IUser user = ISkillsFactory.eINSTANCE.createUser();
		final IUserTask userTask = user.addTask(task);
		userTask.setFinished(new Date());

		fDependency.setUser(user);
		fDependency.setTask(task);
		fDependency.activate();

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Is fulfilled when waiting for completion of completing task")
	public void fulfilledWhenWaitingForCompletingTask() {
		final ITask task = ISkillsFactory.eINSTANCE.createTask();

		final IUser user = ISkillsFactory.eINSTANCE.createUser();
		final IUserTask userTask = user.addTask(task);

		fDependency.setUser(user);
		fDependency.setTask(task);
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		userTask.setFinished(new Date());
		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Activate dependency before task is started")
	public void activateDependencyBeforeTask() {
		final ITask task = ISkillsFactory.eINSTANCE.createTask();

		final IUser user = ISkillsFactory.eINSTANCE.createUser();

		fDependency.setUser(user);
		fDependency.setTask(task);
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		final IUserTask userTask = user.addTask(task);
		assertFalse(fDependency.isFulfilled());

		userTask.setStarted(new Date());
		assertFalse(fDependency.isFulfilled());

		userTask.setFinished(new Date());
		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Is not fulfilled when waiting for start of non-scheduled task")
	public void notFulfilledWhenWaitingForNewTask() {
		final ITask task = ISkillsFactory.eINSTANCE.createTask();

		final IUser user = ISkillsFactory.eINSTANCE.createUser();

		fDependency.setUser(user);
		fDependency.setTask(task);
		fDependency.setCompleted(false);
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Is not fulfilled when waiting for start of open task")
	public void notFulfilledWhenWaitingForStartOfOpenTask() {
		final ITask task = ISkillsFactory.eINSTANCE.createTask();

		final IUser user = ISkillsFactory.eINSTANCE.createUser();
		user.addTask(task);

		fDependency.setUser(user);
		fDependency.setTask(task);
		fDependency.setCompleted(false);
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Is fulfilled when waiting for running task")
	public void fulfilledWhenWaitingForRunningTask() {
		final ITask task = ISkillsFactory.eINSTANCE.createTask();

		final IUser user = ISkillsFactory.eINSTANCE.createUser();
		final IUserTask userTask = user.addTask(task);
		userTask.setStarted(new Date());

		fDependency.setUser(user);
		fDependency.setTask(task);
		fDependency.setCompleted(false);
		fDependency.activate();

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Is fulfilled when waiting for task activation")
	public void fulfilledWhenWaitingForTaskActivation() {
		final ITask task = ISkillsFactory.eINSTANCE.createTask();

		final IUser user = ISkillsFactory.eINSTANCE.createUser();

		fDependency.setUser(user);
		fDependency.setTask(task);
		fDependency.setCompleted(false);
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		final IUserTask userTask = user.addTask(task);
		assertFalse(fDependency.isFulfilled());

		userTask.setStarted(new Date());
		assertTrue(fDependency.isFulfilled());
	}
}
