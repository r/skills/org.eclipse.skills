/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class UserTest {

	private IUser fUser;

	@BeforeEach
	public void setupFixture() {
		fUser = ISkillsFactory.eINSTANCE.createUser();
	}

	@Test
	@DisplayName("New user does not have any assigned usertasks")
	public void createUser() {
		assertNotNull(fUser);
		assertTrue(fUser.getUsertasks().isEmpty());
	}

	@Test
	@DisplayName("Create a new user task from a given task")
	public void createUserTask() {
		final ITask task = ISkillsFactory.eINSTANCE.createTask();

		final IUserTask userTask = fUser.addTask(task);

		assertNotNull(userTask);
		assertEquals(task, userTask.getTask());

		assertEquals(1, fUser.getUsertasks().size());
		assertTrue(fUser.getUsertasks().contains(userTask));
	}

	@Test
	@DisplayName("getSkill() returns null if skill is not available")
	public void findNonExistingSkillByName() {
		assertNull(fUser.getSkill("not there"));
	}

	@Test
	@DisplayName("getSkill() returns skill if skill is available")
	public void findExistingSkillByName() {
		final ISkill skill = ISkillsFactory.eINSTANCE.createSkill();
		skill.setName("Test Skill");
		fUser.getSkills().add(skill);

		assertNotNull(fUser.getSkill("Test Skill"));
		assertEquals("Test Skill", fUser.getSkill("Test Skill").getName());
	}
}
