/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class StringReplacerTest {

	@Test
	@DisplayName("return null when data == null")
	public void replaceOnNullReturnsNull() {
		assertNull(StringReplacer.replace(null, null));
	}

	@Test
	@DisplayName("return simple data when replacement == null")
	public void returnDataWhenReplacementIsNull() {
		assertEquals("input", StringReplacer.replace("input", null));
	}

	@Test
	@DisplayName("trim result from whitespace")
	public void trimWhitespace() {
		assertEquals("input", StringReplacer.replace("  input\t", null));
	}

	@Test
	@DisplayName("strip wildcards from data when replacement == null")
	public void returnDataWithoutWildcardsWhenReplacementIsNull() {
		assertEquals("input", StringReplacer.replace("input${wildcard}", null));
	}

	@Test
	@DisplayName("replace wildcards when keys exist")
	public void replaceWildcardswithExistingKeys() {
		final Map<String, String> replacements = new HashMap<>();
		replacements.put("user", "John");
		replacements.put("city", "Graz");

		assertEquals("Greetings John from Graz", StringReplacer.replace("Greetings ${user} from ${city}", replacements));
	}

	@Test
	@DisplayName("replace wildcards when some keys exist")
	public void replaceWildcardswithPartiallyExistingKeys() {
		final Map<String, String> replacements = new HashMap<>();
		replacements.put("user", "John");

		assertEquals("Greetings John from", StringReplacer.replace("Greetings ${user} from ${city}", replacements));
	}

	@Test
	@DisplayName("replace wildcards containing regex")
	public void replaceWildcardsContainingRegex() {
		final Map<String, String> replacements = new HashMap<>();
		replacements.put("user.name", "John");

		assertEquals("Greetings John /", StringReplacer.replace("Greetings ${user.name} / ${userXname}", replacements));
	}
}
