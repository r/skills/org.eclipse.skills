/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.eclipse.skills.model.IQuest;
import org.eclipse.skills.model.ISkill;
import org.eclipse.skills.model.ISkillsFactory;
import org.eclipse.skills.model.IUser;
import org.eclipse.skills.model.IUserTitle;
import org.eclipse.skills.service.questprovider.IQuestProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class UserTitleGeneratorTest {

	private IUser fUser;
	private QuestProviderMock fQuestProvider;
	private UserTitleGenerator fTitleGenerator;

	@BeforeEach
	public void setup() {
		fUser = new UserFactory().createUser();
		fQuestProvider = new QuestProviderMock();

		fTitleGenerator = new UserTitleGenerator(fUser, fQuestProvider);
	}

	@Test
	@DisplayName("throw IllegalArgumentexception when user is null")
	public void throwIfUserIsNull() {
		assertThrows(IllegalArgumentException.class, () -> new UserTitleGenerator(null, fQuestProvider));
	}

	@Test
	@DisplayName("throw IllegalArgumentexception when questProvider is null")
	public void throwIfQuestProviderIsNull() {
		assertThrows(IllegalArgumentException.class, () -> new UserTitleGenerator(fUser, null));
	}

	@Test
	@DisplayName("empty string  is returned when no titles are defined")
	public void createDefaultUserTitle() {
		assertEquals("", fTitleGenerator.createUserTitle());
	}

	@Test
	@DisplayName("skill name is replaced in title string")
	public void replaceSkillName() {
		addTitle("${skill.name} expert", 0, "mySkill");

		final ISkill skill = ISkillsFactory.eINSTANCE.createSkill();
		skill.setName("mySkill");
		fUser.getSkills().add(skill);

		assertEquals("mySkill expert", fTitleGenerator.createUserTitle());
	}

	@Test
	@DisplayName("skill name is not replaced when no skill is defined")
	public void discardSkillName() {
		addTitle("${skill.name} expert", 0, null);

		assertEquals("expert", fTitleGenerator.createUserTitle());
	}

	@Test
	@DisplayName("select title for highest matching skill (level = 1)")
	public void selectTitleForBestMatchingSkillA() {
		addTitle("Beginner", 0, null);
		addTitle("Intermediate", 2, null);
		addTitle("Advanced", 3, null);
		addTitle("Expert", 4, null);

		final int xpNeeded = fUser.getExperience().getProgression().getMinimumXpForLevel(2);
		fUser.getExperience().setExperience(xpNeeded - 1);

		assertEquals("Beginner", fTitleGenerator.createUserTitle());
	}

	@Test
	@DisplayName("select title for highest matching skill (level = 2)")
	public void selectTitleForBestMatchingSkillB() {
		addTitle("Beginner", 0, null);
		addTitle("Intermediate", 2, null);
		addTitle("Advanced", 3, null);
		addTitle("Expert", 4, null);

		final int xpNeeded = fUser.getExperience().getProgression().getMinimumXpForLevel(2);
		fUser.getExperience().setExperience(xpNeeded);

		assertEquals("Intermediate", fTitleGenerator.createUserTitle());
	}

	@Test
	@DisplayName("select title for highest matching skill (level = 4)")
	public void selectTitleForBestMatchingSkillC() {
		addTitle("Beginner", 0, null);
		addTitle("Intermediate", 2, null);
		addTitle("Advanced", 3, null);
		addTitle("Expert", 4, null);

		final int xpNeeded = fUser.getExperience().getProgression().getMinimumXpForLevel(4);
		fUser.getExperience().setExperience(xpNeeded);

		assertEquals("Expert", fTitleGenerator.createUserTitle());
	}

	private void addTitle(String displayString, int minLevel, String skillName) {

		final IQuest quest = fQuestProvider.getQuests().iterator().next();

		ISkill skill = null;
		if (skillName != null) {
			skill = ISkillsFactory.eINSTANCE.createSkill();
			skill.setName(skillName);

			quest.getSkills().add(skill);
		}

		final IUserTitle title = ISkillsFactory.eINSTANCE.createUserTitle();
		title.setDisplayString(displayString);
		title.setMinLevel(minLevel);
		title.setSkill(skill);

		quest.getUserTitles().add(title);
	}

	private class QuestProviderMock implements IQuestProvider {

		private final List<IQuest> fQuests;

		public QuestProviderMock() {
			fQuests = Arrays.asList(ISkillsFactory.eINSTANCE.createQuest());
		}

		@Override
		public Collection<IQuest> getQuests() {
			return fQuests;
		}
	}
}
