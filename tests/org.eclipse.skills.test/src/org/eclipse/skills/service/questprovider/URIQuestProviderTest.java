/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service.questprovider;

import static org.junit.jupiter.api.Assertions.assertFalse;

import org.eclipse.emf.common.util.URI;
import org.eclipse.skills.model.IQuest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class URIQuestProviderTest {

	@Test
	@DisplayName("Read quests from URI")
	public void readQuestsFromURIString() {
		final IQuestProvider provider = new URIQuestProvider("platform:/plugin/org.eclipse.skills.test/resources/URIQuestProviderTest.skills");

		assertFalse(provider.getQuests().isEmpty());

		final IQuest quest = provider.getQuests().iterator().next();
		assertFalse(quest.getSkills().isEmpty());
		assertFalse(quest.getTasks().isEmpty());
		assertFalse(quest.getUserTitles().isEmpty());
	}

	@Test
	@DisplayName("Read quests from URI")
	public void readQuestsFromURI() {
		final IQuestProvider provider = new URIQuestProvider(URI.createURI("platform:/plugin/org.eclipse.skills.test/resources/URIQuestProviderTest.skills"));

		assertFalse(provider.getQuests().isEmpty());

		final IQuest quest = provider.getQuests().iterator().next();
		assertFalse(quest.getSkills().isEmpty());
		assertFalse(quest.getTasks().isEmpty());
		assertFalse(quest.getUserTitles().isEmpty());
	}
}
