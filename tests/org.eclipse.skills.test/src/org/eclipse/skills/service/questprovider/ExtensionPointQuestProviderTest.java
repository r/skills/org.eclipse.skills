/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service.questprovider;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.eclipse.skills.model.IQuest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ExtensionPointQuestProviderTest {

	private static IQuest getTestQuest(final IQuestProvider provider) {
		for (final IQuest quest : provider.getQuests()) {
			if ("UnitTest Quest".equals(quest.getTitle()))
				return quest;
		}

		throw new RuntimeException("Test quest not found");
	}

	@Test
	@DisplayName("Read quests from extension point")
	public void readQuestsFromExtensionPoint() {
		final ExtensionPointQuestProvider provider = new ExtensionPointQuestProvider();

		// we only know that at minimum our test quest is available via extension point
		assertFalse(provider.getQuests().isEmpty());
	}

	@Test
	@DisplayName("Read tasks from extension point")
	public void readTasksFromExtensionPoint() {
		final ExtensionPointQuestProvider provider = new ExtensionPointQuestProvider();

		final IQuest quest = getTestQuest(provider);
		assertEquals(2, quest.getTasks().size());
	}
}
