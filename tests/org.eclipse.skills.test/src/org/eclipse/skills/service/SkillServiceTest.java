/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import org.eclipse.skills.model.IExperienceReward;
import org.eclipse.skills.model.INotDependency;
import org.eclipse.skills.model.IQuest;
import org.eclipse.skills.model.ISkillsFactory;
import org.eclipse.skills.model.ITask;
import org.eclipse.skills.model.IUser;
import org.eclipse.skills.model.IUserTask;
import org.eclipse.skills.service.questprovider.IQuestProvider;
import org.eclipse.skills.service.questprovider.URIQuestProvider;
import org.eclipse.skills.service.storage.IDataStorage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SkillServiceTest {

	private static final String HIERARCHICAL_TASKS_CONFIG = "platform:/plugin/org.eclipse.skills.test/resources/SkillServiceTest_HierarchicalTasks.skills";
	private static final String REWARDS_CONFIG = "platform:/plugin/org.eclipse.skills.test/resources/SkillServiceTest_Rewards.skills";

	private ISkillService fService;

	@BeforeEach
	public void setup() {
		try {
			fService = SkillService.getInstance();
			fService.resetProgress();

		} catch (final NullPointerException e) {
			throw new RuntimeException("These tests need to be launched as JUnit Plug-in Test", e);
		}
	}

	@AfterEach
	public void teardown() {
		fService.deactivateService();
	}

	@Test
	@DisplayName("New user does not have assigned tasks")
	public void defaultUserHasNoTasks() {
		assertTrue(fService.getUser().getUsertasks().isEmpty());
	}

	@Test
	@DisplayName("New user does not have experience")
	public void defaultUserHasNoProgress() {
		assertEquals(0, fService.getUser().getExperience().getExperience());
	}

	@Test
	@DisplayName("New user has no title")
	public void defaultUserHasNoTitle() {
		assertTrue(fService.getUser().getTitle().isEmpty());
	}

	@Test
	@DisplayName("When a task is ready it should be added to the current user tasks")
	public void reportReadyTaskShallBeAddedToActiveUser() {
		assertEquals(0, fService.getUser().getUsertasks().size());

		final ITask task = createTask();
		final IUserTask usertask = fService.notifyTaskReady(task);

		assertEquals(1, fService.getUser().getUsertasks().size());
		assertEquals(usertask, fService.getUser().getUsertasks().get(0));
		assertEquals(task, fService.getUser().getUsertasks().get(0).getTask());
	}

	@Test
	@DisplayName("When a task is ready it should be started if flag is set")
	public void notifyTaskReadyShallBeActivated() {
		final ITask task = createTask();
		task.setAutoActivation(true);

		final IUserTask userTask = fService.notifyTaskReady(task);

		assertTrue(userTask.isStarted());
	}

	@Test
	@DisplayName("When a task is ready the user profile gets stored")
	public void notifyTaskReadyStoresUserProfile() throws IOException {
		final IDataStorage storage = mock(IDataStorage.class);
		fService.setStorage(storage);

		fService.notifyTaskReady(createTask());

		verify(storage, times(1)).storeResource(eq(IDataStorage.USER_PROFILE), any());
	}

	@Test
	@DisplayName("When a task is ready it should not be started if flag is not set")
	public void reportReadyTaskShallRemainReady() {
		final ITask task = createTask();
		task.setAutoActivation(false);

		final IUserTask userTask = fService.notifyTaskReady(task);

		assertFalse(userTask.isStarted());
	}

	@Test
	@DisplayName("Use default quest provider if not set by user")
	public void useDefaultTestProvider() {
		assertNotNull(fService.getQuestProvider());
	}

	@Test
	@DisplayName("Use local quest provider")
	public void useLocalTestProvider() {
		final IQuestProvider questProvider = new URIQuestProvider(HIERARCHICAL_TASKS_CONFIG);
		fService.setQuestProvider(questProvider);
		assertEquals(questProvider, fService.getQuestProvider());
	}

	@Test
	@DisplayName("Get all available tasks, including nested ones")
	public void getAllAvailableTasks() {
		final IQuestProvider questProvider = new URIQuestProvider(HIERARCHICAL_TASKS_CONFIG);
		final IQuest quest = questProvider.getQuests().iterator().next();

		fService.setQuestProvider(questProvider);

		assertTrue(fService.getAllAvailableTasks().size() == 3);
		assertTrue(fService.getAllAvailableTasks().contains(quest.getTasks().get(0)));
		assertTrue(fService.getAllAvailableTasks().contains(quest.getTasks().get(1)));
		assertTrue(fService.getAllAvailableTasks().contains(quest.getTasks().get(0).getTasks().get(0)));
	}

	@Test
	@DisplayName("Completed usertask pays out reward")
	public void rewardIsPaidWhenTaskIsCompleted() {
		final IQuestProvider questProvider = new URIQuestProvider(REWARDS_CONFIG);
		final ITask task = getFirstTaskFromQuestProvider(questProvider);
		final IExperienceReward reward = (IExperienceReward) task.getRewards().get(0);

		fService.activateService();
		final IUser user = fService.getUser();

		assertEquals(0, user.getExperience().getExperience());

		final IUserTask userTask = fService.notifyTaskReady(task);
		fService.notifyTaskCompleted(userTask);

		assertEquals(reward.getExperience(), user.getExperience().getExperience());
	}

	@Test
	@DisplayName("XP level update changes user title")
	public void xpLevelUpdateChangesUserTitle() {
		final IQuestProvider questProvider = new URIQuestProvider(REWARDS_CONFIG);
		final ITask task = getFirstTaskFromQuestProvider(questProvider);

		fService.setQuestProvider(questProvider);
		fService.activateService();

		final IUser user = fService.getUser();

		final String oldTitle = user.getTitle();

		final IUserTask userTask = fService.notifyTaskReady(task);
		fService.notifyTaskCompleted(userTask);

		assertNotEquals(oldTitle, user.getTitle());
	}

	private static ITask getFirstTaskFromQuestProvider(IQuestProvider questProvider) {
		final IQuest quest = questProvider.getQuests().iterator().next();
		return quest.getTasks().get(0);
	}

	private static ITask createTask() {
		final ITask task = ISkillsFactory.eINSTANCE.createTask();
		final INotDependency incompleteGoal = ISkillsFactory.eINSTANCE.createNotDependency();
		task.setGoal(incompleteGoal);

		return task;
	}
}
