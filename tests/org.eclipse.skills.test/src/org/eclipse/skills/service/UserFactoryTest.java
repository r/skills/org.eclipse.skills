/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.skills.model.ISkill;
import org.eclipse.skills.model.IUser;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class UserFactoryTest {

	@Test
	@DisplayName("createUser() returns a user instance")
	public void createUserReturnsInstance() {
		assertNotNull(new UserFactory().createUser());
	}

	@Test
	@DisplayName("createUser() returns fresh users on each run")
	public void createUserReturnsFreshUsers() {
		assertNotEquals(new UserFactory().createUser(), new UserFactory().createUser());
	}

	@Test
	@DisplayName("default user has a name starting with a capitalized letter")
	public void userHasAName() {
		final IUser user = new UserFactory().createUser();

		assertNotNull(user.getName());
		assertFalse(user.getName().isEmpty());
		assertTrue(Character.isUpperCase(user.getName().charAt(0)));
	}

	@Test
	@DisplayName("default user has 0 XP")
	public void userHas0XP() {
		final IUser user = new UserFactory().createUser();

		assertEquals("Experience", user.getExperience().getName());
		assertEquals(0, user.getExperience().getExperience());
	}

	@Test
	@DisplayName("default user has default skills")
	public void userHasDefaultSkills() {
		final IUser user = new UserFactory().createUser();

		assertNotNull(user.getSkill("Strength"));
		assertNotNull(user.getSkill("Dextery"));
		assertNotNull(user.getSkill("Wisdom"));
	}

	@Test
	@DisplayName("default skills have a description")
	public void defaultSkillsHaveDescription() {
		final IUser user = new UserFactory().createUser();

		assertFalse(user.getExperience().getDescription().getText().isEmpty());

		for (final ISkill skill : user.getSkills())
			assertFalse(skill.getDescription().getText().isEmpty());
	}

	@Test
	@DisplayName("default skills have 0 XP")
	public void defaultSkillsHave0XP() {
		final IUser user = new UserFactory().createUser();

		for (final ISkill skill : user.getSkills())
			assertEquals(0, skill.getExperience());
	}

	@Test
	@DisplayName("default user has a title")
	public void userHasATitle() {
		final IUser user = new UserFactory().createUser();

		assertNotNull(user.getTitle());
	}
}
