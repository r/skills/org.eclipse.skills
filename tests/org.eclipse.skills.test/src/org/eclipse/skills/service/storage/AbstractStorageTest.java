/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service.storage;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public abstract class AbstractStorageTest {

	private static final String UNKNOWN_FILE = "doesNotExist";
	private static final String DEFAULT_FILE = "defaultFile";
	public static final byte[] DEFAULT_CONTENT = "Lorem ipsum dolorem".getBytes();

	private static byte[] readInputStream(InputStream input) throws IOException {
		final ByteArrayOutputStream out = new ByteArrayOutputStream();

		final byte[] buffer = new byte[1024];
		int bytesRead = input.read(buffer);
		while (bytesRead > 0) {
			out.write(buffer, 0, bytesRead);
			bytesRead = input.read(buffer);
		}

		return out.toByteArray();
	}

	private IDataStorage fDataStorage;

	@BeforeEach
	public void setup() {
		fDataStorage = createStorage();
	}

	public IDataStorage getDataStorage() {
		return fDataStorage;
	}

	/**
	 * Create an instance of the storage under test.
	 *
	 * @return storage under test
	 */
	protected abstract IDataStorage createStorage();

	@Test
	@DisplayName("hasResource() == false when file does not exist")
	public void hasResourceFalseWhenFileDoesNotExist() {
		assertFalse(fDataStorage.hasResource(UNKNOWN_FILE));
	}

	@Test
	@DisplayName("hasResource() == true when file exists")
	public void hasResourceTrueWhenFileExists() throws IOException {

		fDataStorage.storeResource(DEFAULT_FILE, DEFAULT_CONTENT);

		assertTrue(fDataStorage.hasResource(DEFAULT_FILE));
	}

	@Test
	@DisplayName("loadResource() throws when file does not exist")
	public void loadResourceThrowsWhenFileDoesNotExist() {
		assertThrows(IOException.class, () -> fDataStorage.loadResource(UNKNOWN_FILE));
	}

	@Test
	@DisplayName("loadResource() returns file content when file exists")
	public void loadResourceReturnsFileContent() throws IOException {
		fDataStorage.storeResource(DEFAULT_FILE, DEFAULT_CONTENT);

		assertArrayEquals(DEFAULT_CONTENT, fDataStorage.loadResource(DEFAULT_FILE));
	}

	@Test
	@DisplayName("openResource() throws when file does not exist")
	public void openResourceThrowsWhenFileDoesNotExist() {
		assertThrows(IOException.class, () -> fDataStorage.openResource(UNKNOWN_FILE));
	}

	@Test
	@DisplayName("openResource() returns file content when file exists")
	public void openResourceReturnsFileContent() throws IOException {
		fDataStorage.storeResource(DEFAULT_FILE, DEFAULT_CONTENT);

		assertArrayEquals(DEFAULT_CONTENT, readInputStream(fDataStorage.openResource(DEFAULT_FILE)));
	}

	@Test
	@DisplayName("stored resource can be reloaded")
	public void storedResourceCanBeReloaded() throws IOException {
		final String content = new String(DEFAULT_CONTENT) + System.currentTimeMillis();
		fDataStorage.storeResource(DEFAULT_FILE, content.getBytes());

		assertArrayEquals(content.getBytes(), fDataStorage.loadResource(DEFAULT_FILE));
	}

	@Test
	@DisplayName("hasResource() == true after storeResource()")
	public void hasResourceIsTrueAfterStoreResource() throws IOException {
		final String fileName = Long.toString(System.currentTimeMillis());

		assertFalse(getDataStorage().hasResource(fileName));
		getDataStorage().storeResource(fileName, DEFAULT_CONTENT);
		assertTrue(getDataStorage().hasResource(fileName));
	}
}
