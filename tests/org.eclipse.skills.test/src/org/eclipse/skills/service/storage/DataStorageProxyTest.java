/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service.storage;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.IOException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class DataStorageProxyTest extends AbstractStorageTest {

	private IDataStorage fBaseStorage;

	@Override
	protected IDataStorage createStorage() {
		fBaseStorage = mock(IDataStorage.class);
		return new DataStorageProxy(fBaseStorage);
	}

	@Override
	public DataStorageProxy getDataStorage() {
		return (DataStorageProxy) super.getDataStorage();
	}

	@Test
	@DisplayName("constructor throws when baseStorage is null")
	public void throwsWhenBaseStorageIsNull() {
		assertThrows(IllegalArgumentException.class, () -> new DataStorageProxy(null));
	}

	@Test
	@DisplayName("getStorage() returns baseStorage")
	public void getStorage() {
		assertEquals(fBaseStorage, getDataStorage().getStorage());
	}

	@Override
	@Test
	@DisplayName("hasResource() == false when file does not exist")
	public void hasResourceFalseWhenFileDoesNotExist() {
		when(fBaseStorage.hasResource(any())).thenReturn(false);

		super.hasResourceFalseWhenFileDoesNotExist();
	}

	@Override
	@Test
	@DisplayName("hasResource() == true when file exists")
	public void hasResourceTrueWhenFileExists() throws IOException {
		when(fBaseStorage.hasResource(any())).thenReturn(true);

		super.hasResourceTrueWhenFileExists();
	}

	@Override
	@Test
	@DisplayName("loadResource() throws when file does not exist")
	public void loadResourceThrowsWhenFileDoesNotExist() {
		when(fBaseStorage.hasResource(any())).thenReturn(false);

		super.loadResourceThrowsWhenFileDoesNotExist();
	}

	@Override
	@Test
	@DisplayName("loadResource() returns file content when file exists")
	public void loadResourceReturnsFileContent() throws IOException {
		when(fBaseStorage.hasResource(any())).thenReturn(true);
		when(fBaseStorage.loadResource(any())).thenReturn(DEFAULT_CONTENT);

		super.loadResourceReturnsFileContent();
	}

	@Test
	@DisplayName("loadResource() throws when base storage throws")
	public void loadResourceThrowsWhenBaseStorageThrows() throws IOException {
		when(fBaseStorage.hasResource(any())).thenReturn(true);
		when(fBaseStorage.loadResource(any())).thenThrow(new EOFException());

		assertThrows(EOFException.class, () -> getDataStorage().loadResource("any"));
	}

	@Override
	@Test
	@DisplayName("openResource() throws when file does not exist")
	public void openResourceThrowsWhenFileDoesNotExist() {
		when(fBaseStorage.hasResource(any())).thenReturn(false);

		super.openResourceThrowsWhenFileDoesNotExist();
	}

	@Override
	@Test
	@DisplayName("openResource() returns file content when file exists")
	public void openResourceReturnsFileContent() throws IOException {
		when(fBaseStorage.hasResource(any())).thenReturn(true);
		when(fBaseStorage.openResource(any())).thenReturn(new ByteArrayInputStream(DEFAULT_CONTENT));

		super.openResourceReturnsFileContent();
	}

	@Test
	@DisplayName("openResource() throws when base storage throws")
	public void openResourceThrowsWhenBaseStorageThrows() throws IOException {
		when(fBaseStorage.hasResource(any())).thenReturn(true);
		when(fBaseStorage.openResource(any())).thenThrow(new EOFException());

		assertThrows(EOFException.class, () -> getDataStorage().openResource("any"));
	}

	@Override
	@Test
	@DisplayName("stored resource can be reloaded")
	public void storedResourceCanBeReloaded() throws IOException {
		when(fBaseStorage.hasResource(any())).thenReturn(true);
		when(fBaseStorage.loadResource(any())).thenReturn(DEFAULT_CONTENT);

		getDataStorage().storeResource("any", DEFAULT_CONTENT);
		assertArrayEquals(DEFAULT_CONTENT, getDataStorage().loadResource("any"));

		verify(fBaseStorage, times(1)).storeResource(any(), any());
	}

	@Override
	@Test
	@DisplayName("hasResource() == true after storeResource()")
	public void hasResourceIsTrueAfterStoreResource() throws IOException {
		when(fBaseStorage.hasResource(any())).thenReturn(false).thenReturn(true);

		super.hasResourceIsTrueAfterStoreResource();

		verify(fBaseStorage, times(1)).storeResource(any(), any());
	}

	@Test
	@DisplayName("storeResource() throws when base storage throws")
	public void storeResourceThrowsWhenBaseStorageThrows() throws IOException {
		doThrow(new EOFException()).when(fBaseStorage).storeResource(any(), any());

		assertThrows(EOFException.class, () -> getDataStorage().storeResource("any", DEFAULT_CONTENT));
	}

	@Test
	@DisplayName("setStorage() throws when baseStorage == null")
	public void setStorageThrowsOnNullParameter() {
		assertThrows(IllegalArgumentException.class, () -> getDataStorage().setStorage(null));
	}

	@Test
	@DisplayName("setStorage() allows to replace storage")
	public void setStorageSetsTheStorage() {
		when(fBaseStorage.hasResource(any())).thenReturn(true);

		final IDataStorage newStorage = mock(IDataStorage.class);
		when(newStorage.hasResource(any())).thenReturn(false);

		assertTrue(getDataStorage().hasResource("any"));

		getDataStorage().setStorage(newStorage);
		assertEquals(newStorage, getDataStorage().getStorage());
		assertFalse(getDataStorage().hasResource("any"));
	}
}
