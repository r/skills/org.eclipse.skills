/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.osgi.service.event.EventHandler;

public class BrokerToolsTest {

	private static final String TEST_TOPIC = "org/eclipse/skills/test";

	@Test
	@DisplayName("Get broker returns broker service")
	public void getBrokerReturnsService() {
		assertNotNull(BrokerTools.getBroker());
	}

	@Test
	@DisplayName("Subscribe  to a channel")
	public void subscribeToChannel() {
		final EventHandler handler = mock(EventHandler.class);
		BrokerTools.subscribe(TEST_TOPIC, handler);

		verify(handler, times(0)).handleEvent(any());

		BrokerTools.send(TEST_TOPIC, new Object());
		verify(handler, times(1)).handleEvent(any());
	}

	@Test
	@DisplayName("Unsubscribe a handler")
	public void unsubscribeFromBroker() {
		final EventHandler handler = mock(EventHandler.class);
		BrokerTools.subscribe(TEST_TOPIC, handler);

		BrokerTools.unsubscribe(handler);

		BrokerTools.send(TEST_TOPIC, new Object());
		verify(handler, times(0)).handleEvent(any());
	}
}
