/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.dependencies;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.ui.PlatformUI;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class BrokerEventDependencyTest {

	public static boolean hasBroker() {
		try {
			getBroker();
			return true;
		} catch (final IllegalStateException e) {
			// workbench not available
			return false;
		}
	}

	public static IEventBroker getBroker() {
		return PlatformUI.getWorkbench().getService(IEventBroker.class);
	}

	private BrokerEventDependency fDependency;

	// FIXME tests run locally but cannot be executed on jenkins. can we disable these tests only on jenkins?
	@BeforeEach
	@Disabled
	public void setup() {
		fDependency = new BrokerEventDependency();
	}

	@Test
	@Disabled
	public void anyEventOnConcreateChannel() {
		fDependency.setAttributes("topic=org/eclipse/skills/testchannel");
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		getBroker().send("org/eclipse/skills/testchannel", "something");

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@Disabled
	public void eventOnWrongChannel() {
		fDependency.setAttributes("topic=org/eclipse/skills/testchannel");
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		getBroker().send("org/eclipse/skills/anotherchannel", "something");

		assertFalse(fDependency.isFulfilled());
	}

	@Test
	@Disabled
	public void anyEventOnSubChannel() {
		fDependency.setAttributes("topic=org/eclipse/skills/*");
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		getBroker().send("org/eclipse/skills/testchannel/sub", "something");

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@Disabled
	public void specificEventAsSimpleDataObject() {
		fDependency.setAttributes("topic=org/eclipse/skills/testchannel\ndata=something");
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		getBroker().send("org/eclipse/skills/testchannel", "something");

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@Disabled
	public void specificEventAsMappedDataObject() {
		fDependency.setAttributes("topic=org/eclipse/skills/testchannel\ndata=something");
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		final Map<String, String> attributes = new HashMap<>();
		attributes.put("data", "something");
		getBroker().send("org/eclipse/skills/testchannel", attributes);

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@Disabled
	public void specificEventWithSpecialAttribute() {
		fDependency.setAttributes("topic=org/eclipse/skills/testchannel\nspecial=something");
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		final Map<String, String> attributes = new HashMap<>();
		attributes.put("special", "something");
		getBroker().send("org/eclipse/skills/testchannel", attributes);

		assertTrue(fDependency.isFulfilled());
	}
}
