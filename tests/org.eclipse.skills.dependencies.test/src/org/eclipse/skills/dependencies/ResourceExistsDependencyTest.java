/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.dependencies;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ResourceExistsDependencyTest {

	private ResourceExistsDependency fDependency;

	@BeforeEach
	public void setup() {
		fDependency = new ResourceExistsDependency();
	}

	@Test
	@DisplayName("Resource exists before activation for exact match")
	public void projectAlreadyExists() throws CoreException {

		ResourcesPlugin.getWorkspace().getRoot().getProject("Testproject").create(null);

		fDependency.setAttributes("Testproject");
		fDependency.activate();

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Resource created after activation for exact match")
	public void projectGetsCreated() throws CoreException {
		fDependency.setAttributes("Testproject2");
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		ResourcesPlugin.getWorkspace().getRoot().getProject("Testproject2").create(null);

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Resource created after activation for wildcard match")
	public void projectWithWildcard() throws CoreException {
		fDependency.setAttributes("Wildcard*");
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		ResourcesPlugin.getWorkspace().getRoot().getProject("WildcardProject").create(null);

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Resource with path wildcard")
	public void projectWithPathWildcard() throws CoreException {
		fDependency.setAttributes("Project1/**/Simple.txt");
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		final IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject("Project1");
		project.create(null);
		project.open(null);

		project.getFile("Simple.txt").create(new ByteArrayInputStream("test".getBytes()), true, null);

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Resource containing subfolders with path wildcard")
	public void projectWithPathWildcard2() throws CoreException {
		fDependency.setAttributes("Project2/**/Simple.txt");
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		final IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject("Project2");
		project.create(null);
		project.open(null);

		final IFolder folder1 = project.getFolder("parent");
		folder1.create(true, true, null);

		final IFolder folder2 = project.getFolder("child");
		folder2.create(true, true, null);

		folder2.getFile("Simple.txt").create(new ByteArrayInputStream("test".getBytes()), true, null);

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Resource containing subfolders with path wildcard and file wildcard")
	public void projectWithPathAndFileWildcard() throws CoreException {
		fDependency.setAttributes("Project3/**/*.txt");
		fDependency.activate();

		assertFalse(fDependency.isFulfilled());

		final IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject("Project3");
		project.create(null);
		project.open(null);

		final IFolder folder1 = project.getFolder("parent");
		folder1.create(true, true, null);

		final IFolder folder2 = project.getFolder("child");
		folder2.create(true, true, null);

		folder2.getFile("Simple.bin").create(new ByteArrayInputStream("test".getBytes()), true, null);

		assertFalse(fDependency.isFulfilled());

		folder2.getFile("Simple.txt").create(new ByteArrayInputStream("test".getBytes()), true, null);

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("Existing resource gets deleted")
	public void existingProjectGetsDeleted() throws CoreException {
		final IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject("Project1");

		fDependency.setAttributes("Testproject");

		ResourcesPlugin.getWorkspace().getRoot().getProject("Testproject").create(null);

		fDependency.activate();
		assertTrue(fDependency.isFulfilled());

		ResourcesPlugin.getWorkspace().getRoot().getProject("Testproject").delete(true, null);

		assertFalse(fDependency.isFulfilled());
	}
}
