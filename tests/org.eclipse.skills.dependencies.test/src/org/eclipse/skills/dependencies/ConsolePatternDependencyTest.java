/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.dependencies;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.jface.text.DocumentEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ConsolePatternDependencyTest {

	private ConsolePatternDependency fDependency;

	@BeforeEach
	public void setup() {
		fDependency = new ConsolePatternDependency();
	}

	@Test
	public void createDependency() {
		assertNotNull(fDependency);
		assertFalse(fDependency.isFulfilled());
		assertEquals("<none>", fDependency.getPattern());
	}

	@Test
	public void setAttributes() {
		fDependency.setAttributes("First .+ Last");
		assertEquals("First .+ Last", fDependency.getPattern());
	}

	@Test
	@DisplayName("stay not fulfilled on text mismatch")
	public void patternMismatch() {
		fDependency.setAttributes("First .+ Last");

		final DocumentEvent event = new DocumentEvent();
		event.fText = "First only";
		fDependency.documentChanged(event);

		assertFalse(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("fulfilled on exact text match")
	public void exactPatternMatch() {
		fDependency.setAttributes("First .+ Last");

		final DocumentEvent event = new DocumentEvent();
		event.fText = "First Second Last";
		fDependency.documentChanged(event);

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("fulfilled on partial text match")
	public void partialPatternMatch() {
		fDependency.setAttributes("First .+ Last");

		final DocumentEvent event = new DocumentEvent();
		event.fText = "A name is comprised of First Second Last and hopefully noting more";
		fDependency.documentChanged(event);

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	@DisplayName("multiline match")
	public void multilineMatch() {
		fDependency.setAttributes("First .+ Last");

		final DocumentEvent event = new DocumentEvent();
		event.fText = "A name is comprised of First \nSecond Last\n and hopefully noting more";
		fDependency.documentChanged(event);

		assertTrue(fDependency.isFulfilled());
	}
}
