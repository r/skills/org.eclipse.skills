/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.dependencies;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class P2DependencyTest {

	private P2Dependency fDependency;

	@BeforeEach
	public void setup() {
		fDependency = new P2Dependency();
	}

	@Test
	public void existingFeature() {
		fDependency.setAttributes("org.junit.jupiter.api");

		assertFalse(fDependency.isFulfilled());

		fDependency.activate();

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	public void nonExistingFeature() {
		fDependency.setAttributes("org.notthere.junit.jupiter.api");

		assertFalse(fDependency.isFulfilled());

		fDependency.activate();

		assertFalse(fDependency.isFulfilled());
	}

	@Test
	public void existingExactFeature() {
		fDependency.setAttributes("org.junit.jupiter.api;bundle-version=\"5.6.0\"");

		assertFalse(fDependency.isFulfilled());

		fDependency.activate();

		assertTrue(fDependency.isFulfilled());
	}

	@Test
	public void existingRangedFeature() {
		fDependency.setAttributes("org.junit.jupiter.api;bundle-version=\"[5.4.0, 5.8.0]\"");

		assertFalse(fDependency.isFulfilled());

		fDependency.activate();

		assertTrue(fDependency.isFulfilled());
	}
}
