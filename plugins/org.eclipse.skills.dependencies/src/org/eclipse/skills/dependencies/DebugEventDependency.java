/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.dependencies;

import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IDebugEventSetListener;
import org.eclipse.skills.model.IDependencyWithAttributes;
import org.eclipse.skills.model.impl.MDependency;

public class DebugEventDependency extends MDependency implements IDependencyWithAttributes, IDebugEventSetListener {

	private static String toString(DebugEvent event) {
		switch (event.getKind()) {
		case DebugEvent.RESUME: {

			switch (event.getDetail()) {
			case DebugEvent.STEP_INTO:
				return "resume,stepInto";
			case DebugEvent.STEP_OVER:
				return "resume,stepOver";
			case DebugEvent.STEP_RETURN:
				return "resume,stepReturn";
			case DebugEvent.STEP_END:
				return "resume,stepEnd";
			case DebugEvent.EVALUATION:
				return "resume,evaluation";

			default:
				return "resume";
			}
		}
		case DebugEvent.SUSPEND: {
			switch (event.getDetail()) {
			case DebugEvent.STEP_END:
				return "suspend,stepEnd";

			case DebugEvent.BREAKPOINT:
				return "suspend,breakpoint";
			case DebugEvent.EVALUATION:
				return "suspend,evaluation";

			default:
				return "suspend";
			}
		}

		case DebugEvent.CREATE:
			return "create";

		case DebugEvent.TERMINATE:
			return "terminate";

		case DebugEvent.CHANGE:
			return "change";

		default:
			return "<unknown>";
		}
	}

	private String fEventType;

	@Override
	public void setAttributes(String attributes) {
		fEventType = attributes;
	}

	public String getEventType() {
		return fEventType;
	}

	@Override
	public void activate() {
		DebugPlugin.getDefault().addDebugEventListener(this);
	}

	@Override
	public void deactivate() {
		DebugPlugin.getDefault().removeDebugEventListener(this);
	}

	@Override
	public String toString() {
		return "Debug event \"" + getEventType() + "\" detected";
	}

	@Override
	public void handleDebugEvents(DebugEvent[] events) {
		for (final DebugEvent event : events) {
			if (toString(event).contains(getEventType()))
				setFulfilled(true);
		}
	}
}
