/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.dependencies;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IBundleGroup;
import org.eclipse.core.runtime.IBundleGroupProvider;
import org.eclipse.core.runtime.Platform;
import org.eclipse.osgi.service.resolver.VersionRange;
import org.eclipse.skills.model.IDependencyWithAttributes;
import org.eclipse.skills.model.impl.MDependency;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.Version;

public class P2Dependency extends MDependency implements IDependencyWithAttributes {

	private final Pattern ATTRIBUTE_PATTERN = Pattern.compile("([\\w\\.]+)(?:(?:;bundle-version=\\\")?\\s?([\\[\\]\\(\\)\\d\\.,\\s]+)\\\"?)?");

	private String fFeatureId;

	private VersionRange fVersionRange;

	@Override
	public void setAttributes(String attributes) {
		final Matcher matcher = ATTRIBUTE_PATTERN.matcher(attributes);
		if (matcher.matches()) {
			fFeatureId = matcher.group(1);
			fVersionRange = (matcher.group(2) != null) ? new VersionRange(matcher.group(2)) : null;
		}
	}

	public String getFeatureId() {
		return fFeatureId;
	}

	public VersionRange getVersionRange() {
		return fVersionRange;
	}

	@Override
	public void activate() {
		for (final IBundleGroupProvider provider : Platform.getBundleGroupProviders()) {
			for (final IBundleGroup feature : provider.getBundleGroups()) {
				final String featureId = feature.getIdentifier();

				if ((Objects.equals(featureId, getFeatureId())) || (Objects.equals(featureId, getFeatureId() + ".feature.group"))) {
					// feature match, check version
					setFulfilled(isRangeValid(new Version(feature.getVersion())));
					return;
				}
			}
		}

		final BundleContext context = FrameworkUtil.getBundle(getClass()).getBundleContext();
		for (final Bundle bundle : context.getBundles()) {
			if (Objects.equals(getFeatureId(), bundle.getSymbolicName())) {
				// ID match, check version
				setFulfilled(isRangeValid(bundle.getVersion()));
				return;
			}
		}

		setFulfilled(false);
	}

	public boolean isRangeValid(Version currentVersion) {
		if (getVersionRange() != null)
			return getVersionRange().includes(currentVersion);

		return true;
	}

	@Override
	public void deactivate() {
		// nothing to do as this is a one-time check
	}

	@Override
	public String toString() {
		return "Installed \"" + getFeatureId() + ((getVersionRange() != null) ? " " + getVersionRange() : "") + "\"";
	}
}
