/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.dependencies;

import org.eclipse.skills.RunnableWithResult;
import org.eclipse.skills.model.IDependencyWithAttributes;
import org.eclipse.skills.model.impl.MDependency;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.PlatformUI;

public class ViewDependency extends MDependency implements IDependencyWithAttributes, IPartListener {

	private static String getActivePartId() {
		final RunnableWithResult<String> runnable = new RunnableWithResult<String>() {
			@Override
			public String runWithTry() throws Throwable {

				final IWorkbenchPartReference reference = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActivePartReference();
				if (reference instanceof IViewReference) {
					final String secondaryId = ((IViewReference) reference).getSecondaryId();
					return (secondaryId == null) ? reference.getId() : reference.getId() + ":" + secondaryId;
				}

				return PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActivePartReference().getId();
			}
		};
		Display.getDefault().syncExec(runnable);

		return runnable.getResult();
	}

	private String fViewID;

	public String getViewID() {
		return fViewID;
	}

	public void setViewID(String viewID) {
		fViewID = viewID;
	}

	@Override
	public void setAttributes(String attributes) {
		setViewID(attributes);
	}

	@Override
	public void activate() {
		waitUntilWorkbenchIsReady();
		Display.getDefault().asyncExec(() -> PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().addPartListener(this));

		setFulfilled(getActivePartId().equals(getViewID()));
	}

	@Override
	public void deactivate() {
		Display.getDefault().asyncExec(() -> PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().removePartListener(this));
	}

	@Override
	public void partOpened(IWorkbenchPart part) {
	}

	@Override
	public void partDeactivated(IWorkbenchPart part) {
	}

	@Override
	public void partClosed(IWorkbenchPart part) {
	}

	@Override
	public void partBroughtToTop(IWorkbenchPart part) {
	}

	@Override
	public void partActivated(IWorkbenchPart part) {
		setFulfilled(getActivePartId().equals(getViewID()));
	}

	private void waitUntilWorkbenchIsReady() {
		while (true) {
			try {
				getActivePartId();
				return;
			} catch (final Exception e) {
				e.printStackTrace();

				try {
					Thread.sleep(300);
				} catch (final InterruptedException e1) {
					return;
				}
			}
		}
	}

	@Override
	public String toString() {
		return "View \"" + getViewID() + "\" active";
	}
}
