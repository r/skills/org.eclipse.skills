/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.dependencies;

import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.ui.PlatformUI;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

public class BrokerEventDependency extends AbstractCustomDependency implements EventHandler {

	public String getTopic() {
		return getProperty("topic", "*");
	}

	@Override
	public void activate() {
		final IEventBroker eventBroker = PlatformUI.getWorkbench().getService(IEventBroker.class);
		eventBroker.subscribe(getTopic(), this);
	}

	@Override
	public void deactivate() {
		final IEventBroker eventBroker = PlatformUI.getWorkbench().getService(IEventBroker.class);
		eventBroker.unsubscribe(this);
	}

	@Override
	public String toString() {
		return "Event on \"" + getTopic() + "\" detected";
	}

	@Override
	public void handleEvent(Event event) {
		boolean matches = true;

		for (final Entry<Object, Object> entry : getProperties().entrySet()) {
			if ("topic".equals(entry.getKey()))
				continue;

			matches &= Pattern.matches(String.valueOf(entry.getValue()), getEventData(event, String.valueOf(entry.getKey())));
		}

		if (matches)
			setFulfilled(true);
	}

	private String getEventData(Event event, String key) {
		if ("data".equals(key)) {
			Object value = event.getProperty(key);
			if (value == null)
				value = event.getProperty(IEventBroker.DATA);

			return String.valueOf(value);
		}

		return String.valueOf(event.getProperty(key));
	}
}
