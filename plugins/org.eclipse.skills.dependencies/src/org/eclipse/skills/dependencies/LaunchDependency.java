/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.dependencies;

import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.ILaunchesListener2;

public class LaunchDependency extends AbstractCustomDependency implements ILaunchesListener2 {

	public String getLaunchMode() {
		return getProperty("mode", "*");
	}

	public String getLaunchName() {
		return getProperty("name", "*");
	}

	@Override
	public void activate() {
		final ILaunchManager launchManager = DebugPlugin.getDefault().getLaunchManager();
		launchManager.addLaunchListener(this);
	}

	@Override
	public void deactivate() {
		final ILaunchManager launchManager = DebugPlugin.getDefault().getLaunchManager();
		launchManager.removeLaunchListener(this);
	}

	@Override
	public String toString() {
		return "Launch \"" + getLaunchName() + "\" (mode " + getLaunchMode() + ") finished";
	}

	@Override
	public void launchesRemoved(ILaunch[] launches) {
		// nothing to do
	}

	@Override
	public void launchesAdded(ILaunch[] launches) {
		// nothing to do
	}

	@Override
	public void launchesChanged(ILaunch[] launches) {
		// nothing to do
	}

	@Override
	public void launchesTerminated(ILaunch[] launches) {
		for (final ILaunch launch : launches) {
			final boolean modeStatus = matchesWithWildcard(getLaunchMode(), launch.getLaunchMode());
			final boolean nameStatus = matchesWithWildcard(getLaunchName(), launch.getLaunchConfiguration().getName());

			if (nameStatus || modeStatus)
				setFulfilled(true);
		}
	}
}
