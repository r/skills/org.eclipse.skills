/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.dependencies;

import org.eclipse.skills.RunnableWithResult;
import org.eclipse.skills.model.IDependencyWithAttributes;
import org.eclipse.skills.model.impl.MDependency;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IPerspectiveListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

public class PerspectiveDependency extends MDependency implements IDependencyWithAttributes, IPerspectiveListener {

	private static String getActivePerspectiveId() {
		final RunnableWithResult<String> runnable = new RunnableWithResult<String>() {
			@Override
			public String runWithTry() throws Throwable {
				return PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getPerspective().getId();
			}
		};
		Display.getDefault().syncExec(runnable);

		return runnable.getResult();
	}

	private String fPerspectiveID;

	public String getPerspectiveID() {
		return fPerspectiveID;
	}

	public void setPerspectiveID(String perspectiveID) {
		fPerspectiveID = perspectiveID;
	}

	@Override
	public void setAttributes(String attributes) {
		setPerspectiveID(attributes);
	}

	@Override
	public void activate() {
		waitUntilWorkbenchIsReady();
		Display.getDefault().asyncExec(() -> PlatformUI.getWorkbench().getActiveWorkbenchWindow().addPerspectiveListener(this));

		setFulfilled(getActivePerspectiveId().equals(getPerspectiveID()));
	}

	@Override
	public void deactivate() {
		Display.getDefault().asyncExec(() -> PlatformUI.getWorkbench().getActiveWorkbenchWindow().removePerspectiveListener(this));
	}

	@Override
	public void perspectiveChanged(IWorkbenchPage page, IPerspectiveDescriptor perspective, String changeId) {
	}

	@Override
	public void perspectiveActivated(IWorkbenchPage page, IPerspectiveDescriptor perspective) {
		setFulfilled(getActivePerspectiveId().equals(getPerspectiveID()));
	}

	private void waitUntilWorkbenchIsReady() {
		while (true) {
			try {
				getActivePerspectiveId();
				return;
			} catch (final Exception e) {
				e.printStackTrace();

				try {
					Thread.sleep(300);
				} catch (final InterruptedException e1) {
					return;
				}
			}
		}
	}

	@Override
	public String toString() {
		return "Perspective \"" + getPerspectiveID() + "\" active";
	}
}
