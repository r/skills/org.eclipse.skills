/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.dependencies;

import java.util.regex.Pattern;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.skills.Activator;
import org.eclipse.skills.Logger;
import org.eclipse.skills.model.IDependencyWithAttributes;
import org.eclipse.skills.model.impl.MDependency;

public class ResourceExistsDependency extends MDependency implements IDependencyWithAttributes, IResourceChangeListener {

	private IPath fResourcePath;

	public ResourceExistsDependency() {
	}

	@Override
	public void setAttributes(String attributes) {
		fResourcePath = (attributes.contains("*")) ? new WildcardPath(attributes) : new Path(attributes);
		if (!fResourcePath.isAbsolute())
			fResourcePath = fResourcePath.makeAbsolute();
	}

	public IPath getResourcePath() {
		return fResourcePath;
	}

	public IContainer getRootContainerToMonitor() {
		IPath rootPath = new Path("/");
		for (final String segment : getResourcePath().segments()) {
			if (segment.contains("*"))
				break;

			rootPath = rootPath.append(segment);
		}

		if (rootPath.segmentCount() >= 1)
			rootPath = rootPath.removeLastSegments(1);

		if (rootPath.segmentCount() > 0)
			return ResourcesPlugin.getWorkspace().getRoot().getFolder(rootPath);

		return ResourcesPlugin.getWorkspace().getRoot();
	}

	@Override
	public void activate() {
		ResourcesPlugin.getWorkspace().addResourceChangeListener(this, IResourceChangeEvent.POST_CHANGE);

		evaluate();
	}

	@Override
	public void deactivate() {
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
	}

	private void evaluate() {
		final ResourceValidator validator = new ResourceValidator();
		try {
			getRootContainerToMonitor().accept(validator);
			setFulfilled(validator.isResourceFound());

		} catch (final CoreException e) {
			Logger.error(Activator.PLUGIN_ID, "Could not visit resource", e);

			setFulfilled(false);
		}
	}

	@Override
	public void resourceChanged(IResourceChangeEvent event) {
		evaluate();
	}

	@Override
	public String toString() {
		return "Workspace contains \"" + getResourcePath() + "\"";
	}

	private class ResourceValidator implements IResourceVisitor {

		private boolean fResourceFound = false;

		@Override
		public boolean visit(IResource resource) throws CoreException {
			if (getResourcePath().equals(resource.getFullPath()))
				fResourceFound = true;

			// do not continue if a matching resource is already detected
			return !isResourceFound();
		}

		public boolean isResourceFound() {
			return fResourceFound;
		}
	}

	public static class WildcardPath extends Path {

		public static boolean matches(IPath expectedPath, IPath actualPath) {
			if (expectedPath.toPortableString().contains("**")) {
				for (int index = 0; index < expectedPath.segmentCount(); index++) {
					if (expectedPath.segment(index).equals("**")) {
						// try to greedily consume actualPath
						for (int removeFromTarget = actualPath.segmentCount() - 1; removeFromTarget >= 0; removeFromTarget--) {
							if (matches(expectedPath.removeFirstSegments(1), actualPath.removeFirstSegments(removeFromTarget)))
								return true;
						}

						return false;
					}

					if (actualPath.segmentCount() <= index)
						return false;

					if (!Pattern.matches(expectedPath.segment(index).replaceAll("\\*", ".*"), actualPath.segment(index)))
						return false;
				}

			} else if (expectedPath.toPortableString().contains("*")) {
				return Pattern.matches(expectedPath.toPortableString().replaceAll("\\*", "[^/]*"), actualPath.toPortableString());

			} else
				return expectedPath.equals(actualPath);

			return false;
		}

		public WildcardPath(String device, String path) {
			super(device, path);
		}

		public WildcardPath(String fullPath) {
			super(fullPath);
		}

		public boolean matches(IPath target) {
			return matches(this, target);
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof IPath)
				return matches((IPath) obj);

			return super.equals(obj);
		}

		@Override
		public IPath makeAbsolute() {
			return new WildcardPath(super.makeAbsolute().toPortableString());
		}
	}
}