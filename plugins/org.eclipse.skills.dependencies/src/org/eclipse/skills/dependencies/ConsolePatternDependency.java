/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.dependencies;

import java.util.regex.Pattern;

import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.skills.model.IDependencyWithAttributes;
import org.eclipse.skills.model.impl.MDependency;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleListener;
import org.eclipse.ui.console.TextConsole;

public class ConsolePatternDependency extends MDependency implements IDependencyWithAttributes, IConsoleListener, IDocumentListener {

	private String fOutputPattern = "<none>";

	@Override
	public void setAttributes(String attributes) {
		fOutputPattern = attributes;
	}

	public String getPattern() {
		return fOutputPattern;
	}

	@Override
	public void activate() {
		ConsolePlugin.getDefault().getConsoleManager().addConsoleListener(this);

		consolesAdded(ConsolePlugin.getDefault().getConsoleManager().getConsoles());
	}

	@Override
	public void deactivate() {
		consolesRemoved(ConsolePlugin.getDefault().getConsoleManager().getConsoles());

		ConsolePlugin.getDefault().getConsoleManager().removeConsoleListener(this);
	}

	@Override
	public String toString() {
		return "Console contains \"" + fOutputPattern + "\"";
	}

	@Override
	public void consolesAdded(IConsole[] consoles) {
		for (final IConsole console : consoles) {
			if (console instanceof TextConsole)
				((TextConsole) console).getDocument().addDocumentListener(this);
		}
	}

	@Override
	public void consolesRemoved(IConsole[] consoles) {
		for (final IConsole console : consoles) {
			if (console instanceof TextConsole)
				((TextConsole) console).getDocument().removeDocumentListener(this);
		}
	}

	@Override
	public void documentAboutToBeChanged(DocumentEvent event) {
		// nothing to do
	}

	@Override
	public void documentChanged(DocumentEvent event) {
		final Pattern pattern = Pattern.compile(getPattern(), Pattern.DOTALL | Pattern.MULTILINE);

		if (pattern.matcher(event.getText()).find()) {
			setFulfilled(true);
			deactivate();
		}
	}
}
