/**
 */
package org.eclipse.skills.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Badge Reward</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.IBadgeReward#getBadge <em>Badge</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getBadgeReward()
 * @model
 * @generated
 */
public interface IBadgeReward extends IReward {
	/**
	 * Returns the value of the '<em><b>Badge</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Badge</em>' containment reference.
	 * @see #setBadge(IBadge)
	 * @see org.eclipse.skills.model.ISkillsPackage#getBadgeReward_Badge()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IBadge getBadge();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IBadgeReward#getBadge <em>Badge</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Badge</em>' containment reference.
	 * @see #getBadge()
	 * @generated
	 */
	void setBadge(IBadge value);

} // IBadgeReward
