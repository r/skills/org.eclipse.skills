/**
 */
package org.eclipse.skills.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Skill Reward</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.ISkillReward#getSkill <em>Skill</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getSkillReward()
 * @model
 * @generated
 */
public interface ISkillReward extends IExperienceReward {
	/**
	 * Returns the value of the '<em><b>Skill</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Skill</em>' reference.
	 * @see #setSkill(ISkill)
	 * @see org.eclipse.skills.model.ISkillsPackage#getSkillReward_Skill()
	 * @model required="true"
	 * @generated
	 */
	ISkill getSkill();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ISkillReward#getSkill <em>Skill</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Skill</em>' reference.
	 * @see #getSkill()
	 * @generated
	 */
	void setSkill(ISkill value);

} // ISkillReward
