/**
 */
package org.eclipse.skills.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Not Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getNotDependency()
 * @model
 * @generated
 */
public interface INotDependency extends IContainedDependency {
} // INotDependency
