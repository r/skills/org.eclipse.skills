/**
 */
package org.eclipse.skills.model;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.skills.model.ISkillsFactory
 * @model kind="package"
 * @generated
 */
public interface ISkillsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "model";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://eclipse.org/skills/1.0.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "skills";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ISkillsPackage eINSTANCE = org.eclipse.skills.model.impl.MSkillsPackage.init();

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MTask <em>Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MTask
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getTask()
	 * @generated
	 */
	int TASK = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Goal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__GOAL = 1;

	/**
	 * The feature id for the '<em><b>Rewards</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__REWARDS = 2;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__TITLE = 3;

	/**
	 * The feature id for the '<em><b>Requirement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__REQUIREMENT = 4;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__TASKS = 5;

	/**
	 * The feature id for the '<em><b>Hints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__HINTS = 6;

	/**
	 * The feature id for the '<em><b>Auto Activation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__AUTO_ACTIVATION = 7;

	/**
	 * The feature id for the '<em><b>Skill Service</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__SKILL_SERVICE = 8;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__ID = 9;

	/**
	 * The number of structural features of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_FEATURE_COUNT = 10;

	/**
	 * The operation id for the '<em>Is Available</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK___IS_AVAILABLE = 0;

	/**
	 * The operation id for the '<em>Get Short Title</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK___GET_SHORT_TITLE__INT = 1;

	/**
	 * The number of operations of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MDescription <em>Description</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MDescription
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getDescription()
	 * @generated
	 */
	int DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIPTION__TEXT = 0;

	/**
	 * The number of structural features of the '<em>Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIPTION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIPTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.IReward <em>Reward</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.IReward
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getReward()
	 * @generated
	 */
	int REWARD = 2;

	/**
	 * The number of structural features of the '<em>Reward</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Pay Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD___PAY_OUT__IUSER = 0;

	/**
	 * The number of operations of the '<em>Reward</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MBadge <em>Badge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MBadge
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getBadge()
	 * @generated
	 */
	int BADGE = 3;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGE__TITLE = 0;

	/**
	 * The feature id for the '<em><b>Image URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGE__IMAGE_URL = 1;

	/**
	 * The number of structural features of the '<em>Badge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGE_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Get Image Descriptor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGE___GET_IMAGE_DESCRIPTOR = 0;

	/**
	 * The number of operations of the '<em>Badge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGE_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MUser <em>User</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MUser
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getUser()
	 * @generated
	 */
	int USER = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__NAME = 0;

	/**
	 * The feature id for the '<em><b>Skills</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__SKILLS = 1;

	/**
	 * The feature id for the '<em><b>Usertasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__USERTASKS = 2;

	/**
	 * The feature id for the '<em><b>Image Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__IMAGE_LOCATION = 3;

	/**
	 * The feature id for the '<em><b>Experience</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__EXPERIENCE = 4;

	/**
	 * The feature id for the '<em><b>Badges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__BADGES = 5;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__TITLE = 6;

	/**
	 * The number of structural features of the '<em>User</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_FEATURE_COUNT = 7;

	/**
	 * The operation id for the '<em>Add Task</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER___ADD_TASK__ITASK = 0;

	/**
	 * The operation id for the '<em>Get Skill</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER___GET_SKILL__ISKILL = 1;

	/**
	 * The operation id for the '<em>Consume</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER___CONSUME__IREWARD = 2;

	/**
	 * The operation id for the '<em>Get Avatar</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER___GET_AVATAR = 3;

	/**
	 * The operation id for the '<em>Get Skill</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER___GET_SKILL__STRING = 4;

	/**
	 * The number of operations of the '<em>User</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_OPERATION_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MSkill <em>Skill</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MSkill
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getSkill()
	 * @generated
	 */
	int SKILL = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL__NAME = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Experience</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL__EXPERIENCE = 2;

	/**
	 * The feature id for the '<em><b>Progression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL__PROGRESSION = 3;

	/**
	 * The feature id for the '<em><b>Base Skill</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL__BASE_SKILL = 4;

	/**
	 * The feature id for the '<em><b>Image URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL__IMAGE_URI = 5;

	/**
	 * The number of structural features of the '<em>Skill</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FEATURE_COUNT = 6;

	/**
	 * The operation id for the '<em>Add Experience</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL___ADD_EXPERIENCE__INT = 0;

	/**
	 * The operation id for the '<em>Get Image Descriptor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL___GET_IMAGE_DESCRIPTOR = 1;

	/**
	 * The number of operations of the '<em>Skill</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MUserTask <em>User Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MUserTask
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getUserTask()
	 * @generated
	 */
	int USER_TASK = 6;

	/**
	 * The feature id for the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TASK__TASK = 0;

	/**
	 * The feature id for the '<em><b>Started</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TASK__STARTED = 1;

	/**
	 * The feature id for the '<em><b>Finished</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TASK__FINISHED = 2;

	/**
	 * The feature id for the '<em><b>Hints Displayed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TASK__HINTS_DISPLAYED = 3;

	/**
	 * The feature id for the '<em><b>Added</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TASK__ADDED = 4;

	/**
	 * The number of structural features of the '<em>User Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TASK_FEATURE_COUNT = 5;

	/**
	 * The operation id for the '<em>Is Completed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TASK___IS_COMPLETED = 0;

	/**
	 * The operation id for the '<em>Activate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TASK___ACTIVATE = 1;

	/**
	 * The operation id for the '<em>Get User</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TASK___GET_USER = 2;

	/**
	 * The operation id for the '<em>Is Started</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TASK___IS_STARTED = 3;

	/**
	 * The operation id for the '<em>Reveal Next Hint</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TASK___REVEAL_NEXT_HINT = 4;

	/**
	 * The number of operations of the '<em>User Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TASK_OPERATION_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MQuest <em>Quest</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MQuest
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getQuest()
	 * @generated
	 */
	int QUEST = 7;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEST__TASKS = 0;

	/**
	 * The feature id for the '<em><b>Skills</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEST__SKILLS = 1;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEST__TITLE = 2;

	/**
	 * The feature id for the '<em><b>User Titles</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEST__USER_TITLES = 3;

	/**
	 * The number of structural features of the '<em>Quest</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEST_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Quest</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MDependency <em>Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MDependency
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getDependency()
	 * @generated
	 */
	int DEPENDENCY = 8;

	/**
	 * The feature id for the '<em><b>Fulfilled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__FULFILLED = 0;

	/**
	 * The number of structural features of the '<em>Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Activate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY___ACTIVATE = 0;

	/**
	 * The operation id for the '<em>Deactivate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY___DEACTIVATE = 1;

	/**
	 * The number of operations of the '<em>Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MBadgeReward <em>Badge Reward</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MBadgeReward
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getBadgeReward()
	 * @generated
	 */
	int BADGE_REWARD = 9;

	/**
	 * The feature id for the '<em><b>Badge</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGE_REWARD__BADGE = REWARD_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Badge Reward</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGE_REWARD_FEATURE_COUNT = REWARD_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Pay Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGE_REWARD___PAY_OUT__IUSER = REWARD___PAY_OUT__IUSER;

	/**
	 * The number of operations of the '<em>Badge Reward</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGE_REWARD_OPERATION_COUNT = REWARD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MExperienceReward <em>Experience Reward</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MExperienceReward
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getExperienceReward()
	 * @generated
	 */
	int EXPERIENCE_REWARD = 10;

	/**
	 * The feature id for the '<em><b>Experience</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPERIENCE_REWARD__EXPERIENCE = REWARD_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Experience Reward</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPERIENCE_REWARD_FEATURE_COUNT = REWARD_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Pay Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPERIENCE_REWARD___PAY_OUT__IUSER = REWARD___PAY_OUT__IUSER;

	/**
	 * The number of operations of the '<em>Experience Reward</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPERIENCE_REWARD_OPERATION_COUNT = REWARD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MSkillReward <em>Skill Reward</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MSkillReward
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getSkillReward()
	 * @generated
	 */
	int SKILL_REWARD = 11;

	/**
	 * The feature id for the '<em><b>Experience</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_REWARD__EXPERIENCE = EXPERIENCE_REWARD__EXPERIENCE;

	/**
	 * The feature id for the '<em><b>Skill</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_REWARD__SKILL = EXPERIENCE_REWARD_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Skill Reward</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_REWARD_FEATURE_COUNT = EXPERIENCE_REWARD_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Pay Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_REWARD___PAY_OUT__IUSER = EXPERIENCE_REWARD___PAY_OUT__IUSER;

	/**
	 * The number of operations of the '<em>Skill Reward</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_REWARD_OPERATION_COUNT = EXPERIENCE_REWARD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MContainedDependency <em>Contained Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MContainedDependency
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getContainedDependency()
	 * @generated
	 */
	int CONTAINED_DEPENDENCY = 12;

	/**
	 * The feature id for the '<em><b>Fulfilled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_DEPENDENCY__FULFILLED = DEPENDENCY__FULFILLED;

	/**
	 * The feature id for the '<em><b>Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_DEPENDENCY__DEPENDENCIES = DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Contained Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_DEPENDENCY_FEATURE_COUNT = DEPENDENCY_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Activate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_DEPENDENCY___ACTIVATE = DEPENDENCY___ACTIVATE;

	/**
	 * The operation id for the '<em>Deactivate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_DEPENDENCY___DEACTIVATE = DEPENDENCY___DEACTIVATE;

	/**
	 * The operation id for the '<em>Evaluate Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_DEPENDENCY___EVALUATE_DEPENDENCIES = DEPENDENCY_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Contained Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_DEPENDENCY_OPERATION_COUNT = DEPENDENCY_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MAndDependency <em>And Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MAndDependency
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getAndDependency()
	 * @generated
	 */
	int AND_DEPENDENCY = 13;

	/**
	 * The feature id for the '<em><b>Fulfilled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_DEPENDENCY__FULFILLED = CONTAINED_DEPENDENCY__FULFILLED;

	/**
	 * The feature id for the '<em><b>Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_DEPENDENCY__DEPENDENCIES = CONTAINED_DEPENDENCY__DEPENDENCIES;

	/**
	 * The number of structural features of the '<em>And Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_DEPENDENCY_FEATURE_COUNT = CONTAINED_DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Activate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_DEPENDENCY___ACTIVATE = CONTAINED_DEPENDENCY___ACTIVATE;

	/**
	 * The operation id for the '<em>Deactivate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_DEPENDENCY___DEACTIVATE = CONTAINED_DEPENDENCY___DEACTIVATE;

	/**
	 * The operation id for the '<em>Evaluate Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_DEPENDENCY___EVALUATE_DEPENDENCIES = CONTAINED_DEPENDENCY___EVALUATE_DEPENDENCIES;

	/**
	 * The number of operations of the '<em>And Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_DEPENDENCY_OPERATION_COUNT = CONTAINED_DEPENDENCY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MOrDependency <em>Or Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MOrDependency
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getOrDependency()
	 * @generated
	 */
	int OR_DEPENDENCY = 14;

	/**
	 * The feature id for the '<em><b>Fulfilled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_DEPENDENCY__FULFILLED = CONTAINED_DEPENDENCY__FULFILLED;

	/**
	 * The feature id for the '<em><b>Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_DEPENDENCY__DEPENDENCIES = CONTAINED_DEPENDENCY__DEPENDENCIES;

	/**
	 * The number of structural features of the '<em>Or Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_DEPENDENCY_FEATURE_COUNT = CONTAINED_DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Activate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_DEPENDENCY___ACTIVATE = CONTAINED_DEPENDENCY___ACTIVATE;

	/**
	 * The operation id for the '<em>Deactivate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_DEPENDENCY___DEACTIVATE = CONTAINED_DEPENDENCY___DEACTIVATE;

	/**
	 * The operation id for the '<em>Evaluate Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_DEPENDENCY___EVALUATE_DEPENDENCIES = CONTAINED_DEPENDENCY___EVALUATE_DEPENDENCIES;

	/**
	 * The number of operations of the '<em>Or Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_DEPENDENCY_OPERATION_COUNT = CONTAINED_DEPENDENCY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MNotDependency <em>Not Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MNotDependency
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getNotDependency()
	 * @generated
	 */
	int NOT_DEPENDENCY = 15;

	/**
	 * The feature id for the '<em><b>Fulfilled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_DEPENDENCY__FULFILLED = CONTAINED_DEPENDENCY__FULFILLED;

	/**
	 * The feature id for the '<em><b>Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_DEPENDENCY__DEPENDENCIES = CONTAINED_DEPENDENCY__DEPENDENCIES;

	/**
	 * The number of structural features of the '<em>Not Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_DEPENDENCY_FEATURE_COUNT = CONTAINED_DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Activate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_DEPENDENCY___ACTIVATE = CONTAINED_DEPENDENCY___ACTIVATE;

	/**
	 * The operation id for the '<em>Deactivate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_DEPENDENCY___DEACTIVATE = CONTAINED_DEPENDENCY___DEACTIVATE;

	/**
	 * The operation id for the '<em>Evaluate Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_DEPENDENCY___EVALUATE_DEPENDENCIES = CONTAINED_DEPENDENCY___EVALUATE_DEPENDENCIES;

	/**
	 * The number of operations of the '<em>Not Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_DEPENDENCY_OPERATION_COUNT = CONTAINED_DEPENDENCY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MDelayedDependency <em>Delayed Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MDelayedDependency
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getDelayedDependency()
	 * @generated
	 */
	int DELAYED_DEPENDENCY = 16;

	/**
	 * The feature id for the '<em><b>Fulfilled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAYED_DEPENDENCY__FULFILLED = CONTAINED_DEPENDENCY__FULFILLED;

	/**
	 * The feature id for the '<em><b>Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAYED_DEPENDENCY__DEPENDENCIES = CONTAINED_DEPENDENCY__DEPENDENCIES;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAYED_DEPENDENCY__DELAY = CONTAINED_DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Delayed Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAYED_DEPENDENCY_FEATURE_COUNT = CONTAINED_DEPENDENCY_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Activate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAYED_DEPENDENCY___ACTIVATE = CONTAINED_DEPENDENCY___ACTIVATE;

	/**
	 * The operation id for the '<em>Deactivate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAYED_DEPENDENCY___DEACTIVATE = CONTAINED_DEPENDENCY___DEACTIVATE;

	/**
	 * The operation id for the '<em>Evaluate Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAYED_DEPENDENCY___EVALUATE_DEPENDENCIES = CONTAINED_DEPENDENCY___EVALUATE_DEPENDENCIES;

	/**
	 * The number of operations of the '<em>Delayed Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAYED_DEPENDENCY_OPERATION_COUNT = CONTAINED_DEPENDENCY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MCustomDependency <em>Custom Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MCustomDependency
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getCustomDependency()
	 * @generated
	 */
	int CUSTOM_DEPENDENCY = 17;

	/**
	 * The feature id for the '<em><b>Fulfilled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_DEPENDENCY__FULFILLED = DEPENDENCY__FULFILLED;

	/**
	 * The feature id for the '<em><b>Extension Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_DEPENDENCY__EXTENSION_ID = DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Extension Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_DEPENDENCY__EXTENSION_ATTRIBUTE = DEPENDENCY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Dependency</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_DEPENDENCY__DEPENDENCY = DEPENDENCY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Definition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_DEPENDENCY__DEFINITION = DEPENDENCY_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Custom Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_DEPENDENCY_FEATURE_COUNT = DEPENDENCY_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Activate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_DEPENDENCY___ACTIVATE = DEPENDENCY___ACTIVATE;

	/**
	 * The operation id for the '<em>Deactivate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_DEPENDENCY___DEACTIVATE = DEPENDENCY___DEACTIVATE;

	/**
	 * The number of operations of the '<em>Custom Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_DEPENDENCY_OPERATION_COUNT = DEPENDENCY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.IDependencyWithAttributes <em>Dependency With Attributes</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.IDependencyWithAttributes
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getDependencyWithAttributes()
	 * @generated
	 */
	int DEPENDENCY_WITH_ATTRIBUTES = 18;

	/**
	 * The feature id for the '<em><b>Fulfilled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_WITH_ATTRIBUTES__FULFILLED = DEPENDENCY__FULFILLED;

	/**
	 * The number of structural features of the '<em>Dependency With Attributes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_WITH_ATTRIBUTES_FEATURE_COUNT = DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Activate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_WITH_ATTRIBUTES___ACTIVATE = DEPENDENCY___ACTIVATE;

	/**
	 * The operation id for the '<em>Deactivate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_WITH_ATTRIBUTES___DEACTIVATE = DEPENDENCY___DEACTIVATE;

	/**
	 * The operation id for the '<em>Set Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_WITH_ATTRIBUTES___SET_ATTRIBUTES__STRING = DEPENDENCY_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dependency With Attributes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_WITH_ATTRIBUTES_OPERATION_COUNT = DEPENDENCY_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MSequenceDependency <em>Sequence Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MSequenceDependency
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getSequenceDependency()
	 * @generated
	 */
	int SEQUENCE_DEPENDENCY = 19;

	/**
	 * The feature id for the '<em><b>Fulfilled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_DEPENDENCY__FULFILLED = AND_DEPENDENCY__FULFILLED;

	/**
	 * The feature id for the '<em><b>Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_DEPENDENCY__DEPENDENCIES = AND_DEPENDENCY__DEPENDENCIES;

	/**
	 * The feature id for the '<em><b>Completed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_DEPENDENCY__COMPLETED = AND_DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sequence Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_DEPENDENCY_FEATURE_COUNT = AND_DEPENDENCY_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Activate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_DEPENDENCY___ACTIVATE = AND_DEPENDENCY___ACTIVATE;

	/**
	 * The operation id for the '<em>Deactivate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_DEPENDENCY___DEACTIVATE = AND_DEPENDENCY___DEACTIVATE;

	/**
	 * The operation id for the '<em>Evaluate Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_DEPENDENCY___EVALUATE_DEPENDENCIES = AND_DEPENDENCY___EVALUATE_DEPENDENCIES;

	/**
	 * The number of operations of the '<em>Sequence Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_DEPENDENCY_OPERATION_COUNT = AND_DEPENDENCY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MUserDependency <em>User Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MUserDependency
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getUserDependency()
	 * @generated
	 */
	int USER_DEPENDENCY = 24;

	/**
	 * The feature id for the '<em><b>Fulfilled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEPENDENCY__FULFILLED = DEPENDENCY__FULFILLED;

	/**
	 * The feature id for the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEPENDENCY__USER = DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>User Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEPENDENCY_FEATURE_COUNT = DEPENDENCY_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Activate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEPENDENCY___ACTIVATE = DEPENDENCY___ACTIVATE;

	/**
	 * The operation id for the '<em>Deactivate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEPENDENCY___DEACTIVATE = DEPENDENCY___DEACTIVATE;

	/**
	 * The number of operations of the '<em>User Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEPENDENCY_OPERATION_COUNT = DEPENDENCY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MCompleteIncludedTasksDependency <em>Complete Included Tasks Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MCompleteIncludedTasksDependency
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getCompleteIncludedTasksDependency()
	 * @generated
	 */
	int COMPLETE_INCLUDED_TASKS_DEPENDENCY = 20;

	/**
	 * The feature id for the '<em><b>Fulfilled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLETE_INCLUDED_TASKS_DEPENDENCY__FULFILLED = USER_DEPENDENCY__FULFILLED;

	/**
	 * The feature id for the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLETE_INCLUDED_TASKS_DEPENDENCY__USER = USER_DEPENDENCY__USER;

	/**
	 * The number of structural features of the '<em>Complete Included Tasks Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLETE_INCLUDED_TASKS_DEPENDENCY_FEATURE_COUNT = USER_DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Activate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLETE_INCLUDED_TASKS_DEPENDENCY___ACTIVATE = USER_DEPENDENCY___ACTIVATE;

	/**
	 * The operation id for the '<em>Deactivate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLETE_INCLUDED_TASKS_DEPENDENCY___DEACTIVATE = USER_DEPENDENCY___DEACTIVATE;

	/**
	 * The number of operations of the '<em>Complete Included Tasks Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLETE_INCLUDED_TASKS_DEPENDENCY_OPERATION_COUNT = USER_DEPENDENCY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MTaskDependency <em>Task Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MTaskDependency
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getTaskDependency()
	 * @generated
	 */
	int TASK_DEPENDENCY = 21;

	/**
	 * The feature id for the '<em><b>Fulfilled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY__FULFILLED = USER_DEPENDENCY__FULFILLED;

	/**
	 * The feature id for the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY__USER = USER_DEPENDENCY__USER;

	/**
	 * The feature id for the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY__TASK = USER_DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Completed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY__COMPLETED = USER_DEPENDENCY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Task Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY_FEATURE_COUNT = USER_DEPENDENCY_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Activate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY___ACTIVATE = USER_DEPENDENCY___ACTIVATE;

	/**
	 * The operation id for the '<em>Deactivate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY___DEACTIVATE = USER_DEPENDENCY___DEACTIVATE;

	/**
	 * The number of operations of the '<em>Task Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY_OPERATION_COUNT = USER_DEPENDENCY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MSkillDependency <em>Skill Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MSkillDependency
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getSkillDependency()
	 * @generated
	 */
	int SKILL_DEPENDENCY = 22;

	/**
	 * The feature id for the '<em><b>Fulfilled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEPENDENCY__FULFILLED = USER_DEPENDENCY__FULFILLED;

	/**
	 * The feature id for the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEPENDENCY__USER = USER_DEPENDENCY__USER;

	/**
	 * The feature id for the '<em><b>Min Experience</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEPENDENCY__MIN_EXPERIENCE = USER_DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Min Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEPENDENCY__MIN_LEVEL = USER_DEPENDENCY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Skill</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEPENDENCY__SKILL = USER_DEPENDENCY_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Skill Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEPENDENCY_FEATURE_COUNT = USER_DEPENDENCY_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Activate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEPENDENCY___ACTIVATE = USER_DEPENDENCY___ACTIVATE;

	/**
	 * The operation id for the '<em>Deactivate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEPENDENCY___DEACTIVATE = USER_DEPENDENCY___DEACTIVATE;

	/**
	 * The number of operations of the '<em>Skill Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEPENDENCY_OPERATION_COUNT = USER_DEPENDENCY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MHint <em>Hint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MHint
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getHint()
	 * @generated
	 */
	int HINT = 23;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HINT__TEXT = DESCRIPTION__TEXT;

	/**
	 * The feature id for the '<em><b>Penalty</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HINT__PENALTY = DESCRIPTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Hint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HINT_FEATURE_COUNT = DESCRIPTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Hint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HINT_OPERATION_COUNT = DESCRIPTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.ILevelProgression <em>Level Progression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.ILevelProgression
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getLevelProgression()
	 * @generated
	 */
	int LEVEL_PROGRESSION = 25;

	/**
	 * The number of structural features of the '<em>Level Progression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEVEL_PROGRESSION_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Minimum Xp For Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEVEL_PROGRESSION___GET_MINIMUM_XP_FOR_LEVEL__INT = 0;

	/**
	 * The operation id for the '<em>Get Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEVEL_PROGRESSION___GET_LEVEL__INT = 1;

	/**
	 * The number of operations of the '<em>Level Progression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEVEL_PROGRESSION_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MFactorProgression <em>Factor Progression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MFactorProgression
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getFactorProgression()
	 * @generated
	 */
	int FACTOR_PROGRESSION = 26;

	/**
	 * The feature id for the '<em><b>Base Xp Needed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACTOR_PROGRESSION__BASE_XP_NEEDED = LEVEL_PROGRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Xp Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACTOR_PROGRESSION__XP_FACTOR = LEVEL_PROGRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Factor Progression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACTOR_PROGRESSION_FEATURE_COUNT = LEVEL_PROGRESSION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Minimum Xp For Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACTOR_PROGRESSION___GET_MINIMUM_XP_FOR_LEVEL__INT = LEVEL_PROGRESSION___GET_MINIMUM_XP_FOR_LEVEL__INT;

	/**
	 * The operation id for the '<em>Get Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACTOR_PROGRESSION___GET_LEVEL__INT = LEVEL_PROGRESSION___GET_LEVEL__INT;

	/**
	 * The number of operations of the '<em>Factor Progression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACTOR_PROGRESSION_OPERATION_COUNT = LEVEL_PROGRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.impl.MUserTitle <em>User Title</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.impl.MUserTitle
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getUserTitle()
	 * @generated
	 */
	int USER_TITLE = 27;

	/**
	 * The feature id for the '<em><b>Min Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TITLE__MIN_LEVEL = 0;

	/**
	 * The feature id for the '<em><b>Skill</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TITLE__SKILL = 1;

	/**
	 * The feature id for the '<em><b>Display String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TITLE__DISPLAY_STRING = 2;

	/**
	 * The number of structural features of the '<em>User Title</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TITLE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>User Title</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TITLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.skills.model.LevelName <em>Level Name</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.model.LevelName
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getLevelName()
	 * @generated
	 */
	int LEVEL_NAME = 28;

	/**
	 * The meta object id for the '<em>Date</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.Date
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getDate()
	 * @generated
	 */
	int DATE = 29;


	/**
	 * The meta object id for the '<em>Custom Dependency Definition</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.dependencies.CustomDependencyDefinition
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getCustomDependencyDefinition()
	 * @generated
	 */
	int CUSTOM_DEPENDENCY_DEFINITION = 30;


	/**
	 * The meta object id for the '<em>Image Descriptor</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.jface.resource.ImageDescriptor
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getImageDescriptor()
	 * @generated
	 */
	int IMAGE_DESCRIPTOR = 31;

	/**
	 * The meta object id for the '<em>ISkill Service</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.skills.service.ISkillService
	 * @see org.eclipse.skills.model.impl.MSkillsPackage#getISkillService()
	 * @generated
	 */
	int ISKILL_SERVICE = 32;


	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.ITask <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task</em>'.
	 * @see org.eclipse.skills.model.ITask
	 * @generated
	 */
	EClass getTask();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.skills.model.ITask#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Description</em>'.
	 * @see org.eclipse.skills.model.ITask#getDescription()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Description();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.skills.model.ITask#getGoal <em>Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Goal</em>'.
	 * @see org.eclipse.skills.model.ITask#getGoal()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Goal();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.skills.model.ITask#getRewards <em>Rewards</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rewards</em>'.
	 * @see org.eclipse.skills.model.ITask#getRewards()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Rewards();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.ITask#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see org.eclipse.skills.model.ITask#getTitle()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_Title();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.skills.model.ITask#getRequirement <em>Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Requirement</em>'.
	 * @see org.eclipse.skills.model.ITask#getRequirement()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Requirement();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.skills.model.ITask#getTasks <em>Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tasks</em>'.
	 * @see org.eclipse.skills.model.ITask#getTasks()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Tasks();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.skills.model.ITask#getHints <em>Hints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Hints</em>'.
	 * @see org.eclipse.skills.model.ITask#getHints()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Hints();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.ITask#isAutoActivation <em>Auto Activation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Auto Activation</em>'.
	 * @see org.eclipse.skills.model.ITask#isAutoActivation()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_AutoActivation();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.ITask#getSkillService <em>Skill Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Skill Service</em>'.
	 * @see org.eclipse.skills.model.ITask#getSkillService()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_SkillService();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.ITask#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.eclipse.skills.model.ITask#getId()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_Id();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.ITask#isAvailable() <em>Is Available</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Available</em>' operation.
	 * @see org.eclipse.skills.model.ITask#isAvailable()
	 * @generated
	 */
	EOperation getTask__IsAvailable();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.ITask#getShortTitle(int) <em>Get Short Title</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Short Title</em>' operation.
	 * @see org.eclipse.skills.model.ITask#getShortTitle(int)
	 * @generated
	 */
	EOperation getTask__GetShortTitle__int();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Description</em>'.
	 * @see org.eclipse.skills.model.IDescription
	 * @generated
	 */
	EClass getDescription();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IDescription#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see org.eclipse.skills.model.IDescription#getText()
	 * @see #getDescription()
	 * @generated
	 */
	EAttribute getDescription_Text();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IReward <em>Reward</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reward</em>'.
	 * @see org.eclipse.skills.model.IReward
	 * @generated
	 */
	EClass getReward();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.IReward#payOut(org.eclipse.skills.model.IUser) <em>Pay Out</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Out</em>' operation.
	 * @see org.eclipse.skills.model.IReward#payOut(org.eclipse.skills.model.IUser)
	 * @generated
	 */
	EOperation getReward__PayOut__IUser();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IBadge <em>Badge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Badge</em>'.
	 * @see org.eclipse.skills.model.IBadge
	 * @generated
	 */
	EClass getBadge();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IBadge#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see org.eclipse.skills.model.IBadge#getTitle()
	 * @see #getBadge()
	 * @generated
	 */
	EAttribute getBadge_Title();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IBadge#getImageURL <em>Image URL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Image URL</em>'.
	 * @see org.eclipse.skills.model.IBadge#getImageURL()
	 * @see #getBadge()
	 * @generated
	 */
	EAttribute getBadge_ImageURL();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.IBadge#getImageDescriptor() <em>Get Image Descriptor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Image Descriptor</em>' operation.
	 * @see org.eclipse.skills.model.IBadge#getImageDescriptor()
	 * @generated
	 */
	EOperation getBadge__GetImageDescriptor();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IUser <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User</em>'.
	 * @see org.eclipse.skills.model.IUser
	 * @generated
	 */
	EClass getUser();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IUser#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.skills.model.IUser#getName()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.skills.model.IUser#getSkills <em>Skills</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Skills</em>'.
	 * @see org.eclipse.skills.model.IUser#getSkills()
	 * @see #getUser()
	 * @generated
	 */
	EReference getUser_Skills();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.skills.model.IUser#getUsertasks <em>Usertasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Usertasks</em>'.
	 * @see org.eclipse.skills.model.IUser#getUsertasks()
	 * @see #getUser()
	 * @generated
	 */
	EReference getUser_Usertasks();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IUser#getImageLocation <em>Image Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Image Location</em>'.
	 * @see org.eclipse.skills.model.IUser#getImageLocation()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_ImageLocation();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.skills.model.IUser#getExperience <em>Experience</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Experience</em>'.
	 * @see org.eclipse.skills.model.IUser#getExperience()
	 * @see #getUser()
	 * @generated
	 */
	EReference getUser_Experience();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.skills.model.IUser#getBadges <em>Badges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Badges</em>'.
	 * @see org.eclipse.skills.model.IUser#getBadges()
	 * @see #getUser()
	 * @generated
	 */
	EReference getUser_Badges();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IUser#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see org.eclipse.skills.model.IUser#getTitle()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_Title();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.IUser#addTask(org.eclipse.skills.model.ITask) <em>Add Task</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Task</em>' operation.
	 * @see org.eclipse.skills.model.IUser#addTask(org.eclipse.skills.model.ITask)
	 * @generated
	 */
	EOperation getUser__AddTask__ITask();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.IUser#getSkill(org.eclipse.skills.model.ISkill) <em>Get Skill</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Skill</em>' operation.
	 * @see org.eclipse.skills.model.IUser#getSkill(org.eclipse.skills.model.ISkill)
	 * @generated
	 */
	EOperation getUser__GetSkill__ISkill();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.IUser#consume(org.eclipse.skills.model.IReward) <em>Consume</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Consume</em>' operation.
	 * @see org.eclipse.skills.model.IUser#consume(org.eclipse.skills.model.IReward)
	 * @generated
	 */
	EOperation getUser__Consume__IReward();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.IUser#getAvatar() <em>Get Avatar</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Avatar</em>' operation.
	 * @see org.eclipse.skills.model.IUser#getAvatar()
	 * @generated
	 */
	EOperation getUser__GetAvatar();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.IUser#getSkill(java.lang.String) <em>Get Skill</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Skill</em>' operation.
	 * @see org.eclipse.skills.model.IUser#getSkill(java.lang.String)
	 * @generated
	 */
	EOperation getUser__GetSkill__String();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.ISkill <em>Skill</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Skill</em>'.
	 * @see org.eclipse.skills.model.ISkill
	 * @generated
	 */
	EClass getSkill();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.ISkill#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.skills.model.ISkill#getName()
	 * @see #getSkill()
	 * @generated
	 */
	EAttribute getSkill_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.skills.model.ISkill#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Description</em>'.
	 * @see org.eclipse.skills.model.ISkill#getDescription()
	 * @see #getSkill()
	 * @generated
	 */
	EReference getSkill_Description();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.ISkill#getExperience <em>Experience</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Experience</em>'.
	 * @see org.eclipse.skills.model.ISkill#getExperience()
	 * @see #getSkill()
	 * @generated
	 */
	EAttribute getSkill_Experience();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.skills.model.ISkill#getProgression <em>Progression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Progression</em>'.
	 * @see org.eclipse.skills.model.ISkill#getProgression()
	 * @see #getSkill()
	 * @generated
	 */
	EReference getSkill_Progression();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.ISkill#isBaseSkill <em>Base Skill</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base Skill</em>'.
	 * @see org.eclipse.skills.model.ISkill#isBaseSkill()
	 * @see #getSkill()
	 * @generated
	 */
	EAttribute getSkill_BaseSkill();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.ISkill#getImageURI <em>Image URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Image URI</em>'.
	 * @see org.eclipse.skills.model.ISkill#getImageURI()
	 * @see #getSkill()
	 * @generated
	 */
	EAttribute getSkill_ImageURI();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.ISkill#addExperience(int) <em>Add Experience</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Experience</em>' operation.
	 * @see org.eclipse.skills.model.ISkill#addExperience(int)
	 * @generated
	 */
	EOperation getSkill__AddExperience__int();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.ISkill#getImageDescriptor() <em>Get Image Descriptor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Image Descriptor</em>' operation.
	 * @see org.eclipse.skills.model.ISkill#getImageDescriptor()
	 * @generated
	 */
	EOperation getSkill__GetImageDescriptor();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IUserTask <em>User Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Task</em>'.
	 * @see org.eclipse.skills.model.IUserTask
	 * @generated
	 */
	EClass getUserTask();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.skills.model.IUserTask#getTask <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Task</em>'.
	 * @see org.eclipse.skills.model.IUserTask#getTask()
	 * @see #getUserTask()
	 * @generated
	 */
	EReference getUserTask_Task();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IUserTask#getStarted <em>Started</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Started</em>'.
	 * @see org.eclipse.skills.model.IUserTask#getStarted()
	 * @see #getUserTask()
	 * @generated
	 */
	EAttribute getUserTask_Started();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IUserTask#getFinished <em>Finished</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Finished</em>'.
	 * @see org.eclipse.skills.model.IUserTask#getFinished()
	 * @see #getUserTask()
	 * @generated
	 */
	EAttribute getUserTask_Finished();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IUserTask#getHintsDisplayed <em>Hints Displayed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hints Displayed</em>'.
	 * @see org.eclipse.skills.model.IUserTask#getHintsDisplayed()
	 * @see #getUserTask()
	 * @generated
	 */
	EAttribute getUserTask_HintsDisplayed();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IUserTask#getAdded <em>Added</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Added</em>'.
	 * @see org.eclipse.skills.model.IUserTask#getAdded()
	 * @see #getUserTask()
	 * @generated
	 */
	EAttribute getUserTask_Added();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.IUserTask#isCompleted() <em>Is Completed</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Completed</em>' operation.
	 * @see org.eclipse.skills.model.IUserTask#isCompleted()
	 * @generated
	 */
	EOperation getUserTask__IsCompleted();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.IUserTask#activate() <em>Activate</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Activate</em>' operation.
	 * @see org.eclipse.skills.model.IUserTask#activate()
	 * @generated
	 */
	EOperation getUserTask__Activate();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.IUserTask#getUser() <em>Get User</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get User</em>' operation.
	 * @see org.eclipse.skills.model.IUserTask#getUser()
	 * @generated
	 */
	EOperation getUserTask__GetUser();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.IUserTask#isStarted() <em>Is Started</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Started</em>' operation.
	 * @see org.eclipse.skills.model.IUserTask#isStarted()
	 * @generated
	 */
	EOperation getUserTask__IsStarted();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.IUserTask#revealNextHint() <em>Reveal Next Hint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Reveal Next Hint</em>' operation.
	 * @see org.eclipse.skills.model.IUserTask#revealNextHint()
	 * @generated
	 */
	EOperation getUserTask__RevealNextHint();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IQuest <em>Quest</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Quest</em>'.
	 * @see org.eclipse.skills.model.IQuest
	 * @generated
	 */
	EClass getQuest();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.skills.model.IQuest#getTasks <em>Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tasks</em>'.
	 * @see org.eclipse.skills.model.IQuest#getTasks()
	 * @see #getQuest()
	 * @generated
	 */
	EReference getQuest_Tasks();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.skills.model.IQuest#getSkills <em>Skills</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Skills</em>'.
	 * @see org.eclipse.skills.model.IQuest#getSkills()
	 * @see #getQuest()
	 * @generated
	 */
	EReference getQuest_Skills();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IQuest#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see org.eclipse.skills.model.IQuest#getTitle()
	 * @see #getQuest()
	 * @generated
	 */
	EAttribute getQuest_Title();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.skills.model.IQuest#getUserTitles <em>User Titles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>User Titles</em>'.
	 * @see org.eclipse.skills.model.IQuest#getUserTitles()
	 * @see #getQuest()
	 * @generated
	 */
	EReference getQuest_UserTitles();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IDependency <em>Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dependency</em>'.
	 * @see org.eclipse.skills.model.IDependency
	 * @generated
	 */
	EClass getDependency();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IDependency#isFulfilled <em>Fulfilled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fulfilled</em>'.
	 * @see org.eclipse.skills.model.IDependency#isFulfilled()
	 * @see #getDependency()
	 * @generated
	 */
	EAttribute getDependency_Fulfilled();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.IDependency#activate() <em>Activate</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Activate</em>' operation.
	 * @see org.eclipse.skills.model.IDependency#activate()
	 * @generated
	 */
	EOperation getDependency__Activate();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.IDependency#deactivate() <em>Deactivate</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Deactivate</em>' operation.
	 * @see org.eclipse.skills.model.IDependency#deactivate()
	 * @generated
	 */
	EOperation getDependency__Deactivate();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IBadgeReward <em>Badge Reward</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Badge Reward</em>'.
	 * @see org.eclipse.skills.model.IBadgeReward
	 * @generated
	 */
	EClass getBadgeReward();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.skills.model.IBadgeReward#getBadge <em>Badge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Badge</em>'.
	 * @see org.eclipse.skills.model.IBadgeReward#getBadge()
	 * @see #getBadgeReward()
	 * @generated
	 */
	EReference getBadgeReward_Badge();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IExperienceReward <em>Experience Reward</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Experience Reward</em>'.
	 * @see org.eclipse.skills.model.IExperienceReward
	 * @generated
	 */
	EClass getExperienceReward();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IExperienceReward#getExperience <em>Experience</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Experience</em>'.
	 * @see org.eclipse.skills.model.IExperienceReward#getExperience()
	 * @see #getExperienceReward()
	 * @generated
	 */
	EAttribute getExperienceReward_Experience();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.ISkillReward <em>Skill Reward</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Skill Reward</em>'.
	 * @see org.eclipse.skills.model.ISkillReward
	 * @generated
	 */
	EClass getSkillReward();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.skills.model.ISkillReward#getSkill <em>Skill</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Skill</em>'.
	 * @see org.eclipse.skills.model.ISkillReward#getSkill()
	 * @see #getSkillReward()
	 * @generated
	 */
	EReference getSkillReward_Skill();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IContainedDependency <em>Contained Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contained Dependency</em>'.
	 * @see org.eclipse.skills.model.IContainedDependency
	 * @generated
	 */
	EClass getContainedDependency();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.skills.model.IContainedDependency#getDependencies <em>Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dependencies</em>'.
	 * @see org.eclipse.skills.model.IContainedDependency#getDependencies()
	 * @see #getContainedDependency()
	 * @generated
	 */
	EReference getContainedDependency_Dependencies();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.IContainedDependency#evaluateDependencies() <em>Evaluate Dependencies</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Evaluate Dependencies</em>' operation.
	 * @see org.eclipse.skills.model.IContainedDependency#evaluateDependencies()
	 * @generated
	 */
	EOperation getContainedDependency__EvaluateDependencies();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IAndDependency <em>And Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>And Dependency</em>'.
	 * @see org.eclipse.skills.model.IAndDependency
	 * @generated
	 */
	EClass getAndDependency();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IOrDependency <em>Or Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Or Dependency</em>'.
	 * @see org.eclipse.skills.model.IOrDependency
	 * @generated
	 */
	EClass getOrDependency();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.INotDependency <em>Not Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Not Dependency</em>'.
	 * @see org.eclipse.skills.model.INotDependency
	 * @generated
	 */
	EClass getNotDependency();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IDelayedDependency <em>Delayed Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Delayed Dependency</em>'.
	 * @see org.eclipse.skills.model.IDelayedDependency
	 * @generated
	 */
	EClass getDelayedDependency();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IDelayedDependency#getDelay <em>Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delay</em>'.
	 * @see org.eclipse.skills.model.IDelayedDependency#getDelay()
	 * @see #getDelayedDependency()
	 * @generated
	 */
	EAttribute getDelayedDependency_Delay();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.ICustomDependency <em>Custom Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Custom Dependency</em>'.
	 * @see org.eclipse.skills.model.ICustomDependency
	 * @generated
	 */
	EClass getCustomDependency();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.ICustomDependency#getExtensionId <em>Extension Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Extension Id</em>'.
	 * @see org.eclipse.skills.model.ICustomDependency#getExtensionId()
	 * @see #getCustomDependency()
	 * @generated
	 */
	EAttribute getCustomDependency_ExtensionId();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.ICustomDependency#getExtensionAttribute <em>Extension Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Extension Attribute</em>'.
	 * @see org.eclipse.skills.model.ICustomDependency#getExtensionAttribute()
	 * @see #getCustomDependency()
	 * @generated
	 */
	EAttribute getCustomDependency_ExtensionAttribute();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.skills.model.ICustomDependency#getDependency <em>Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dependency</em>'.
	 * @see org.eclipse.skills.model.ICustomDependency#getDependency()
	 * @see #getCustomDependency()
	 * @generated
	 */
	EReference getCustomDependency_Dependency();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.ICustomDependency#getDefinition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Definition</em>'.
	 * @see org.eclipse.skills.model.ICustomDependency#getDefinition()
	 * @see #getCustomDependency()
	 * @generated
	 */
	EAttribute getCustomDependency_Definition();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IDependencyWithAttributes <em>Dependency With Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dependency With Attributes</em>'.
	 * @see org.eclipse.skills.model.IDependencyWithAttributes
	 * @generated
	 */
	EClass getDependencyWithAttributes();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.IDependencyWithAttributes#setAttributes(java.lang.String) <em>Set Attributes</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Attributes</em>' operation.
	 * @see org.eclipse.skills.model.IDependencyWithAttributes#setAttributes(java.lang.String)
	 * @generated
	 */
	EOperation getDependencyWithAttributes__SetAttributes__String();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.ISequenceDependency <em>Sequence Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequence Dependency</em>'.
	 * @see org.eclipse.skills.model.ISequenceDependency
	 * @generated
	 */
	EClass getSequenceDependency();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.ISequenceDependency#getCompleted <em>Completed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Completed</em>'.
	 * @see org.eclipse.skills.model.ISequenceDependency#getCompleted()
	 * @see #getSequenceDependency()
	 * @generated
	 */
	EAttribute getSequenceDependency_Completed();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.ICompleteIncludedTasksDependency <em>Complete Included Tasks Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complete Included Tasks Dependency</em>'.
	 * @see org.eclipse.skills.model.ICompleteIncludedTasksDependency
	 * @generated
	 */
	EClass getCompleteIncludedTasksDependency();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.ITaskDependency <em>Task Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task Dependency</em>'.
	 * @see org.eclipse.skills.model.ITaskDependency
	 * @generated
	 */
	EClass getTaskDependency();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.skills.model.ITaskDependency#getTask <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Task</em>'.
	 * @see org.eclipse.skills.model.ITaskDependency#getTask()
	 * @see #getTaskDependency()
	 * @generated
	 */
	EReference getTaskDependency_Task();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.ITaskDependency#isCompleted <em>Completed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Completed</em>'.
	 * @see org.eclipse.skills.model.ITaskDependency#isCompleted()
	 * @see #getTaskDependency()
	 * @generated
	 */
	EAttribute getTaskDependency_Completed();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.ISkillDependency <em>Skill Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Skill Dependency</em>'.
	 * @see org.eclipse.skills.model.ISkillDependency
	 * @generated
	 */
	EClass getSkillDependency();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.ISkillDependency#getMinExperience <em>Min Experience</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Experience</em>'.
	 * @see org.eclipse.skills.model.ISkillDependency#getMinExperience()
	 * @see #getSkillDependency()
	 * @generated
	 */
	EAttribute getSkillDependency_MinExperience();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.ISkillDependency#getMinLevel <em>Min Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Level</em>'.
	 * @see org.eclipse.skills.model.ISkillDependency#getMinLevel()
	 * @see #getSkillDependency()
	 * @generated
	 */
	EAttribute getSkillDependency_MinLevel();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.skills.model.ISkillDependency#getSkill <em>Skill</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Skill</em>'.
	 * @see org.eclipse.skills.model.ISkillDependency#getSkill()
	 * @see #getSkillDependency()
	 * @generated
	 */
	EReference getSkillDependency_Skill();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IHint <em>Hint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hint</em>'.
	 * @see org.eclipse.skills.model.IHint
	 * @generated
	 */
	EClass getHint();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IHint#getPenalty <em>Penalty</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Penalty</em>'.
	 * @see org.eclipse.skills.model.IHint#getPenalty()
	 * @see #getHint()
	 * @generated
	 */
	EAttribute getHint_Penalty();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IUserDependency <em>User Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Dependency</em>'.
	 * @see org.eclipse.skills.model.IUserDependency
	 * @generated
	 */
	EClass getUserDependency();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.skills.model.IUserDependency#getUser <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>User</em>'.
	 * @see org.eclipse.skills.model.IUserDependency#getUser()
	 * @see #getUserDependency()
	 * @generated
	 */
	EReference getUserDependency_User();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.ILevelProgression <em>Level Progression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Level Progression</em>'.
	 * @see org.eclipse.skills.model.ILevelProgression
	 * @generated
	 */
	EClass getLevelProgression();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.ILevelProgression#getMinimumXpForLevel(int) <em>Get Minimum Xp For Level</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Minimum Xp For Level</em>' operation.
	 * @see org.eclipse.skills.model.ILevelProgression#getMinimumXpForLevel(int)
	 * @generated
	 */
	EOperation getLevelProgression__GetMinimumXpForLevel__int();

	/**
	 * Returns the meta object for the '{@link org.eclipse.skills.model.ILevelProgression#getLevel(int) <em>Get Level</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Level</em>' operation.
	 * @see org.eclipse.skills.model.ILevelProgression#getLevel(int)
	 * @generated
	 */
	EOperation getLevelProgression__GetLevel__int();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IFactorProgression <em>Factor Progression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Factor Progression</em>'.
	 * @see org.eclipse.skills.model.IFactorProgression
	 * @generated
	 */
	EClass getFactorProgression();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IFactorProgression#getBaseXpNeeded <em>Base Xp Needed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base Xp Needed</em>'.
	 * @see org.eclipse.skills.model.IFactorProgression#getBaseXpNeeded()
	 * @see #getFactorProgression()
	 * @generated
	 */
	EAttribute getFactorProgression_BaseXpNeeded();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IFactorProgression#getXpFactor <em>Xp Factor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Xp Factor</em>'.
	 * @see org.eclipse.skills.model.IFactorProgression#getXpFactor()
	 * @see #getFactorProgression()
	 * @generated
	 */
	EAttribute getFactorProgression_XpFactor();

	/**
	 * Returns the meta object for class '{@link org.eclipse.skills.model.IUserTitle <em>User Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Title</em>'.
	 * @see org.eclipse.skills.model.IUserTitle
	 * @generated
	 */
	EClass getUserTitle();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IUserTitle#getMinLevel <em>Min Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Level</em>'.
	 * @see org.eclipse.skills.model.IUserTitle#getMinLevel()
	 * @see #getUserTitle()
	 * @generated
	 */
	EAttribute getUserTitle_MinLevel();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.skills.model.IUserTitle#getSkill <em>Skill</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Skill</em>'.
	 * @see org.eclipse.skills.model.IUserTitle#getSkill()
	 * @see #getUserTitle()
	 * @generated
	 */
	EReference getUserTitle_Skill();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.skills.model.IUserTitle#getDisplayString <em>Display String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Display String</em>'.
	 * @see org.eclipse.skills.model.IUserTitle#getDisplayString()
	 * @see #getUserTitle()
	 * @generated
	 */
	EAttribute getUserTitle_DisplayString();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.skills.model.LevelName <em>Level Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Level Name</em>'.
	 * @see org.eclipse.skills.model.LevelName
	 * @generated
	 */
	EEnum getLevelName();

	/**
	 * Returns the meta object for data type '{@link java.util.Date <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Date</em>'.
	 * @see java.util.Date
	 * @model instanceClass="java.util.Date"
	 * @generated
	 */
	EDataType getDate();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.skills.dependencies.CustomDependencyDefinition <em>Custom Dependency Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Custom Dependency Definition</em>'.
	 * @see org.eclipse.skills.dependencies.CustomDependencyDefinition
	 * @model instanceClass="org.eclipse.skills.dependencies.CustomDependencyDefinition"
	 * @generated
	 */
	EDataType getCustomDependencyDefinition();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.jface.resource.ImageDescriptor <em>Image Descriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Image Descriptor</em>'.
	 * @see org.eclipse.jface.resource.ImageDescriptor
	 * @model instanceClass="org.eclipse.jface.resource.ImageDescriptor" serializeable="false"
	 * @generated
	 */
	EDataType getImageDescriptor();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.skills.service.ISkillService <em>ISkill Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ISkill Service</em>'.
	 * @see org.eclipse.skills.service.ISkillService
	 * @model instanceClass="org.eclipse.skills.service.ISkillService"
	 * @generated
	 */
	EDataType getISkillService();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ISkillsFactory getSkillsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MTask <em>Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MTask
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getTask()
		 * @generated
		 */
		EClass TASK = eINSTANCE.getTask();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__DESCRIPTION = eINSTANCE.getTask_Description();

		/**
		 * The meta object literal for the '<em><b>Goal</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__GOAL = eINSTANCE.getTask_Goal();

		/**
		 * The meta object literal for the '<em><b>Rewards</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__REWARDS = eINSTANCE.getTask_Rewards();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__TITLE = eINSTANCE.getTask_Title();

		/**
		 * The meta object literal for the '<em><b>Requirement</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__REQUIREMENT = eINSTANCE.getTask_Requirement();

		/**
		 * The meta object literal for the '<em><b>Tasks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__TASKS = eINSTANCE.getTask_Tasks();

		/**
		 * The meta object literal for the '<em><b>Hints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__HINTS = eINSTANCE.getTask_Hints();

		/**
		 * The meta object literal for the '<em><b>Auto Activation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__AUTO_ACTIVATION = eINSTANCE.getTask_AutoActivation();

		/**
		 * The meta object literal for the '<em><b>Skill Service</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__SKILL_SERVICE = eINSTANCE.getTask_SkillService();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__ID = eINSTANCE.getTask_Id();

		/**
		 * The meta object literal for the '<em><b>Is Available</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TASK___IS_AVAILABLE = eINSTANCE.getTask__IsAvailable();

		/**
		 * The meta object literal for the '<em><b>Get Short Title</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TASK___GET_SHORT_TITLE__INT = eINSTANCE.getTask__GetShortTitle__int();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MDescription <em>Description</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MDescription
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getDescription()
		 * @generated
		 */
		EClass DESCRIPTION = eINSTANCE.getDescription();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DESCRIPTION__TEXT = eINSTANCE.getDescription_Text();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.IReward <em>Reward</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.IReward
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getReward()
		 * @generated
		 */
		EClass REWARD = eINSTANCE.getReward();

		/**
		 * The meta object literal for the '<em><b>Pay Out</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation REWARD___PAY_OUT__IUSER = eINSTANCE.getReward__PayOut__IUser();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MBadge <em>Badge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MBadge
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getBadge()
		 * @generated
		 */
		EClass BADGE = eINSTANCE.getBadge();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BADGE__TITLE = eINSTANCE.getBadge_Title();

		/**
		 * The meta object literal for the '<em><b>Image URL</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BADGE__IMAGE_URL = eINSTANCE.getBadge_ImageURL();

		/**
		 * The meta object literal for the '<em><b>Get Image Descriptor</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BADGE___GET_IMAGE_DESCRIPTOR = eINSTANCE.getBadge__GetImageDescriptor();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MUser <em>User</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MUser
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getUser()
		 * @generated
		 */
		EClass USER = eINSTANCE.getUser();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__NAME = eINSTANCE.getUser_Name();

		/**
		 * The meta object literal for the '<em><b>Skills</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER__SKILLS = eINSTANCE.getUser_Skills();

		/**
		 * The meta object literal for the '<em><b>Usertasks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER__USERTASKS = eINSTANCE.getUser_Usertasks();

		/**
		 * The meta object literal for the '<em><b>Image Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__IMAGE_LOCATION = eINSTANCE.getUser_ImageLocation();

		/**
		 * The meta object literal for the '<em><b>Experience</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER__EXPERIENCE = eINSTANCE.getUser_Experience();

		/**
		 * The meta object literal for the '<em><b>Badges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER__BADGES = eINSTANCE.getUser_Badges();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__TITLE = eINSTANCE.getUser_Title();

		/**
		 * The meta object literal for the '<em><b>Add Task</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation USER___ADD_TASK__ITASK = eINSTANCE.getUser__AddTask__ITask();

		/**
		 * The meta object literal for the '<em><b>Get Skill</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation USER___GET_SKILL__ISKILL = eINSTANCE.getUser__GetSkill__ISkill();

		/**
		 * The meta object literal for the '<em><b>Consume</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation USER___CONSUME__IREWARD = eINSTANCE.getUser__Consume__IReward();

		/**
		 * The meta object literal for the '<em><b>Get Avatar</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation USER___GET_AVATAR = eINSTANCE.getUser__GetAvatar();

		/**
		 * The meta object literal for the '<em><b>Get Skill</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation USER___GET_SKILL__STRING = eINSTANCE.getUser__GetSkill__String();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MSkill <em>Skill</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MSkill
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getSkill()
		 * @generated
		 */
		EClass SKILL = eINSTANCE.getSkill();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKILL__NAME = eINSTANCE.getSkill_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL__DESCRIPTION = eINSTANCE.getSkill_Description();

		/**
		 * The meta object literal for the '<em><b>Experience</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKILL__EXPERIENCE = eINSTANCE.getSkill_Experience();

		/**
		 * The meta object literal for the '<em><b>Progression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL__PROGRESSION = eINSTANCE.getSkill_Progression();

		/**
		 * The meta object literal for the '<em><b>Base Skill</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKILL__BASE_SKILL = eINSTANCE.getSkill_BaseSkill();

		/**
		 * The meta object literal for the '<em><b>Image URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKILL__IMAGE_URI = eINSTANCE.getSkill_ImageURI();

		/**
		 * The meta object literal for the '<em><b>Add Experience</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SKILL___ADD_EXPERIENCE__INT = eINSTANCE.getSkill__AddExperience__int();

		/**
		 * The meta object literal for the '<em><b>Get Image Descriptor</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SKILL___GET_IMAGE_DESCRIPTOR = eINSTANCE.getSkill__GetImageDescriptor();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MUserTask <em>User Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MUserTask
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getUserTask()
		 * @generated
		 */
		EClass USER_TASK = eINSTANCE.getUserTask();

		/**
		 * The meta object literal for the '<em><b>Task</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER_TASK__TASK = eINSTANCE.getUserTask_Task();

		/**
		 * The meta object literal for the '<em><b>Started</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_TASK__STARTED = eINSTANCE.getUserTask_Started();

		/**
		 * The meta object literal for the '<em><b>Finished</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_TASK__FINISHED = eINSTANCE.getUserTask_Finished();

		/**
		 * The meta object literal for the '<em><b>Hints Displayed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_TASK__HINTS_DISPLAYED = eINSTANCE.getUserTask_HintsDisplayed();

		/**
		 * The meta object literal for the '<em><b>Added</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_TASK__ADDED = eINSTANCE.getUserTask_Added();

		/**
		 * The meta object literal for the '<em><b>Is Completed</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation USER_TASK___IS_COMPLETED = eINSTANCE.getUserTask__IsCompleted();

		/**
		 * The meta object literal for the '<em><b>Activate</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation USER_TASK___ACTIVATE = eINSTANCE.getUserTask__Activate();

		/**
		 * The meta object literal for the '<em><b>Get User</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation USER_TASK___GET_USER = eINSTANCE.getUserTask__GetUser();

		/**
		 * The meta object literal for the '<em><b>Is Started</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation USER_TASK___IS_STARTED = eINSTANCE.getUserTask__IsStarted();

		/**
		 * The meta object literal for the '<em><b>Reveal Next Hint</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation USER_TASK___REVEAL_NEXT_HINT = eINSTANCE.getUserTask__RevealNextHint();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MQuest <em>Quest</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MQuest
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getQuest()
		 * @generated
		 */
		EClass QUEST = eINSTANCE.getQuest();

		/**
		 * The meta object literal for the '<em><b>Tasks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUEST__TASKS = eINSTANCE.getQuest_Tasks();

		/**
		 * The meta object literal for the '<em><b>Skills</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUEST__SKILLS = eINSTANCE.getQuest_Skills();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUEST__TITLE = eINSTANCE.getQuest_Title();

		/**
		 * The meta object literal for the '<em><b>User Titles</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUEST__USER_TITLES = eINSTANCE.getQuest_UserTitles();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MDependency <em>Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MDependency
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getDependency()
		 * @generated
		 */
		EClass DEPENDENCY = eINSTANCE.getDependency();

		/**
		 * The meta object literal for the '<em><b>Fulfilled</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPENDENCY__FULFILLED = eINSTANCE.getDependency_Fulfilled();

		/**
		 * The meta object literal for the '<em><b>Activate</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DEPENDENCY___ACTIVATE = eINSTANCE.getDependency__Activate();

		/**
		 * The meta object literal for the '<em><b>Deactivate</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DEPENDENCY___DEACTIVATE = eINSTANCE.getDependency__Deactivate();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MBadgeReward <em>Badge Reward</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MBadgeReward
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getBadgeReward()
		 * @generated
		 */
		EClass BADGE_REWARD = eINSTANCE.getBadgeReward();

		/**
		 * The meta object literal for the '<em><b>Badge</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BADGE_REWARD__BADGE = eINSTANCE.getBadgeReward_Badge();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MExperienceReward <em>Experience Reward</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MExperienceReward
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getExperienceReward()
		 * @generated
		 */
		EClass EXPERIENCE_REWARD = eINSTANCE.getExperienceReward();

		/**
		 * The meta object literal for the '<em><b>Experience</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXPERIENCE_REWARD__EXPERIENCE = eINSTANCE.getExperienceReward_Experience();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MSkillReward <em>Skill Reward</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MSkillReward
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getSkillReward()
		 * @generated
		 */
		EClass SKILL_REWARD = eINSTANCE.getSkillReward();

		/**
		 * The meta object literal for the '<em><b>Skill</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_REWARD__SKILL = eINSTANCE.getSkillReward_Skill();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MContainedDependency <em>Contained Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MContainedDependency
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getContainedDependency()
		 * @generated
		 */
		EClass CONTAINED_DEPENDENCY = eINSTANCE.getContainedDependency();

		/**
		 * The meta object literal for the '<em><b>Dependencies</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTAINED_DEPENDENCY__DEPENDENCIES = eINSTANCE.getContainedDependency_Dependencies();

		/**
		 * The meta object literal for the '<em><b>Evaluate Dependencies</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONTAINED_DEPENDENCY___EVALUATE_DEPENDENCIES = eINSTANCE.getContainedDependency__EvaluateDependencies();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MAndDependency <em>And Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MAndDependency
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getAndDependency()
		 * @generated
		 */
		EClass AND_DEPENDENCY = eINSTANCE.getAndDependency();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MOrDependency <em>Or Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MOrDependency
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getOrDependency()
		 * @generated
		 */
		EClass OR_DEPENDENCY = eINSTANCE.getOrDependency();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MNotDependency <em>Not Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MNotDependency
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getNotDependency()
		 * @generated
		 */
		EClass NOT_DEPENDENCY = eINSTANCE.getNotDependency();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MDelayedDependency <em>Delayed Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MDelayedDependency
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getDelayedDependency()
		 * @generated
		 */
		EClass DELAYED_DEPENDENCY = eINSTANCE.getDelayedDependency();

		/**
		 * The meta object literal for the '<em><b>Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DELAYED_DEPENDENCY__DELAY = eINSTANCE.getDelayedDependency_Delay();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MCustomDependency <em>Custom Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MCustomDependency
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getCustomDependency()
		 * @generated
		 */
		EClass CUSTOM_DEPENDENCY = eINSTANCE.getCustomDependency();

		/**
		 * The meta object literal for the '<em><b>Extension Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUSTOM_DEPENDENCY__EXTENSION_ID = eINSTANCE.getCustomDependency_ExtensionId();

		/**
		 * The meta object literal for the '<em><b>Extension Attribute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUSTOM_DEPENDENCY__EXTENSION_ATTRIBUTE = eINSTANCE.getCustomDependency_ExtensionAttribute();

		/**
		 * The meta object literal for the '<em><b>Dependency</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUSTOM_DEPENDENCY__DEPENDENCY = eINSTANCE.getCustomDependency_Dependency();

		/**
		 * The meta object literal for the '<em><b>Definition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUSTOM_DEPENDENCY__DEFINITION = eINSTANCE.getCustomDependency_Definition();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.IDependencyWithAttributes <em>Dependency With Attributes</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.IDependencyWithAttributes
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getDependencyWithAttributes()
		 * @generated
		 */
		EClass DEPENDENCY_WITH_ATTRIBUTES = eINSTANCE.getDependencyWithAttributes();

		/**
		 * The meta object literal for the '<em><b>Set Attributes</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DEPENDENCY_WITH_ATTRIBUTES___SET_ATTRIBUTES__STRING = eINSTANCE.getDependencyWithAttributes__SetAttributes__String();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MSequenceDependency <em>Sequence Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MSequenceDependency
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getSequenceDependency()
		 * @generated
		 */
		EClass SEQUENCE_DEPENDENCY = eINSTANCE.getSequenceDependency();

		/**
		 * The meta object literal for the '<em><b>Completed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEQUENCE_DEPENDENCY__COMPLETED = eINSTANCE.getSequenceDependency_Completed();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MCompleteIncludedTasksDependency <em>Complete Included Tasks Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MCompleteIncludedTasksDependency
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getCompleteIncludedTasksDependency()
		 * @generated
		 */
		EClass COMPLETE_INCLUDED_TASKS_DEPENDENCY = eINSTANCE.getCompleteIncludedTasksDependency();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MTaskDependency <em>Task Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MTaskDependency
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getTaskDependency()
		 * @generated
		 */
		EClass TASK_DEPENDENCY = eINSTANCE.getTaskDependency();

		/**
		 * The meta object literal for the '<em><b>Task</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_DEPENDENCY__TASK = eINSTANCE.getTaskDependency_Task();

		/**
		 * The meta object literal for the '<em><b>Completed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK_DEPENDENCY__COMPLETED = eINSTANCE.getTaskDependency_Completed();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MSkillDependency <em>Skill Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MSkillDependency
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getSkillDependency()
		 * @generated
		 */
		EClass SKILL_DEPENDENCY = eINSTANCE.getSkillDependency();

		/**
		 * The meta object literal for the '<em><b>Min Experience</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKILL_DEPENDENCY__MIN_EXPERIENCE = eINSTANCE.getSkillDependency_MinExperience();

		/**
		 * The meta object literal for the '<em><b>Min Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKILL_DEPENDENCY__MIN_LEVEL = eINSTANCE.getSkillDependency_MinLevel();

		/**
		 * The meta object literal for the '<em><b>Skill</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_DEPENDENCY__SKILL = eINSTANCE.getSkillDependency_Skill();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MHint <em>Hint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MHint
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getHint()
		 * @generated
		 */
		EClass HINT = eINSTANCE.getHint();

		/**
		 * The meta object literal for the '<em><b>Penalty</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HINT__PENALTY = eINSTANCE.getHint_Penalty();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MUserDependency <em>User Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MUserDependency
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getUserDependency()
		 * @generated
		 */
		EClass USER_DEPENDENCY = eINSTANCE.getUserDependency();

		/**
		 * The meta object literal for the '<em><b>User</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER_DEPENDENCY__USER = eINSTANCE.getUserDependency_User();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.ILevelProgression <em>Level Progression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.ILevelProgression
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getLevelProgression()
		 * @generated
		 */
		EClass LEVEL_PROGRESSION = eINSTANCE.getLevelProgression();

		/**
		 * The meta object literal for the '<em><b>Get Minimum Xp For Level</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LEVEL_PROGRESSION___GET_MINIMUM_XP_FOR_LEVEL__INT = eINSTANCE.getLevelProgression__GetMinimumXpForLevel__int();

		/**
		 * The meta object literal for the '<em><b>Get Level</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LEVEL_PROGRESSION___GET_LEVEL__INT = eINSTANCE.getLevelProgression__GetLevel__int();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MFactorProgression <em>Factor Progression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MFactorProgression
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getFactorProgression()
		 * @generated
		 */
		EClass FACTOR_PROGRESSION = eINSTANCE.getFactorProgression();

		/**
		 * The meta object literal for the '<em><b>Base Xp Needed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FACTOR_PROGRESSION__BASE_XP_NEEDED = eINSTANCE.getFactorProgression_BaseXpNeeded();

		/**
		 * The meta object literal for the '<em><b>Xp Factor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FACTOR_PROGRESSION__XP_FACTOR = eINSTANCE.getFactorProgression_XpFactor();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.impl.MUserTitle <em>User Title</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.impl.MUserTitle
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getUserTitle()
		 * @generated
		 */
		EClass USER_TITLE = eINSTANCE.getUserTitle();

		/**
		 * The meta object literal for the '<em><b>Min Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_TITLE__MIN_LEVEL = eINSTANCE.getUserTitle_MinLevel();

		/**
		 * The meta object literal for the '<em><b>Skill</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER_TITLE__SKILL = eINSTANCE.getUserTitle_Skill();

		/**
		 * The meta object literal for the '<em><b>Display String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_TITLE__DISPLAY_STRING = eINSTANCE.getUserTitle_DisplayString();

		/**
		 * The meta object literal for the '{@link org.eclipse.skills.model.LevelName <em>Level Name</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.model.LevelName
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getLevelName()
		 * @generated
		 */
		EEnum LEVEL_NAME = eINSTANCE.getLevelName();

		/**
		 * The meta object literal for the '<em>Date</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.Date
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getDate()
		 * @generated
		 */
		EDataType DATE = eINSTANCE.getDate();

		/**
		 * The meta object literal for the '<em>Custom Dependency Definition</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.dependencies.CustomDependencyDefinition
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getCustomDependencyDefinition()
		 * @generated
		 */
		EDataType CUSTOM_DEPENDENCY_DEFINITION = eINSTANCE.getCustomDependencyDefinition();

		/**
		 * The meta object literal for the '<em>Image Descriptor</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.jface.resource.ImageDescriptor
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getImageDescriptor()
		 * @generated
		 */
		EDataType IMAGE_DESCRIPTOR = eINSTANCE.getImageDescriptor();

		/**
		 * The meta object literal for the '<em>ISkill Service</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.skills.service.ISkillService
		 * @see org.eclipse.skills.model.impl.MSkillsPackage#getISkillService()
		 * @generated
		 */
		EDataType ISKILL_SERVICE = eINSTANCE.getISkillService();

	}

} //ISkillsPackage
