/**
 */
package org.eclipse.skills.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sequence Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.ISequenceDependency#getCompleted <em>Completed</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getSequenceDependency()
 * @model
 * @generated
 */
public interface ISequenceDependency extends IAndDependency {

	/**
	 * Returns the value of the '<em><b>Completed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Completed</em>' attribute.
	 * @see #setCompleted(int)
	 * @see org.eclipse.skills.model.ISkillsPackage#getSequenceDependency_Completed()
	 * @model
	 * @generated
	 */
	int getCompleted();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ISequenceDependency#getCompleted <em>Completed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Completed</em>' attribute.
	 * @see #getCompleted()
	 * @generated
	 */
	void setCompleted(int value);
} // ISequenceDependency
