/**
 */
package org.eclipse.skills.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complete Included Tasks Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getCompleteIncludedTasksDependency()
 * @model
 * @generated
 */
public interface ICompleteIncludedTasksDependency extends IUserDependency {
} // ICompleteIncludedTasksDependency
