/**
 */
package org.eclipse.skills.model.impl;

import java.util.Optional;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.skills.Activator;
import org.eclipse.skills.Logger;
import org.eclipse.skills.dependencies.CustomDependencyDefinition;
import org.eclipse.skills.model.ICustomDependency;
import org.eclipse.skills.model.IDependency;
import org.eclipse.skills.model.ISkillsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Custom Dependency</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.impl.MCustomDependency#getExtensionId <em>Extension Id</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MCustomDependency#getExtensionAttribute <em>Extension Attribute</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MCustomDependency#getDependency <em>Dependency</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MCustomDependency#getDefinition <em>Definition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MCustomDependency extends MDependency implements ICustomDependency {
	/**
	 * The default value of the '{@link #getExtensionId() <em>Extension Id</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getExtensionId()
	 * @generated
	 * @ordered
	 */
	protected static final String EXTENSION_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExtensionId() <em>Extension Id</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getExtensionId()
	 * @generated
	 * @ordered
	 */
	protected String extensionId = EXTENSION_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getExtensionAttribute() <em>Extension Attribute</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getExtensionAttribute()
	 * @generated
	 * @ordered
	 */
	protected static final String EXTENSION_ATTRIBUTE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExtensionAttribute() <em>Extension Attribute</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getExtensionAttribute()
	 * @generated
	 * @ordered
	 */
	protected String extensionAttribute = EXTENSION_ATTRIBUTE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDependency() <em>Dependency</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDependency()
	 * @generated
	 * @ordered
	 */
	protected IDependency dependency;

	/**
	 * The default value of the '{@link #getDefinition() <em>Definition</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDefinition()
	 * @generated
	 * @ordered
	 */
	protected static final CustomDependencyDefinition DEFINITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDefinition() <em>Definition</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDefinition()
	 * @generated
	 * @ordered
	 */
	protected CustomDependencyDefinition definition = DEFINITION_EDEFAULT;

	/**
	 * Adapter to listen for changes in sub-dependencies.
	 *
	 * @generated NOT
	 */
	private final AdapterImpl fChangeAdapter = new AdapterImpl() {
		@Override
		public void notifyChanged(Notification msg) {
			if (ISkillsPackage.eINSTANCE.getDependency_Fulfilled().equals(msg.getFeature()))
				setFulfilled(getDependency().isFulfilled());
		};
	};

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MCustomDependency() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ISkillsPackage.Literals.CUSTOM_DEPENDENCY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getExtensionId() {
		return extensionId;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtensionIdGen(String newExtensionId) {
		String oldExtensionId = extensionId;
		extensionId = newExtensionId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.CUSTOM_DEPENDENCY__EXTENSION_ID, oldExtensionId, extensionId));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public void setExtensionId(String newExtensionId) {
		setExtensionIdGen(newExtensionId);

		setDefinition(null);
		basicSetDependency(null, null);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getExtensionAttribute() {
		return extensionAttribute;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtensionAttributeGen(String newExtensionAttribute) {
		String oldExtensionAttribute = extensionAttribute;
		extensionAttribute = newExtensionAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.CUSTOM_DEPENDENCY__EXTENSION_ATTRIBUTE, oldExtensionAttribute, extensionAttribute));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public void setExtensionAttribute(String newExtensionAttribute) {
		setExtensionAttributeGen(newExtensionAttribute);

		setDefinition(null);
		basicSetDependency(null, null);
	}

	/**
	 * @generated
	 */
	public IDependency getDependencyGen() {
		return dependency;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public IDependency getDependency() {
		if (dependency == null) {
			if (getDefinition() != null) {
				try {
					final IDependency instance = getDefinition().createInstance(getExtensionAttribute());
					basicSetDependency(instance, null);
				} catch (final RuntimeException e) {
					Logger.error(Activator.PLUGIN_ID, "Could not create Skills dependency for \"" + getExtensionId() + "\"");
				}
			} else
				Logger.error(Activator.PLUGIN_ID, "Invalid Skills dependency ID: " + getExtensionId());
		}

		return getDependencyGen();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDependency(IDependency newDependency, NotificationChain msgs) {
		IDependency oldDependency = dependency;
		dependency = newDependency;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ISkillsPackage.CUSTOM_DEPENDENCY__DEPENDENCY, oldDependency, newDependency);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CustomDependencyDefinition getDefinitionGen() {
		return definition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public CustomDependencyDefinition getDefinition() {
		if (definition == null) {
			final Optional<CustomDependencyDefinition> optional = CustomDependencyDefinition.getDefinedDefinitions().stream()
					.filter(d -> d.getId().equals(getExtensionId())).findFirst();

			if (optional.isPresent())
				setDefinition(optional.get());
		}

		return getDefinitionGen();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDefinition(CustomDependencyDefinition newDefinition) {
		CustomDependencyDefinition oldDefinition = definition;
		definition = newDefinition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.CUSTOM_DEPENDENCY__DEFINITION, oldDefinition, definition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ISkillsPackage.CUSTOM_DEPENDENCY__DEPENDENCY:
				return basicSetDependency(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ISkillsPackage.CUSTOM_DEPENDENCY__EXTENSION_ID:
				return getExtensionId();
			case ISkillsPackage.CUSTOM_DEPENDENCY__EXTENSION_ATTRIBUTE:
				return getExtensionAttribute();
			case ISkillsPackage.CUSTOM_DEPENDENCY__DEPENDENCY:
				return getDependency();
			case ISkillsPackage.CUSTOM_DEPENDENCY__DEFINITION:
				return getDefinition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ISkillsPackage.CUSTOM_DEPENDENCY__EXTENSION_ID:
				setExtensionId((String)newValue);
				return;
			case ISkillsPackage.CUSTOM_DEPENDENCY__EXTENSION_ATTRIBUTE:
				setExtensionAttribute((String)newValue);
				return;
			case ISkillsPackage.CUSTOM_DEPENDENCY__DEFINITION:
				setDefinition((CustomDependencyDefinition)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ISkillsPackage.CUSTOM_DEPENDENCY__EXTENSION_ID:
				setExtensionId(EXTENSION_ID_EDEFAULT);
				return;
			case ISkillsPackage.CUSTOM_DEPENDENCY__EXTENSION_ATTRIBUTE:
				setExtensionAttribute(EXTENSION_ATTRIBUTE_EDEFAULT);
				return;
			case ISkillsPackage.CUSTOM_DEPENDENCY__DEFINITION:
				setDefinition(DEFINITION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ISkillsPackage.CUSTOM_DEPENDENCY__EXTENSION_ID:
				return EXTENSION_ID_EDEFAULT == null ? extensionId != null : !EXTENSION_ID_EDEFAULT.equals(extensionId);
			case ISkillsPackage.CUSTOM_DEPENDENCY__EXTENSION_ATTRIBUTE:
				return EXTENSION_ATTRIBUTE_EDEFAULT == null ? extensionAttribute != null : !EXTENSION_ATTRIBUTE_EDEFAULT.equals(extensionAttribute);
			case ISkillsPackage.CUSTOM_DEPENDENCY__DEPENDENCY:
				return dependency != null;
			case ISkillsPackage.CUSTOM_DEPENDENCY__DEFINITION:
				return DEFINITION_EDEFAULT == null ? definition != null : !DEFINITION_EDEFAULT.equals(definition);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (extensionId: ");
		result.append(extensionId);
		result.append(", extensionAttribute: ");
		result.append(extensionAttribute);
		result.append(", definition: ");
		result.append(definition);
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public void activate() {
		if (getDependency() != null) {
			getDependency().eAdapters().add(fChangeAdapter);
			getDependency().activate();

			setFulfilled(getDependency().isFulfilled());

		} else
			setFulfilled(false);
	}

	@Override
	public void deactivate() {
		getDependency().eAdapters().remove(fChangeAdapter);
		getDependency().deactivate();
	}
} // MCustomDependency
