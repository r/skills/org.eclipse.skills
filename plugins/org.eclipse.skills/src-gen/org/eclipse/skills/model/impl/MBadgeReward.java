/**
 */
package org.eclipse.skills.model.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.skills.Activator;
import org.eclipse.skills.Logger;
import org.eclipse.skills.model.IBadge;
import org.eclipse.skills.model.IBadgeReward;
import org.eclipse.skills.model.ISkillsPackage;
import org.eclipse.skills.model.IUser;
import org.eclipse.skills.service.SkillService;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Badge Reward</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.impl.MBadgeReward#getBadge <em>Badge</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MBadgeReward extends MinimalEObjectImpl.Container implements IBadgeReward {
	/**
	 * The cached value of the '{@link #getBadge() <em>Badge</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getBadge()
	 * @generated
	 * @ordered
	 */
	protected IBadge badge;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MBadgeReward() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ISkillsPackage.Literals.BADGE_REWARD;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IBadge getBadge() {
		return badge;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBadge(IBadge newBadge, NotificationChain msgs) {
		IBadge oldBadge = badge;
		badge = newBadge;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ISkillsPackage.BADGE_REWARD__BADGE, oldBadge, newBadge);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBadge(IBadge newBadge) {
		if (newBadge != badge) {
			NotificationChain msgs = null;
			if (badge != null)
				msgs = ((InternalEObject)badge).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ISkillsPackage.BADGE_REWARD__BADGE, null, msgs);
			if (newBadge != null)
				msgs = ((InternalEObject)newBadge).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ISkillsPackage.BADGE_REWARD__BADGE, null, msgs);
			msgs = basicSetBadge(newBadge, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.BADGE_REWARD__BADGE, newBadge, newBadge));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public void payOut(IUser user) {
		final IBadge copy = EcoreUtil.copy(getBadge());

		try {
			// try to clone image to a local folder
			SkillService.getInstance().storeResource(getBadge().getTitle(), getBadge().getImageDescriptor().getImageData(100).data);

		} catch (final IOException e) {
			Logger.error(Activator.PLUGIN_ID, "Could not copy badge to local folder", e);
		}

		user.getBadges().add(copy);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ISkillsPackage.BADGE_REWARD__BADGE:
				return basicSetBadge(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ISkillsPackage.BADGE_REWARD__BADGE:
				return getBadge();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ISkillsPackage.BADGE_REWARD__BADGE:
				setBadge((IBadge)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ISkillsPackage.BADGE_REWARD__BADGE:
				setBadge((IBadge)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ISkillsPackage.BADGE_REWARD__BADGE:
				return badge != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ISkillsPackage.BADGE_REWARD___PAY_OUT__IUSER:
				payOut((IUser)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} // MBadgeReward
