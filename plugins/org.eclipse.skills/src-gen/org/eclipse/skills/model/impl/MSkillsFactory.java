/**
 */
package org.eclipse.skills.model.impl;

import java.util.Date;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.skills.dependencies.CustomDependencyDefinition;
import org.eclipse.skills.model.*;
import org.eclipse.skills.service.ISkillService;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MSkillsFactory extends EFactoryImpl implements ISkillsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ISkillsFactory init() {
		try {
			ISkillsFactory theSkillsFactory = (ISkillsFactory)EPackage.Registry.INSTANCE.getEFactory(ISkillsPackage.eNS_URI);
			if (theSkillsFactory != null) {
				return theSkillsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MSkillsFactory();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSkillsFactory() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ISkillsPackage.TASK: return createTask();
			case ISkillsPackage.DESCRIPTION: return createDescription();
			case ISkillsPackage.BADGE: return createBadge();
			case ISkillsPackage.USER: return createUser();
			case ISkillsPackage.SKILL: return createSkill();
			case ISkillsPackage.USER_TASK: return createUserTask();
			case ISkillsPackage.QUEST: return createQuest();
			case ISkillsPackage.BADGE_REWARD: return createBadgeReward();
			case ISkillsPackage.EXPERIENCE_REWARD: return createExperienceReward();
			case ISkillsPackage.SKILL_REWARD: return createSkillReward();
			case ISkillsPackage.AND_DEPENDENCY: return createAndDependency();
			case ISkillsPackage.OR_DEPENDENCY: return createOrDependency();
			case ISkillsPackage.NOT_DEPENDENCY: return createNotDependency();
			case ISkillsPackage.DELAYED_DEPENDENCY: return createDelayedDependency();
			case ISkillsPackage.CUSTOM_DEPENDENCY: return createCustomDependency();
			case ISkillsPackage.SEQUENCE_DEPENDENCY: return createSequenceDependency();
			case ISkillsPackage.COMPLETE_INCLUDED_TASKS_DEPENDENCY: return createCompleteIncludedTasksDependency();
			case ISkillsPackage.TASK_DEPENDENCY: return createTaskDependency();
			case ISkillsPackage.SKILL_DEPENDENCY: return createSkillDependency();
			case ISkillsPackage.HINT: return createHint();
			case ISkillsPackage.FACTOR_PROGRESSION: return createFactorProgression();
			case ISkillsPackage.USER_TITLE: return createUserTitle();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ISkillsPackage.LEVEL_NAME:
				return createLevelNameFromString(eDataType, initialValue);
			case ISkillsPackage.DATE:
				return createDateFromString(eDataType, initialValue);
			case ISkillsPackage.CUSTOM_DEPENDENCY_DEFINITION:
				return createCustomDependencyDefinitionFromString(eDataType, initialValue);
			case ISkillsPackage.ISKILL_SERVICE:
				return createISkillServiceFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ISkillsPackage.LEVEL_NAME:
				return convertLevelNameToString(eDataType, instanceValue);
			case ISkillsPackage.DATE:
				return convertDateToString(eDataType, instanceValue);
			case ISkillsPackage.CUSTOM_DEPENDENCY_DEFINITION:
				return convertCustomDependencyDefinitionToString(eDataType, instanceValue);
			case ISkillsPackage.ISKILL_SERVICE:
				return convertISkillServiceToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ITask createTask() {
		MTask task = new MTask();
		return task;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IDescription createDescription() {
		MDescription description = new MDescription();
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IBadge createBadge() {
		MBadge badge = new MBadge();
		return badge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IUser createUser() {
		MUser user = new MUser();
		return user;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISkill createSkill() {
		MSkill skill = new MSkill();
		return skill;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IUserTask createUserTask() {
		MUserTask userTask = new MUserTask();
		return userTask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IQuest createQuest() {
		MQuest quest = new MQuest();
		return quest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IBadgeReward createBadgeReward() {
		MBadgeReward badgeReward = new MBadgeReward();
		return badgeReward;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IExperienceReward createExperienceReward() {
		MExperienceReward experienceReward = new MExperienceReward();
		return experienceReward;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISkillReward createSkillReward() {
		MSkillReward skillReward = new MSkillReward();
		return skillReward;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IAndDependency createAndDependency() {
		MAndDependency andDependency = new MAndDependency();
		return andDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IOrDependency createOrDependency() {
		MOrDependency orDependency = new MOrDependency();
		return orDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public INotDependency createNotDependency() {
		MNotDependency notDependency = new MNotDependency();
		return notDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IDelayedDependency createDelayedDependency() {
		MDelayedDependency delayedDependency = new MDelayedDependency();
		return delayedDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ICustomDependency createCustomDependency() {
		MCustomDependency customDependency = new MCustomDependency();
		return customDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISequenceDependency createSequenceDependency() {
		MSequenceDependency sequenceDependency = new MSequenceDependency();
		return sequenceDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ICompleteIncludedTasksDependency createCompleteIncludedTasksDependency() {
		MCompleteIncludedTasksDependency completeIncludedTasksDependency = new MCompleteIncludedTasksDependency();
		return completeIncludedTasksDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ITaskDependency createTaskDependency() {
		MTaskDependency taskDependency = new MTaskDependency();
		return taskDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISkillDependency createSkillDependency() {
		MSkillDependency skillDependency = new MSkillDependency();
		return skillDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IHint createHint() {
		MHint hint = new MHint();
		return hint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IFactorProgression createFactorProgression() {
		MFactorProgression factorProgression = new MFactorProgression();
		return factorProgression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IUserTitle createUserTitle() {
		MUserTitle userTitle = new MUserTitle();
		return userTitle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LevelName createLevelNameFromString(EDataType eDataType, String initialValue) {
		LevelName result = LevelName.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLevelNameToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date createDateFromString(EDataType eDataType, String initialValue) {
		return (Date)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDateToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CustomDependencyDefinition createCustomDependencyDefinitionFromString(EDataType eDataType, String initialValue) {
		return (CustomDependencyDefinition)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCustomDependencyDefinitionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISkillService createISkillServiceFromString(EDataType eDataType, String initialValue) {
		return (ISkillService)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertISkillServiceToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISkillsPackage getSkillsPackage() {
		return (ISkillsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ISkillsPackage getPackage() {
		return ISkillsPackage.eINSTANCE;
	}

} //MSkillsFactory
