/**
 */
package org.eclipse.skills.model.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.skills.model.ICompleteIncludedTasksDependency;
import org.eclipse.skills.model.ISkillsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Complete Included Tasks Dependency</b></em>'. <!-- end-user-doc -->
 *
 * @generated
 */
public class MCompleteIncludedTasksDependency extends MUserDependency implements ICompleteIncludedTasksDependency {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MCompleteIncludedTasksDependency() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ISkillsPackage.Literals.COMPLETE_INCLUDED_TASKS_DEPENDENCY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public void activate() {
		// FIXME implement
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public void deactivate() {
		// FIXME implement
	}
} // MCompleteIncludedTasksDependency
