/**
 */
package org.eclipse.skills.model.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.skills.model.IFactorProgression;
import org.eclipse.skills.model.ISkillsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Factor Progression</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.impl.MFactorProgression#getBaseXpNeeded <em>Base Xp Needed</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MFactorProgression#getXpFactor <em>Xp Factor</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MFactorProgression extends MinimalEObjectImpl.Container implements IFactorProgression {
	/**
	 * The default value of the '{@link #getBaseXpNeeded() <em>Base Xp Needed</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getBaseXpNeeded()
	 * @generated
	 * @ordered
	 */
	protected static final int BASE_XP_NEEDED_EDEFAULT = 100;

	/**
	 * The cached value of the '{@link #getBaseXpNeeded() <em>Base Xp Needed</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getBaseXpNeeded()
	 * @generated
	 * @ordered
	 */
	protected int baseXpNeeded = BASE_XP_NEEDED_EDEFAULT;

	/**
	 * The default value of the '{@link #getXpFactor() <em>Xp Factor</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getXpFactor()
	 * @generated
	 * @ordered
	 */
	protected static final double XP_FACTOR_EDEFAULT = 2.4;

	/**
	 * The cached value of the '{@link #getXpFactor() <em>Xp Factor</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getXpFactor()
	 * @generated
	 * @ordered
	 */
	protected double xpFactor = XP_FACTOR_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MFactorProgression() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ISkillsPackage.Literals.FACTOR_PROGRESSION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getBaseXpNeeded() {
		return baseXpNeeded;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBaseXpNeeded(int newBaseXpNeeded) {
		int oldBaseXpNeeded = baseXpNeeded;
		baseXpNeeded = newBaseXpNeeded;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.FACTOR_PROGRESSION__BASE_XP_NEEDED, oldBaseXpNeeded, baseXpNeeded));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public double getXpFactor() {
		return xpFactor;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setXpFactor(double newXpFactor) {
		double oldXpFactor = xpFactor;
		xpFactor = newXpFactor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.FACTOR_PROGRESSION__XP_FACTOR, oldXpFactor, xpFactor));
	}

	/**
	 * @generated NOT
	 */
	@Override
	public int getMinimumXpForLevel(int level) {
		if (level > 2)
			return (int) (getMinimumXpForLevel(level - 1) * getXpFactor());

		if (level == 2)
			return getBaseXpNeeded();

		return 0;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public int getLevel(int xp) {
		int candidate = 1;
		while (getMinimumXpForLevel(candidate) <= xp)
			candidate++;

		return candidate - 1;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ISkillsPackage.FACTOR_PROGRESSION__BASE_XP_NEEDED:
				return getBaseXpNeeded();
			case ISkillsPackage.FACTOR_PROGRESSION__XP_FACTOR:
				return getXpFactor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ISkillsPackage.FACTOR_PROGRESSION__BASE_XP_NEEDED:
				setBaseXpNeeded((Integer)newValue);
				return;
			case ISkillsPackage.FACTOR_PROGRESSION__XP_FACTOR:
				setXpFactor((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ISkillsPackage.FACTOR_PROGRESSION__BASE_XP_NEEDED:
				setBaseXpNeeded(BASE_XP_NEEDED_EDEFAULT);
				return;
			case ISkillsPackage.FACTOR_PROGRESSION__XP_FACTOR:
				setXpFactor(XP_FACTOR_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ISkillsPackage.FACTOR_PROGRESSION__BASE_XP_NEEDED:
				return baseXpNeeded != BASE_XP_NEEDED_EDEFAULT;
			case ISkillsPackage.FACTOR_PROGRESSION__XP_FACTOR:
				return xpFactor != XP_FACTOR_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ISkillsPackage.FACTOR_PROGRESSION___GET_MINIMUM_XP_FOR_LEVEL__INT:
				return getMinimumXpForLevel((Integer)arguments.get(0));
			case ISkillsPackage.FACTOR_PROGRESSION___GET_LEVEL__INT:
				return getLevel((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (baseXpNeeded: ");
		result.append(baseXpNeeded);
		result.append(", xpFactor: ");
		result.append(xpFactor);
		result.append(')');
		return result.toString();
	}

} // MFactorProgression
