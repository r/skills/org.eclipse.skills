/**
 */
package org.eclipse.skills.model.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.skills.model.ISkill;
import org.eclipse.skills.model.ISkillDependency;
import org.eclipse.skills.model.ISkillsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Skill Dependency</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.impl.MSkillDependency#getMinExperience <em>Min Experience</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MSkillDependency#getMinLevel <em>Min Level</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MSkillDependency#getSkill <em>Skill</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MSkillDependency extends MUserDependency implements ISkillDependency {
	/**
	 * The default value of the '{@link #getMinExperience() <em>Min Experience</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMinExperience()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_EXPERIENCE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMinExperience() <em>Min Experience</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMinExperience()
	 * @generated
	 * @ordered
	 */
	protected int minExperience = MIN_EXPERIENCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinLevel() <em>Min Level</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMinLevel()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_LEVEL_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMinLevel() <em>Min Level</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMinLevel()
	 * @generated
	 * @ordered
	 */
	protected int minLevel = MIN_LEVEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSkill() <em>Skill</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSkill()
	 * @generated
	 * @ordered
	 */
	protected ISkill skill;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MSkillDependency() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ISkillsPackage.Literals.SKILL_DEPENDENCY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getMinExperience() {
		return minExperience;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMinExperience(int newMinExperience) {
		int oldMinExperience = minExperience;
		minExperience = newMinExperience;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.SKILL_DEPENDENCY__MIN_EXPERIENCE, oldMinExperience, minExperience));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getMinLevel() {
		return minLevel;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMinLevel(int newMinLevel) {
		int oldMinLevel = minLevel;
		minLevel = newMinLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.SKILL_DEPENDENCY__MIN_LEVEL, oldMinLevel, minLevel));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISkill getSkill() {
		if (skill != null && skill.eIsProxy()) {
			InternalEObject oldSkill = (InternalEObject)skill;
			skill = (ISkill)eResolveProxy(oldSkill);
			if (skill != oldSkill) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ISkillsPackage.SKILL_DEPENDENCY__SKILL, oldSkill, skill));
			}
		}
		return skill;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ISkill basicGetSkill() {
		return skill;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSkill(ISkill newSkill) {
		ISkill oldSkill = skill;
		skill = newSkill;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.SKILL_DEPENDENCY__SKILL, oldSkill, skill));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ISkillsPackage.SKILL_DEPENDENCY__MIN_EXPERIENCE:
				return getMinExperience();
			case ISkillsPackage.SKILL_DEPENDENCY__MIN_LEVEL:
				return getMinLevel();
			case ISkillsPackage.SKILL_DEPENDENCY__SKILL:
				if (resolve) return getSkill();
				return basicGetSkill();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ISkillsPackage.SKILL_DEPENDENCY__MIN_EXPERIENCE:
				setMinExperience((Integer)newValue);
				return;
			case ISkillsPackage.SKILL_DEPENDENCY__MIN_LEVEL:
				setMinLevel((Integer)newValue);
				return;
			case ISkillsPackage.SKILL_DEPENDENCY__SKILL:
				setSkill((ISkill)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ISkillsPackage.SKILL_DEPENDENCY__MIN_EXPERIENCE:
				setMinExperience(MIN_EXPERIENCE_EDEFAULT);
				return;
			case ISkillsPackage.SKILL_DEPENDENCY__MIN_LEVEL:
				setMinLevel(MIN_LEVEL_EDEFAULT);
				return;
			case ISkillsPackage.SKILL_DEPENDENCY__SKILL:
				setSkill((ISkill)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ISkillsPackage.SKILL_DEPENDENCY__MIN_EXPERIENCE:
				return minExperience != MIN_EXPERIENCE_EDEFAULT;
			case ISkillsPackage.SKILL_DEPENDENCY__MIN_LEVEL:
				return minLevel != MIN_LEVEL_EDEFAULT;
			case ISkillsPackage.SKILL_DEPENDENCY__SKILL:
				return skill != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (minExperience: ");
		result.append(minExperience);
		result.append(", minLevel: ");
		result.append(minLevel);
		result.append(')');
		return result.toString();
	}
}
// MSkillDependency
