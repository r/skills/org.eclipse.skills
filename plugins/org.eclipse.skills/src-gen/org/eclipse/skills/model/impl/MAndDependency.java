/**
 */
package org.eclipse.skills.model.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.skills.model.IAndDependency;
import org.eclipse.skills.model.IDependency;
import org.eclipse.skills.model.ISkillsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>And Dependency</b></em>'. <!-- end-user-doc -->
 *
 * @generated
 */
public class MAndDependency extends MContainedDependency implements IAndDependency {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MAndDependency() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ISkillsPackage.Literals.AND_DEPENDENCY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public void evaluateDependencies() {
		for (final IDependency dependency : getDependencies()) {
			if (!dependency.isFulfilled()) {
				setFulfilled(false);
				return;
			}
		}

		setFulfilled(true);
	}
} // MAndDependency
