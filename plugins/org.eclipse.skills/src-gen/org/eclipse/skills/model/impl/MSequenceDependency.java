/**
 */
package org.eclipse.skills.model.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.skills.model.IDependency;
import org.eclipse.skills.model.ISequenceDependency;
import org.eclipse.skills.model.ISkillsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Sequence Dependency</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.impl.MSequenceDependency#getCompleted <em>Completed</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MSequenceDependency extends MAndDependency implements ISequenceDependency {

	/**
	 * The default value of the '{@link #getCompleted() <em>Completed</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCompleted()
	 * @generated
	 * @ordered
	 */
	protected static final int COMPLETED_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getCompleted() <em>Completed</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCompleted()
	 * @generated
	 * @ordered
	 */
	protected int completed = COMPLETED_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MSequenceDependency() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ISkillsPackage.Literals.SEQUENCE_DEPENDENCY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getCompleted() {
		return completed;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCompleted(int newCompleted) {
		int oldCompleted = completed;
		completed = newCompleted;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.SEQUENCE_DEPENDENCY__COMPLETED, oldCompleted, completed));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ISkillsPackage.SEQUENCE_DEPENDENCY__COMPLETED:
				return getCompleted();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ISkillsPackage.SEQUENCE_DEPENDENCY__COMPLETED:
				setCompleted((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ISkillsPackage.SEQUENCE_DEPENDENCY__COMPLETED:
				setCompleted(COMPLETED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ISkillsPackage.SEQUENCE_DEPENDENCY__COMPLETED:
				return completed != COMPLETED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (completed: ");
		result.append(completed);
		result.append(')');
		return result.toString();
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void activate() {
		activateNextDependency();
		evaluateDependencies();
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void evaluateDependencies() {
		for (int index = getCompleted(); index < getDependencies().size(); index++) {
			if (!getDependencies().get(index).isFulfilled()) {
				setFulfilled(false);
				return;

			} else {
				setCompleted(index + 1);
				activateNextDependency();
			}
		}

		setFulfilled(true);
	}

	/**
	 * @generated NOT
	 */
	private void activateNextDependency() {
		if (getCompleted() < getDependencies().size()) {
			final IDependency nextDependency = getDependencies().get(getCompleted());
			nextDependency.eAdapters().add(getChangeAdapter());
			nextDependency.activate();
		}
	}

} // MSequenceDependency
