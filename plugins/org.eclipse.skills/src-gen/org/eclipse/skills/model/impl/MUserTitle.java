/**
 */
package org.eclipse.skills.model.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.skills.model.ISkill;
import org.eclipse.skills.model.ISkillsPackage;
import org.eclipse.skills.model.IUserTitle;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>User Title</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.impl.MUserTitle#getMinLevel <em>Min Level</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MUserTitle#getSkill <em>Skill</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MUserTitle#getDisplayString <em>Display String</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MUserTitle extends MinimalEObjectImpl.Container implements IUserTitle {
	/**
	 * The default value of the '{@link #getMinLevel() <em>Min Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinLevel()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_LEVEL_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMinLevel() <em>Min Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinLevel()
	 * @generated
	 * @ordered
	 */
	protected int minLevel = MIN_LEVEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSkill() <em>Skill</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSkill()
	 * @generated
	 * @ordered
	 */
	protected ISkill skill;

	/**
	 * The default value of the '{@link #getDisplayString() <em>Display String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayString()
	 * @generated
	 * @ordered
	 */
	protected static final String DISPLAY_STRING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDisplayString() <em>Display String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayString()
	 * @generated
	 * @ordered
	 */
	protected String displayString = DISPLAY_STRING_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MUserTitle() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ISkillsPackage.Literals.USER_TITLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getMinLevel() {
		return minLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMinLevel(int newMinLevel) {
		int oldMinLevel = minLevel;
		minLevel = newMinLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.USER_TITLE__MIN_LEVEL, oldMinLevel, minLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISkill getSkill() {
		if (skill != null && skill.eIsProxy()) {
			InternalEObject oldSkill = (InternalEObject)skill;
			skill = (ISkill)eResolveProxy(oldSkill);
			if (skill != oldSkill) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ISkillsPackage.USER_TITLE__SKILL, oldSkill, skill));
			}
		}
		return skill;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISkill basicGetSkill() {
		return skill;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSkill(ISkill newSkill) {
		ISkill oldSkill = skill;
		skill = newSkill;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.USER_TITLE__SKILL, oldSkill, skill));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDisplayString() {
		return displayString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDisplayString(String newDisplayString) {
		String oldDisplayString = displayString;
		displayString = newDisplayString;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.USER_TITLE__DISPLAY_STRING, oldDisplayString, displayString));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ISkillsPackage.USER_TITLE__MIN_LEVEL:
				return getMinLevel();
			case ISkillsPackage.USER_TITLE__SKILL:
				if (resolve) return getSkill();
				return basicGetSkill();
			case ISkillsPackage.USER_TITLE__DISPLAY_STRING:
				return getDisplayString();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ISkillsPackage.USER_TITLE__MIN_LEVEL:
				setMinLevel((Integer)newValue);
				return;
			case ISkillsPackage.USER_TITLE__SKILL:
				setSkill((ISkill)newValue);
				return;
			case ISkillsPackage.USER_TITLE__DISPLAY_STRING:
				setDisplayString((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ISkillsPackage.USER_TITLE__MIN_LEVEL:
				setMinLevel(MIN_LEVEL_EDEFAULT);
				return;
			case ISkillsPackage.USER_TITLE__SKILL:
				setSkill((ISkill)null);
				return;
			case ISkillsPackage.USER_TITLE__DISPLAY_STRING:
				setDisplayString(DISPLAY_STRING_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ISkillsPackage.USER_TITLE__MIN_LEVEL:
				return minLevel != MIN_LEVEL_EDEFAULT;
			case ISkillsPackage.USER_TITLE__SKILL:
				return skill != null;
			case ISkillsPackage.USER_TITLE__DISPLAY_STRING:
				return DISPLAY_STRING_EDEFAULT == null ? displayString != null : !DISPLAY_STRING_EDEFAULT.equals(displayString);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (minLevel: ");
		result.append(minLevel);
		result.append(", displayString: ");
		result.append(displayString);
		result.append(')');
		return result.toString();
	}

} //MUserTitle
