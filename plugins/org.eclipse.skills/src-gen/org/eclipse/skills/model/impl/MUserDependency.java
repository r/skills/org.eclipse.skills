/**
 */
package org.eclipse.skills.model.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.skills.model.ISkillsPackage;
import org.eclipse.skills.model.IUser;
import org.eclipse.skills.model.IUserDependency;
import org.eclipse.skills.service.ISkillService;
import org.eclipse.skills.service.SkillService;
import org.eclipse.ui.PlatformUI;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>User Dependency</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.impl.MUserDependency#getUser <em>User</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class MUserDependency extends MDependency implements IUserDependency {
	/**
	 * The cached value of the '{@link #getUser() <em>User</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getUser()
	 * @generated
	 * @ordered
	 */
	protected IUser user;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MUserDependency() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ISkillsPackage.Literals.USER_DEPENDENCY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public IUser getUserGen() {
		return user;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public IUser getUser() {
		final IUser result = getUserGen();
		if (result != null)
			return result;

		final ISkillService service = getSkillService();
		if (service != null)
			return service.getUser();

		throw new RuntimeException("User context not available");
	}

	/**
	 * @generated NOT
	 */
	private static ISkillService getSkillService() {
		if (PlatformUI.isWorkbenchRunning())
			return PlatformUI.getWorkbench().getService(ISkillService.class);

		return SkillService.getInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUser(IUser newUser) {
		IUser oldUser = user;
		user = newUser;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.USER_DEPENDENCY__USER, oldUser, user));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ISkillsPackage.USER_DEPENDENCY__USER:
				return getUser();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ISkillsPackage.USER_DEPENDENCY__USER:
				setUser((IUser)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ISkillsPackage.USER_DEPENDENCY__USER:
				setUser((IUser)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ISkillsPackage.USER_DEPENDENCY__USER:
				return user != null;
		}
		return super.eIsSet(featureID);
	}

} // MUserDependency
