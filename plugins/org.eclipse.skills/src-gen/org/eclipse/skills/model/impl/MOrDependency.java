/**
 */
package org.eclipse.skills.model.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.skills.model.IDependency;
import org.eclipse.skills.model.IOrDependency;
import org.eclipse.skills.model.ISkillsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Or Dependency</b></em>'. <!-- end-user-doc -->
 *
 * @generated
 */
public class MOrDependency extends MContainedDependency implements IOrDependency {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MOrDependency() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ISkillsPackage.Literals.OR_DEPENDENCY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public void evaluateDependencies() {
		for (final IDependency dependency : getDependencies()) {
			if (dependency.isFulfilled()) {
				setFulfilled(true);
				return;
			}
		}

		setFulfilled(false);
	}
} // MOrDependency
