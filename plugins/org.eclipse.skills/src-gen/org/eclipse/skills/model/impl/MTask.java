/**
 */
package org.eclipse.skills.model.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.skills.BrokerTools;
import org.eclipse.skills.model.IDependency;
import org.eclipse.skills.model.IDescription;
import org.eclipse.skills.model.IHint;
import org.eclipse.skills.model.IQuest;
import org.eclipse.skills.model.IReward;
import org.eclipse.skills.model.ISkillsPackage;
import org.eclipse.skills.model.ITask;
import org.eclipse.skills.service.ISkillService;
import org.eclipse.skills.service.SkillService;
import org.eclipse.ui.PlatformUI;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Task</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.impl.MTask#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MTask#getGoal <em>Goal</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MTask#getRewards <em>Rewards</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MTask#getTitle <em>Title</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MTask#getRequirement <em>Requirement</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MTask#getTasks <em>Tasks</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MTask#getHints <em>Hints</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MTask#isAutoActivation <em>Auto Activation</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MTask#getSkillService <em>Skill Service</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MTask#getId <em>Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MTask extends MinimalEObjectImpl.Container implements ITask {

	/**
	 * @generated NOT
	 */
	private static String replaceSpecialCharacters(String input) {
		return input.replaceAll("[^a-zA-Z0-9_-]", "_");
	}

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected IDescription description;

	/**
	 * The cached value of the '{@link #getGoal() <em>Goal</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getGoal()
	 * @generated
	 * @ordered
	 */
	protected IDependency goal;

	/**
	 * The cached value of the '{@link #getRewards() <em>Rewards</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRewards()
	 * @generated
	 * @ordered
	 */
	protected EList<IReward> rewards;

	/**
	 * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected static final String TITLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected String title = TITLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRequirement() <em>Requirement</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRequirement()
	 * @generated
	 * @ordered
	 */
	protected IDependency requirement;

	/**
	 * The cached value of the '{@link #getTasks() <em>Tasks</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTasks()
	 * @generated
	 * @ordered
	 */
	protected EList<ITask> tasks;

	/**
	 * The cached value of the '{@link #getHints() <em>Hints</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getHints()
	 * @generated
	 * @ordered
	 */
	protected EList<IHint> hints;

	/**
	 * The default value of the '{@link #isAutoActivation() <em>Auto Activation</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isAutoActivation()
	 * @generated
	 * @ordered
	 */
	protected static final boolean AUTO_ACTIVATION_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isAutoActivation() <em>Auto Activation</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isAutoActivation()
	 * @generated
	 * @ordered
	 */
	protected boolean autoActivation = AUTO_ACTIVATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getSkillService() <em>Skill Service</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSkillService()
	 * @generated
	 * @ordered
	 */
	protected static final ISkillService SKILL_SERVICE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSkillService() <em>Skill Service</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSkillService()
	 * @generated
	 * @ordered
	 */
	protected ISkillService skillService = SKILL_SERVICE_EDEFAULT;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MTask() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ISkillsPackage.Literals.TASK;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IDescription getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDescription(IDescription newDescription, NotificationChain msgs) {
		IDescription oldDescription = description;
		description = newDescription;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ISkillsPackage.TASK__DESCRIPTION, oldDescription, newDescription);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescription(IDescription newDescription) {
		if (newDescription != description) {
			NotificationChain msgs = null;
			if (description != null)
				msgs = ((InternalEObject)description).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ISkillsPackage.TASK__DESCRIPTION, null, msgs);
			if (newDescription != null)
				msgs = ((InternalEObject)newDescription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ISkillsPackage.TASK__DESCRIPTION, null, msgs);
			msgs = basicSetDescription(newDescription, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.TASK__DESCRIPTION, newDescription, newDescription));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IDependency getGoal() {
		return goal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGoal(IDependency newGoal, NotificationChain msgs) {
		IDependency oldGoal = goal;
		goal = newGoal;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ISkillsPackage.TASK__GOAL, oldGoal, newGoal);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGoal(IDependency newGoal) {
		if (newGoal != goal) {
			NotificationChain msgs = null;
			if (goal != null)
				msgs = ((InternalEObject)goal).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ISkillsPackage.TASK__GOAL, null, msgs);
			if (newGoal != null)
				msgs = ((InternalEObject)newGoal).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ISkillsPackage.TASK__GOAL, null, msgs);
			msgs = basicSetGoal(newGoal, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.TASK__GOAL, newGoal, newGoal));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<IReward> getRewards() {
		if (rewards == null) {
			rewards = new EObjectContainmentEList<IReward>(IReward.class, this, ISkillsPackage.TASK__REWARDS);
		}
		return rewards;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTitle() {
		return title;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTitle(String newTitle) {
		String oldTitle = title;
		title = newTitle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.TASK__TITLE, oldTitle, title));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IDependency getRequirement() {
		return requirement;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRequirement(IDependency newRequirement, NotificationChain msgs) {
		IDependency oldRequirement = requirement;
		requirement = newRequirement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ISkillsPackage.TASK__REQUIREMENT, oldRequirement, newRequirement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public void setRequirement(IDependency newRequirement) {
		setRequirementGen(newRequirement);

		if (newRequirement != null) {

			newRequirement.eAdapters().add(new AdapterImpl() {
				@Override
				public void notifyChanged(Notification msg) {
					if (ISkillsPackage.eINSTANCE.getDependency_Fulfilled().equals(msg.getFeature())) {
						if (getRequirement().isFulfilled()) {
							getRequirement().eAdapters().remove(this);

							BrokerTools.post(ISkillService.EVENT_TASK_READY, MTask.this);
						}
					}
				}
			});
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSkillService(ISkillService newSkillService) {
		ISkillService oldSkillService = skillService;
		skillService = newSkillService;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.TASK__SKILL_SERVICE, oldSkillService, skillService));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public String getId() {
		final IQuest quest = getQuest();
		final String questTitle = (quest != null) ? quest.getTitle() : "";

		return replaceSpecialCharacters(questTitle + "/" + getTitle());
	}

	/**
	 * @generated NOT
	 */
	private IQuest getQuest() {
		final EObject container = eContainer();
		if (container instanceof IQuest)
			return (IQuest) container;

		if (container instanceof MTask)
			return ((MTask) container).getQuest();

		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequirementGen(IDependency newRequirement) {
		if (newRequirement != requirement) {
			NotificationChain msgs = null;
			if (requirement != null)
				msgs = ((InternalEObject)requirement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ISkillsPackage.TASK__REQUIREMENT, null, msgs);
			if (newRequirement != null)
				msgs = ((InternalEObject)newRequirement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ISkillsPackage.TASK__REQUIREMENT, null, msgs);
			msgs = basicSetRequirement(newRequirement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.TASK__REQUIREMENT, newRequirement, newRequirement));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ITask> getTasks() {
		if (tasks == null) {
			tasks = new EObjectContainmentEList<ITask>(ITask.class, this, ISkillsPackage.TASK__TASKS);
		}
		return tasks;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<IHint> getHints() {
		if (hints == null) {
			hints = new EObjectContainmentEList<IHint>(IHint.class, this, ISkillsPackage.TASK__HINTS);
		}
		return hints;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isAutoActivation() {
		return autoActivation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAutoActivation(boolean newAutoActivation) {
		boolean oldAutoActivation = autoActivation;
		autoActivation = newAutoActivation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.TASK__AUTO_ACTIVATION, oldAutoActivation, autoActivation));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ISkillService getSkillServiceGen() {
		return skillService;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public ISkillService getSkillService() {
		ISkillService service = getSkillServiceGen();

		if (service != null)
			return service;

		if (PlatformUI.isWorkbenchRunning()) {
			service = PlatformUI.getWorkbench().getService(ISkillService.class);
			if (service != null) {
				setSkillService(service);
				return getSkillServiceGen();
			}
		}

		setSkillService(SkillService.getInstance());
		return getSkillServiceGen();
	}

	/**
	 * @generated NOT
	 */
	@Override
	public boolean isAvailable() {
		final IDependency requirement = getRequirement();
		if (requirement != null)
			return requirement.isFulfilled();

		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public String getShortTitle(int maxLength) {
		if (getTitle().length() > maxLength) {
			if (maxLength > 20) {
				// append '...' to truncated title
				return truncate(getTitle(), maxLength - 3) + "...";

			} else {
				// simply truncate
				return truncate(getTitle(), maxLength);
			}
		}

		return getTitle();
	}

	/**
	 * @generated NOT
	 */
	private String truncate(String input, int maxLength) {
		while (input.length() > maxLength) {
			input = input.substring(0, maxLength + 1);
			final int lastSpace = input.lastIndexOf(' ');
			if (lastSpace > 0)
				input = input.substring(0, lastSpace);
			else
				input = input.substring(0, maxLength);
		}

		return input;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ISkillsPackage.TASK__DESCRIPTION:
				return basicSetDescription(null, msgs);
			case ISkillsPackage.TASK__GOAL:
				return basicSetGoal(null, msgs);
			case ISkillsPackage.TASK__REWARDS:
				return ((InternalEList<?>)getRewards()).basicRemove(otherEnd, msgs);
			case ISkillsPackage.TASK__REQUIREMENT:
				return basicSetRequirement(null, msgs);
			case ISkillsPackage.TASK__TASKS:
				return ((InternalEList<?>)getTasks()).basicRemove(otherEnd, msgs);
			case ISkillsPackage.TASK__HINTS:
				return ((InternalEList<?>)getHints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ISkillsPackage.TASK__DESCRIPTION:
				return getDescription();
			case ISkillsPackage.TASK__GOAL:
				return getGoal();
			case ISkillsPackage.TASK__REWARDS:
				return getRewards();
			case ISkillsPackage.TASK__TITLE:
				return getTitle();
			case ISkillsPackage.TASK__REQUIREMENT:
				return getRequirement();
			case ISkillsPackage.TASK__TASKS:
				return getTasks();
			case ISkillsPackage.TASK__HINTS:
				return getHints();
			case ISkillsPackage.TASK__AUTO_ACTIVATION:
				return isAutoActivation();
			case ISkillsPackage.TASK__SKILL_SERVICE:
				return getSkillService();
			case ISkillsPackage.TASK__ID:
				return getId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ISkillsPackage.TASK__DESCRIPTION:
				setDescription((IDescription)newValue);
				return;
			case ISkillsPackage.TASK__GOAL:
				setGoal((IDependency)newValue);
				return;
			case ISkillsPackage.TASK__REWARDS:
				getRewards().clear();
				getRewards().addAll((Collection<? extends IReward>)newValue);
				return;
			case ISkillsPackage.TASK__TITLE:
				setTitle((String)newValue);
				return;
			case ISkillsPackage.TASK__REQUIREMENT:
				setRequirement((IDependency)newValue);
				return;
			case ISkillsPackage.TASK__TASKS:
				getTasks().clear();
				getTasks().addAll((Collection<? extends ITask>)newValue);
				return;
			case ISkillsPackage.TASK__HINTS:
				getHints().clear();
				getHints().addAll((Collection<? extends IHint>)newValue);
				return;
			case ISkillsPackage.TASK__AUTO_ACTIVATION:
				setAutoActivation((Boolean)newValue);
				return;
			case ISkillsPackage.TASK__SKILL_SERVICE:
				setSkillService((ISkillService)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ISkillsPackage.TASK__DESCRIPTION:
				setDescription((IDescription)null);
				return;
			case ISkillsPackage.TASK__GOAL:
				setGoal((IDependency)null);
				return;
			case ISkillsPackage.TASK__REWARDS:
				getRewards().clear();
				return;
			case ISkillsPackage.TASK__TITLE:
				setTitle(TITLE_EDEFAULT);
				return;
			case ISkillsPackage.TASK__REQUIREMENT:
				setRequirement((IDependency)null);
				return;
			case ISkillsPackage.TASK__TASKS:
				getTasks().clear();
				return;
			case ISkillsPackage.TASK__HINTS:
				getHints().clear();
				return;
			case ISkillsPackage.TASK__AUTO_ACTIVATION:
				setAutoActivation(AUTO_ACTIVATION_EDEFAULT);
				return;
			case ISkillsPackage.TASK__SKILL_SERVICE:
				setSkillService(SKILL_SERVICE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ISkillsPackage.TASK__DESCRIPTION:
				return description != null;
			case ISkillsPackage.TASK__GOAL:
				return goal != null;
			case ISkillsPackage.TASK__REWARDS:
				return rewards != null && !rewards.isEmpty();
			case ISkillsPackage.TASK__TITLE:
				return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
			case ISkillsPackage.TASK__REQUIREMENT:
				return requirement != null;
			case ISkillsPackage.TASK__TASKS:
				return tasks != null && !tasks.isEmpty();
			case ISkillsPackage.TASK__HINTS:
				return hints != null && !hints.isEmpty();
			case ISkillsPackage.TASK__AUTO_ACTIVATION:
				return autoActivation != AUTO_ACTIVATION_EDEFAULT;
			case ISkillsPackage.TASK__SKILL_SERVICE:
				return SKILL_SERVICE_EDEFAULT == null ? skillService != null : !SKILL_SERVICE_EDEFAULT.equals(skillService);
			case ISkillsPackage.TASK__ID:
				return ID_EDEFAULT == null ? getId() != null : !ID_EDEFAULT.equals(getId());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ISkillsPackage.TASK___IS_AVAILABLE:
				return isAvailable();
			case ISkillsPackage.TASK___GET_SHORT_TITLE__INT:
				return getShortTitle((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (title: ");
		result.append(title);
		result.append(", autoActivation: ");
		result.append(autoActivation);
		result.append(", skillService: ");
		result.append(skillService);
		result.append(')');
		return result.toString();
	}

	/**
	 * @generated NOT
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((getId() == null) ? 0 : getId().hashCode());
		return result;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final MTask other = (MTask) obj;
		if (getId() == null) {
			if (other.getId() != null)
				return false;
		} else if (!getId().equals(other.getId()))
			return false;
		return true;
	}

} // MTask
