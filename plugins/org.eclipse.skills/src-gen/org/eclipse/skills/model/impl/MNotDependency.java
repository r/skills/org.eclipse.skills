/**
 */
package org.eclipse.skills.model.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.skills.model.INotDependency;
import org.eclipse.skills.model.ISkillsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Not Dependency</b></em>'. <!-- end-user-doc -->
 *
 * @generated
 */
public class MNotDependency extends MContainedDependency implements INotDependency {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MNotDependency() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ISkillsPackage.Literals.NOT_DEPENDENCY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public void evaluateDependencies() {
		if (!getDependencies().isEmpty())
			setFulfilled(!getDependencies().get(0).isFulfilled());
		else
			setFulfilled(false);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.skills.model.impl.MContainedDependency#activate()
	 */
	@Override
	public void activate() {
		// TODO Auto-generated method stub
		super.activate();
	}
} // MNotDependency
