/**
 */
package org.eclipse.skills.model.impl;

import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.skills.Activator;
import org.eclipse.skills.Logger;
import org.eclipse.skills.model.IDescription;
import org.eclipse.skills.model.ILevelProgression;
import org.eclipse.skills.model.ISkill;
import org.eclipse.skills.model.ISkillsFactory;
import org.eclipse.skills.model.ISkillsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Skill</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.eclipse.skills.model.impl.MSkill#getName <em>Name</em>}</li>
 * <li>{@link org.eclipse.skills.model.impl.MSkill#getDescription <em>Description</em>}</li>
 * <li>{@link org.eclipse.skills.model.impl.MSkill#getExperience <em>Experience</em>}</li>
 * <li>{@link org.eclipse.skills.model.impl.MSkill#getProgression <em>Progression</em>}</li>
 * <li>{@link org.eclipse.skills.model.impl.MSkill#isBaseSkill <em>Base Skill</em>}</li>
 * <li>{@link org.eclipse.skills.model.impl.MSkill#getImageURI <em>Image URI</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MSkill extends MinimalEObjectImpl.Container implements ISkill {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = "<unnamed>";

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected IDescription description;

	/**
	 * The default value of the '{@link #getExperience() <em>Experience</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @see #getExperience()
	 * @generated
	 * @ordered
	 */
	protected static final int EXPERIENCE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getExperience() <em>Experience</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @see #getExperience()
	 * @generated
	 * @ordered
	 */
	protected int experience = EXPERIENCE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProgression() <em>Progression</em>}' containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @see #getProgression()
	 * @generated
	 * @ordered
	 */
	protected ILevelProgression progression;

	/**
	 * The default value of the '{@link #isBaseSkill() <em>Base Skill</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @see #isBaseSkill()
	 * @generated
	 * @ordered
	 */
	protected static final boolean BASE_SKILL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isBaseSkill() <em>Base Skill</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @see #isBaseSkill()
	 * @generated
	 * @ordered
	 */
	protected boolean baseSkill = BASE_SKILL_EDEFAULT;

	/**
	 * The default value of the '{@link #getImageURI() <em>Image URI</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @see #getImageURI()
	 * @generated
	 * @ordered
	 */
	protected static final String IMAGE_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getImageURI() <em>Image URI</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @see #getImageURI()
	 * @generated
	 * @ordered
	 */
	protected String imageURI = IMAGE_URI_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	protected MSkill() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ISkillsPackage.Literals.SKILL;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		final String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.SKILL__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public IDescription getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	public NotificationChain basicSetDescription(IDescription newDescription, NotificationChain msgs) {
		final IDescription oldDescription = description;
		description = newDescription;
		if (eNotificationRequired()) {
			final ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ISkillsPackage.SKILL__DESCRIPTION, oldDescription,
					newDescription);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public void setDescription(IDescription newDescription) {
		if (newDescription != description) {
			NotificationChain msgs = null;
			if (description != null)
				msgs = ((InternalEObject) description).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ISkillsPackage.SKILL__DESCRIPTION, null, msgs);
			if (newDescription != null)
				msgs = ((InternalEObject) newDescription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ISkillsPackage.SKILL__DESCRIPTION, null, msgs);
			msgs = basicSetDescription(newDescription, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.SKILL__DESCRIPTION, newDescription, newDescription));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public int getExperience() {
		return experience;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public void setExperience(int newExperience) {
		final int oldExperience = experience;
		experience = newExperience;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.SKILL__EXPERIENCE, oldExperience, experience));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	public ILevelProgression getProgressionGen() {
		return progression;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public ILevelProgression getProgression() {
		if (getProgressionGen() == null)
			setProgression(ISkillsFactory.eINSTANCE.createFactorProgression());

		return getProgressionGen();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	public NotificationChain basicSetProgression(ILevelProgression newProgression, NotificationChain msgs) {
		final ILevelProgression oldProgression = progression;
		progression = newProgression;
		if (eNotificationRequired()) {
			final ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ISkillsPackage.SKILL__PROGRESSION, oldProgression,
					newProgression);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public void setProgression(ILevelProgression newProgression) {
		if (newProgression != progression) {
			NotificationChain msgs = null;
			if (progression != null)
				msgs = ((InternalEObject) progression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ISkillsPackage.SKILL__PROGRESSION, null, msgs);
			if (newProgression != null)
				msgs = ((InternalEObject) newProgression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ISkillsPackage.SKILL__PROGRESSION, null, msgs);
			msgs = basicSetProgression(newProgression, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.SKILL__PROGRESSION, newProgression, newProgression));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public boolean isBaseSkill() {
		return baseSkill;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public void setBaseSkill(boolean newBaseSkill) {
		final boolean oldBaseSkill = baseSkill;
		baseSkill = newBaseSkill;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.SKILL__BASE_SKILL, oldBaseSkill, baseSkill));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public String getImageURI() {
		return imageURI;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public void setImageURI(String newImageURI) {
		final String oldImageURI = imageURI;
		imageURI = newImageURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.SKILL__IMAGE_URI, oldImageURI, imageURI));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public synchronized void addExperience(int amount) {
		setExperience(getExperience() + amount);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public ImageDescriptor getImageDescriptor() {
		if ((getImageURI() != null) && (!getImageURI().isEmpty())) {
			try {
				return ImageDescriptor.createFromURL(new URL(getImageURI()));
			} catch (final MalformedURLException e) {
				Logger.error(Activator.PLUGIN_ID, "Could not load skill image from" + getImageURI());
				return ImageDescriptor.getMissingImageDescriptor();
			}
		}

		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ISkillsPackage.SKILL__DESCRIPTION:
			return basicSetDescription(null, msgs);
		case ISkillsPackage.SKILL__PROGRESSION:
			return basicSetProgression(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ISkillsPackage.SKILL__NAME:
			return getName();
		case ISkillsPackage.SKILL__DESCRIPTION:
			return getDescription();
		case ISkillsPackage.SKILL__EXPERIENCE:
			return getExperience();
		case ISkillsPackage.SKILL__PROGRESSION:
			return getProgression();
		case ISkillsPackage.SKILL__BASE_SKILL:
			return isBaseSkill();
		case ISkillsPackage.SKILL__IMAGE_URI:
			return getImageURI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ISkillsPackage.SKILL__NAME:
			setName((String) newValue);
			return;
		case ISkillsPackage.SKILL__DESCRIPTION:
			setDescription((IDescription) newValue);
			return;
		case ISkillsPackage.SKILL__EXPERIENCE:
			setExperience((Integer) newValue);
			return;
		case ISkillsPackage.SKILL__PROGRESSION:
			setProgression((ILevelProgression) newValue);
			return;
		case ISkillsPackage.SKILL__BASE_SKILL:
			setBaseSkill((Boolean) newValue);
			return;
		case ISkillsPackage.SKILL__IMAGE_URI:
			setImageURI((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ISkillsPackage.SKILL__NAME:
			setName(NAME_EDEFAULT);
			return;
		case ISkillsPackage.SKILL__DESCRIPTION:
			setDescription((IDescription) null);
			return;
		case ISkillsPackage.SKILL__EXPERIENCE:
			setExperience(EXPERIENCE_EDEFAULT);
			return;
		case ISkillsPackage.SKILL__PROGRESSION:
			setProgression((ILevelProgression) null);
			return;
		case ISkillsPackage.SKILL__BASE_SKILL:
			setBaseSkill(BASE_SKILL_EDEFAULT);
			return;
		case ISkillsPackage.SKILL__IMAGE_URI:
			setImageURI(IMAGE_URI_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ISkillsPackage.SKILL__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case ISkillsPackage.SKILL__DESCRIPTION:
			return description != null;
		case ISkillsPackage.SKILL__EXPERIENCE:
			return experience != EXPERIENCE_EDEFAULT;
		case ISkillsPackage.SKILL__PROGRESSION:
			return progression != null;
		case ISkillsPackage.SKILL__BASE_SKILL:
			return baseSkill != BASE_SKILL_EDEFAULT;
		case ISkillsPackage.SKILL__IMAGE_URI:
			return IMAGE_URI_EDEFAULT == null ? imageURI != null : !IMAGE_URI_EDEFAULT.equals(imageURI);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ISkillsPackage.SKILL___ADD_EXPERIENCE__INT:
			addExperience((Integer) arguments.get(0));
			return null;
		case ISkillsPackage.SKILL___GET_IMAGE_DESCRIPTOR:
			return getImageDescriptor();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		final StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", experience: ");
		result.append(experience);
		result.append(", baseSkill: ");
		result.append(baseSkill);
		result.append(", imageURI: ");
		result.append(imageURI);
		result.append(')');
		return result.toString();
	}

} // MSkill
