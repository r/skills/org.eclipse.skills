/**
 */
package org.eclipse.skills.model.impl;

import java.util.Date;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.skills.dependencies.CustomDependencyDefinition;
import org.eclipse.skills.model.IAndDependency;
import org.eclipse.skills.model.IBadge;
import org.eclipse.skills.model.IBadgeReward;
import org.eclipse.skills.model.ICompleteIncludedTasksDependency;
import org.eclipse.skills.model.IContainedDependency;
import org.eclipse.skills.model.ICustomDependency;
import org.eclipse.skills.model.IDelayedDependency;
import org.eclipse.skills.model.IDependency;
import org.eclipse.skills.model.IDependencyWithAttributes;
import org.eclipse.skills.model.IDescription;
import org.eclipse.skills.model.IExperienceReward;
import org.eclipse.skills.model.IFactorProgression;
import org.eclipse.skills.model.IHint;
import org.eclipse.skills.model.ILevelProgression;
import org.eclipse.skills.model.INotDependency;
import org.eclipse.skills.model.IOrDependency;
import org.eclipse.skills.model.IQuest;
import org.eclipse.skills.model.IReward;
import org.eclipse.skills.model.ISequenceDependency;
import org.eclipse.skills.model.ISkill;
import org.eclipse.skills.model.ISkillDependency;
import org.eclipse.skills.model.ISkillReward;
import org.eclipse.skills.model.ISkillsFactory;
import org.eclipse.skills.model.ISkillsPackage;
import org.eclipse.skills.model.ITask;
import org.eclipse.skills.model.ITaskDependency;
import org.eclipse.skills.model.IUser;
import org.eclipse.skills.model.IUserDependency;
import org.eclipse.skills.model.IUserTask;
import org.eclipse.skills.model.IUserTitle;
import org.eclipse.skills.model.LevelName;
import org.eclipse.skills.service.ISkillService;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MSkillsPackage extends EPackageImpl implements ISkillsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass taskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass descriptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rewardEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass badgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass userEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass skillEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass userTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass questEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass badgeRewardEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass experienceRewardEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass skillRewardEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass containedDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass andDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass orDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass notDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass delayedDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dependencyWithAttributesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sequenceDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass completeIncludedTasksDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass taskDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass skillDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass userDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass levelProgressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass factorProgressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass userTitleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum levelNameEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType dateEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType customDependencyDefinitionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType imageDescriptorEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType iSkillServiceEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.skills.model.ISkillsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MSkillsPackage() {
		super(eNS_URI, ISkillsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ISkillsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ISkillsPackage init() {
		if (isInited) return (ISkillsPackage)EPackage.Registry.INSTANCE.getEPackage(ISkillsPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredSkillsPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		MSkillsPackage theSkillsPackage = registeredSkillsPackage instanceof MSkillsPackage ? (MSkillsPackage)registeredSkillsPackage : new MSkillsPackage();

		isInited = true;

		// Create package meta-data objects
		theSkillsPackage.createPackageContents();

		// Initialize created meta-data
		theSkillsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSkillsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ISkillsPackage.eNS_URI, theSkillsPackage);
		return theSkillsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTask() {
		return taskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTask_Description() {
		return (EReference)taskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTask_Goal() {
		return (EReference)taskEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTask_Rewards() {
		return (EReference)taskEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTask_Title() {
		return (EAttribute)taskEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTask_Requirement() {
		return (EReference)taskEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTask_Tasks() {
		return (EReference)taskEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTask_Hints() {
		return (EReference)taskEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTask_AutoActivation() {
		return (EAttribute)taskEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTask_SkillService() {
		return (EAttribute)taskEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTask_Id() {
		return (EAttribute)taskEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getTask__IsAvailable() {
		return taskEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getTask__GetShortTitle__int() {
		return taskEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDescription() {
		return descriptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDescription_Text() {
		return (EAttribute)descriptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getReward() {
		return rewardEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getReward__PayOut__IUser() {
		return rewardEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBadge() {
		return badgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBadge_Title() {
		return (EAttribute)badgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBadge_ImageURL() {
		return (EAttribute)badgeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getBadge__GetImageDescriptor() {
		return badgeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUser() {
		return userEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUser_Name() {
		return (EAttribute)userEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUser_Skills() {
		return (EReference)userEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUser_Usertasks() {
		return (EReference)userEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUser_ImageLocation() {
		return (EAttribute)userEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUser_Experience() {
		return (EReference)userEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUser_Badges() {
		return (EReference)userEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUser_Title() {
		return (EAttribute)userEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUser__AddTask__ITask() {
		return userEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUser__GetSkill__ISkill() {
		return userEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUser__Consume__IReward() {
		return userEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUser__GetAvatar() {
		return userEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUser__GetSkill__String() {
		return userEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSkill() {
		return skillEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSkill_Name() {
		return (EAttribute)skillEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkill_Description() {
		return (EReference)skillEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSkill_Experience() {
		return (EAttribute)skillEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkill_Progression() {
		return (EReference)skillEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSkill_BaseSkill() {
		return (EAttribute)skillEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSkill_ImageURI() {
		return (EAttribute)skillEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSkill__AddExperience__int() {
		return skillEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSkill__GetImageDescriptor() {
		return skillEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUserTask() {
		return userTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUserTask_Task() {
		return (EReference)userTaskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUserTask_Started() {
		return (EAttribute)userTaskEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUserTask_Finished() {
		return (EAttribute)userTaskEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUserTask_HintsDisplayed() {
		return (EAttribute)userTaskEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUserTask_Added() {
		return (EAttribute)userTaskEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUserTask__IsCompleted() {
		return userTaskEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUserTask__Activate() {
		return userTaskEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUserTask__GetUser() {
		return userTaskEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUserTask__IsStarted() {
		return userTaskEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUserTask__RevealNextHint() {
		return userTaskEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getQuest() {
		return questEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getQuest_Tasks() {
		return (EReference)questEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getQuest_Skills() {
		return (EReference)questEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getQuest_Title() {
		return (EAttribute)questEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getQuest_UserTitles() {
		return (EReference)questEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDependency() {
		return dependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDependency_Fulfilled() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDependency__Activate() {
		return dependencyEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDependency__Deactivate() {
		return dependencyEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBadgeReward() {
		return badgeRewardEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBadgeReward_Badge() {
		return (EReference)badgeRewardEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getExperienceReward() {
		return experienceRewardEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getExperienceReward_Experience() {
		return (EAttribute)experienceRewardEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSkillReward() {
		return skillRewardEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillReward_Skill() {
		return (EReference)skillRewardEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getContainedDependency() {
		return containedDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getContainedDependency_Dependencies() {
		return (EReference)containedDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getContainedDependency__EvaluateDependencies() {
		return containedDependencyEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAndDependency() {
		return andDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOrDependency() {
		return orDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNotDependency() {
		return notDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDelayedDependency() {
		return delayedDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDelayedDependency_Delay() {
		return (EAttribute)delayedDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCustomDependency() {
		return customDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCustomDependency_ExtensionId() {
		return (EAttribute)customDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCustomDependency_ExtensionAttribute() {
		return (EAttribute)customDependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCustomDependency_Dependency() {
		return (EReference)customDependencyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCustomDependency_Definition() {
		return (EAttribute)customDependencyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDependencyWithAttributes() {
		return dependencyWithAttributesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDependencyWithAttributes__SetAttributes__String() {
		return dependencyWithAttributesEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSequenceDependency() {
		return sequenceDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSequenceDependency_Completed() {
		return (EAttribute)sequenceDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCompleteIncludedTasksDependency() {
		return completeIncludedTasksDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTaskDependency() {
		return taskDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTaskDependency_Task() {
		return (EReference)taskDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTaskDependency_Completed() {
		return (EAttribute)taskDependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSkillDependency() {
		return skillDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSkillDependency_MinExperience() {
		return (EAttribute)skillDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSkillDependency_MinLevel() {
		return (EAttribute)skillDependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillDependency_Skill() {
		return (EReference)skillDependencyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHint() {
		return hintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHint_Penalty() {
		return (EAttribute)hintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUserDependency() {
		return userDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUserDependency_User() {
		return (EReference)userDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLevelProgression() {
		return levelProgressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getLevelProgression__GetMinimumXpForLevel__int() {
		return levelProgressionEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getLevelProgression__GetLevel__int() {
		return levelProgressionEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFactorProgression() {
		return factorProgressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFactorProgression_BaseXpNeeded() {
		return (EAttribute)factorProgressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFactorProgression_XpFactor() {
		return (EAttribute)factorProgressionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUserTitle() {
		return userTitleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUserTitle_MinLevel() {
		return (EAttribute)userTitleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUserTitle_Skill() {
		return (EReference)userTitleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUserTitle_DisplayString() {
		return (EAttribute)userTitleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getLevelName() {
		return levelNameEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getDate() {
		return dateEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getCustomDependencyDefinition() {
		return customDependencyDefinitionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getImageDescriptor() {
		return imageDescriptorEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getISkillService() {
		return iSkillServiceEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISkillsFactory getSkillsFactory() {
		return (ISkillsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		taskEClass = createEClass(TASK);
		createEReference(taskEClass, TASK__DESCRIPTION);
		createEReference(taskEClass, TASK__GOAL);
		createEReference(taskEClass, TASK__REWARDS);
		createEAttribute(taskEClass, TASK__TITLE);
		createEReference(taskEClass, TASK__REQUIREMENT);
		createEReference(taskEClass, TASK__TASKS);
		createEReference(taskEClass, TASK__HINTS);
		createEAttribute(taskEClass, TASK__AUTO_ACTIVATION);
		createEAttribute(taskEClass, TASK__SKILL_SERVICE);
		createEAttribute(taskEClass, TASK__ID);
		createEOperation(taskEClass, TASK___IS_AVAILABLE);
		createEOperation(taskEClass, TASK___GET_SHORT_TITLE__INT);

		descriptionEClass = createEClass(DESCRIPTION);
		createEAttribute(descriptionEClass, DESCRIPTION__TEXT);

		rewardEClass = createEClass(REWARD);
		createEOperation(rewardEClass, REWARD___PAY_OUT__IUSER);

		badgeEClass = createEClass(BADGE);
		createEAttribute(badgeEClass, BADGE__TITLE);
		createEAttribute(badgeEClass, BADGE__IMAGE_URL);
		createEOperation(badgeEClass, BADGE___GET_IMAGE_DESCRIPTOR);

		userEClass = createEClass(USER);
		createEAttribute(userEClass, USER__NAME);
		createEReference(userEClass, USER__SKILLS);
		createEReference(userEClass, USER__USERTASKS);
		createEAttribute(userEClass, USER__IMAGE_LOCATION);
		createEReference(userEClass, USER__EXPERIENCE);
		createEReference(userEClass, USER__BADGES);
		createEAttribute(userEClass, USER__TITLE);
		createEOperation(userEClass, USER___ADD_TASK__ITASK);
		createEOperation(userEClass, USER___GET_SKILL__ISKILL);
		createEOperation(userEClass, USER___CONSUME__IREWARD);
		createEOperation(userEClass, USER___GET_AVATAR);
		createEOperation(userEClass, USER___GET_SKILL__STRING);

		skillEClass = createEClass(SKILL);
		createEAttribute(skillEClass, SKILL__NAME);
		createEReference(skillEClass, SKILL__DESCRIPTION);
		createEAttribute(skillEClass, SKILL__EXPERIENCE);
		createEReference(skillEClass, SKILL__PROGRESSION);
		createEAttribute(skillEClass, SKILL__BASE_SKILL);
		createEAttribute(skillEClass, SKILL__IMAGE_URI);
		createEOperation(skillEClass, SKILL___ADD_EXPERIENCE__INT);
		createEOperation(skillEClass, SKILL___GET_IMAGE_DESCRIPTOR);

		userTaskEClass = createEClass(USER_TASK);
		createEReference(userTaskEClass, USER_TASK__TASK);
		createEAttribute(userTaskEClass, USER_TASK__STARTED);
		createEAttribute(userTaskEClass, USER_TASK__FINISHED);
		createEAttribute(userTaskEClass, USER_TASK__HINTS_DISPLAYED);
		createEAttribute(userTaskEClass, USER_TASK__ADDED);
		createEOperation(userTaskEClass, USER_TASK___IS_COMPLETED);
		createEOperation(userTaskEClass, USER_TASK___ACTIVATE);
		createEOperation(userTaskEClass, USER_TASK___GET_USER);
		createEOperation(userTaskEClass, USER_TASK___IS_STARTED);
		createEOperation(userTaskEClass, USER_TASK___REVEAL_NEXT_HINT);

		questEClass = createEClass(QUEST);
		createEReference(questEClass, QUEST__TASKS);
		createEReference(questEClass, QUEST__SKILLS);
		createEAttribute(questEClass, QUEST__TITLE);
		createEReference(questEClass, QUEST__USER_TITLES);

		dependencyEClass = createEClass(DEPENDENCY);
		createEAttribute(dependencyEClass, DEPENDENCY__FULFILLED);
		createEOperation(dependencyEClass, DEPENDENCY___ACTIVATE);
		createEOperation(dependencyEClass, DEPENDENCY___DEACTIVATE);

		badgeRewardEClass = createEClass(BADGE_REWARD);
		createEReference(badgeRewardEClass, BADGE_REWARD__BADGE);

		experienceRewardEClass = createEClass(EXPERIENCE_REWARD);
		createEAttribute(experienceRewardEClass, EXPERIENCE_REWARD__EXPERIENCE);

		skillRewardEClass = createEClass(SKILL_REWARD);
		createEReference(skillRewardEClass, SKILL_REWARD__SKILL);

		containedDependencyEClass = createEClass(CONTAINED_DEPENDENCY);
		createEReference(containedDependencyEClass, CONTAINED_DEPENDENCY__DEPENDENCIES);
		createEOperation(containedDependencyEClass, CONTAINED_DEPENDENCY___EVALUATE_DEPENDENCIES);

		andDependencyEClass = createEClass(AND_DEPENDENCY);

		orDependencyEClass = createEClass(OR_DEPENDENCY);

		notDependencyEClass = createEClass(NOT_DEPENDENCY);

		delayedDependencyEClass = createEClass(DELAYED_DEPENDENCY);
		createEAttribute(delayedDependencyEClass, DELAYED_DEPENDENCY__DELAY);

		customDependencyEClass = createEClass(CUSTOM_DEPENDENCY);
		createEAttribute(customDependencyEClass, CUSTOM_DEPENDENCY__EXTENSION_ID);
		createEAttribute(customDependencyEClass, CUSTOM_DEPENDENCY__EXTENSION_ATTRIBUTE);
		createEReference(customDependencyEClass, CUSTOM_DEPENDENCY__DEPENDENCY);
		createEAttribute(customDependencyEClass, CUSTOM_DEPENDENCY__DEFINITION);

		dependencyWithAttributesEClass = createEClass(DEPENDENCY_WITH_ATTRIBUTES);
		createEOperation(dependencyWithAttributesEClass, DEPENDENCY_WITH_ATTRIBUTES___SET_ATTRIBUTES__STRING);

		sequenceDependencyEClass = createEClass(SEQUENCE_DEPENDENCY);
		createEAttribute(sequenceDependencyEClass, SEQUENCE_DEPENDENCY__COMPLETED);

		completeIncludedTasksDependencyEClass = createEClass(COMPLETE_INCLUDED_TASKS_DEPENDENCY);

		taskDependencyEClass = createEClass(TASK_DEPENDENCY);
		createEReference(taskDependencyEClass, TASK_DEPENDENCY__TASK);
		createEAttribute(taskDependencyEClass, TASK_DEPENDENCY__COMPLETED);

		skillDependencyEClass = createEClass(SKILL_DEPENDENCY);
		createEAttribute(skillDependencyEClass, SKILL_DEPENDENCY__MIN_EXPERIENCE);
		createEAttribute(skillDependencyEClass, SKILL_DEPENDENCY__MIN_LEVEL);
		createEReference(skillDependencyEClass, SKILL_DEPENDENCY__SKILL);

		hintEClass = createEClass(HINT);
		createEAttribute(hintEClass, HINT__PENALTY);

		userDependencyEClass = createEClass(USER_DEPENDENCY);
		createEReference(userDependencyEClass, USER_DEPENDENCY__USER);

		levelProgressionEClass = createEClass(LEVEL_PROGRESSION);
		createEOperation(levelProgressionEClass, LEVEL_PROGRESSION___GET_MINIMUM_XP_FOR_LEVEL__INT);
		createEOperation(levelProgressionEClass, LEVEL_PROGRESSION___GET_LEVEL__INT);

		factorProgressionEClass = createEClass(FACTOR_PROGRESSION);
		createEAttribute(factorProgressionEClass, FACTOR_PROGRESSION__BASE_XP_NEEDED);
		createEAttribute(factorProgressionEClass, FACTOR_PROGRESSION__XP_FACTOR);

		userTitleEClass = createEClass(USER_TITLE);
		createEAttribute(userTitleEClass, USER_TITLE__MIN_LEVEL);
		createEReference(userTitleEClass, USER_TITLE__SKILL);
		createEAttribute(userTitleEClass, USER_TITLE__DISPLAY_STRING);

		// Create enums
		levelNameEEnum = createEEnum(LEVEL_NAME);

		// Create data types
		dateEDataType = createEDataType(DATE);
		customDependencyDefinitionEDataType = createEDataType(CUSTOM_DEPENDENCY_DEFINITION);
		imageDescriptorEDataType = createEDataType(IMAGE_DESCRIPTOR);
		iSkillServiceEDataType = createEDataType(ISKILL_SERVICE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		badgeRewardEClass.getESuperTypes().add(this.getReward());
		experienceRewardEClass.getESuperTypes().add(this.getReward());
		skillRewardEClass.getESuperTypes().add(this.getExperienceReward());
		containedDependencyEClass.getESuperTypes().add(this.getDependency());
		andDependencyEClass.getESuperTypes().add(this.getContainedDependency());
		orDependencyEClass.getESuperTypes().add(this.getContainedDependency());
		notDependencyEClass.getESuperTypes().add(this.getContainedDependency());
		delayedDependencyEClass.getESuperTypes().add(this.getContainedDependency());
		customDependencyEClass.getESuperTypes().add(this.getDependency());
		dependencyWithAttributesEClass.getESuperTypes().add(this.getDependency());
		sequenceDependencyEClass.getESuperTypes().add(this.getAndDependency());
		completeIncludedTasksDependencyEClass.getESuperTypes().add(this.getUserDependency());
		taskDependencyEClass.getESuperTypes().add(this.getUserDependency());
		skillDependencyEClass.getESuperTypes().add(this.getUserDependency());
		hintEClass.getESuperTypes().add(this.getDescription());
		userDependencyEClass.getESuperTypes().add(this.getDependency());
		factorProgressionEClass.getESuperTypes().add(this.getLevelProgression());

		// Initialize classes, features, and operations; add parameters
		initEClass(taskEClass, ITask.class, "Task", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTask_Description(), this.getDescription(), null, "description", null, 1, 1, ITask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTask_Goal(), this.getDependency(), null, "goal", null, 1, 1, ITask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTask_Rewards(), this.getReward(), null, "rewards", null, 0, -1, ITask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTask_Title(), ecorePackage.getEString(), "title", null, 0, 1, ITask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTask_Requirement(), this.getDependency(), null, "requirement", null, 1, 1, ITask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTask_Tasks(), this.getTask(), null, "tasks", null, 0, -1, ITask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTask_Hints(), this.getHint(), null, "hints", null, 0, -1, ITask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTask_AutoActivation(), ecorePackage.getEBoolean(), "autoActivation", "true", 0, 1, ITask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTask_SkillService(), this.getISkillService(), "skillService", null, 0, 1, ITask.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTask_Id(), ecorePackage.getEString(), "id", null, 1, 1, ITask.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEOperation(getTask__IsAvailable(), ecorePackage.getEBoolean(), "isAvailable", 0, 1, IS_UNIQUE, IS_ORDERED);

		EOperation op = initEOperation(getTask__GetShortTitle__int(), ecorePackage.getEString(), "getShortTitle", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "maxLength", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(descriptionEClass, IDescription.class, "Description", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDescription_Text(), ecorePackage.getEString(), "text", "<description>", 0, 1, IDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(rewardEClass, IReward.class, "Reward", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getReward__PayOut__IUser(), null, "payOut", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getUser(), "user", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(badgeEClass, IBadge.class, "Badge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBadge_Title(), ecorePackage.getEString(), "title", null, 0, 1, IBadge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBadge_ImageURL(), ecorePackage.getEString(), "imageURL", null, 0, 1, IBadge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getBadge__GetImageDescriptor(), this.getImageDescriptor(), "getImageDescriptor", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(userEClass, IUser.class, "User", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUser_Name(), ecorePackage.getEString(), "name", null, 0, 1, IUser.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUser_Skills(), this.getSkill(), null, "skills", null, 0, -1, IUser.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUser_Usertasks(), this.getUserTask(), null, "usertasks", null, 0, -1, IUser.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUser_ImageLocation(), ecorePackage.getEString(), "imageLocation", null, 0, 1, IUser.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUser_Experience(), this.getSkill(), null, "experience", null, 1, 1, IUser.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUser_Badges(), this.getBadge(), null, "badges", null, 0, -1, IUser.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUser_Title(), ecorePackage.getEString(), "title", null, 1, 1, IUser.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getUser__AddTask__ITask(), this.getUserTask(), "addTask", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTask(), "task", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getUser__GetSkill__ISkill(), this.getSkill(), "getSkill", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSkill(), "skill", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getUser__Consume__IReward(), null, "consume", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getReward(), "reward", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getUser__GetAvatar(), this.getImageDescriptor(), "getAvatar", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getUser__GetSkill__String(), this.getSkill(), "getSkill", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(skillEClass, ISkill.class, "Skill", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSkill_Name(), ecorePackage.getEString(), "name", "<unnamed>", 1, 1, ISkill.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSkill_Description(), this.getDescription(), null, "description", null, 1, 1, ISkill.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSkill_Experience(), ecorePackage.getEInt(), "experience", null, 0, 1, ISkill.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSkill_Progression(), this.getLevelProgression(), null, "progression", null, 1, 1, ISkill.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSkill_BaseSkill(), ecorePackage.getEBoolean(), "baseSkill", "false", 0, 1, ISkill.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSkill_ImageURI(), ecorePackage.getEString(), "imageURI", null, 0, 1, ISkill.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getSkill__AddExperience__int(), null, "addExperience", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "amount", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getSkill__GetImageDescriptor(), this.getImageDescriptor(), "getImageDescriptor", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(userTaskEClass, IUserTask.class, "UserTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUserTask_Task(), this.getTask(), null, "task", null, 1, 1, IUserTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserTask_Started(), this.getDate(), "started", null, 0, 1, IUserTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserTask_Finished(), this.getDate(), "finished", null, 0, 1, IUserTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserTask_HintsDisplayed(), ecorePackage.getEInt(), "hintsDisplayed", null, 0, 1, IUserTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserTask_Added(), this.getDate(), "added", null, 1, 1, IUserTask.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getUserTask__IsCompleted(), ecorePackage.getEBoolean(), "isCompleted", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getUserTask__Activate(), null, "activate", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getUserTask__GetUser(), this.getUser(), "getUser", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getUserTask__IsStarted(), ecorePackage.getEBoolean(), "isStarted", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getUserTask__RevealNextHint(), null, "revealNextHint", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(questEClass, IQuest.class, "Quest", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getQuest_Tasks(), this.getTask(), null, "tasks", null, 0, -1, IQuest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getQuest_Skills(), this.getSkill(), null, "skills", null, 0, -1, IQuest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getQuest_Title(), ecorePackage.getEString(), "title", null, 0, 1, IQuest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getQuest_UserTitles(), this.getUserTitle(), null, "userTitles", null, 0, -1, IQuest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dependencyEClass, IDependency.class, "Dependency", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDependency_Fulfilled(), ecorePackage.getEBoolean(), "fulfilled", "false", 0, 1, IDependency.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getDependency__Activate(), null, "activate", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getDependency__Deactivate(), null, "deactivate", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(badgeRewardEClass, IBadgeReward.class, "BadgeReward", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBadgeReward_Badge(), this.getBadge(), null, "badge", null, 1, 1, IBadgeReward.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(experienceRewardEClass, IExperienceReward.class, "ExperienceReward", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExperienceReward_Experience(), ecorePackage.getEInt(), "experience", null, 0, 1, IExperienceReward.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(skillRewardEClass, ISkillReward.class, "SkillReward", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSkillReward_Skill(), this.getSkill(), null, "skill", null, 1, 1, ISkillReward.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(containedDependencyEClass, IContainedDependency.class, "ContainedDependency", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getContainedDependency_Dependencies(), this.getDependency(), null, "dependencies", null, 0, -1, IContainedDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getContainedDependency__EvaluateDependencies(), null, "evaluateDependencies", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(andDependencyEClass, IAndDependency.class, "AndDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(orDependencyEClass, IOrDependency.class, "OrDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(notDependencyEClass, INotDependency.class, "NotDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(delayedDependencyEClass, IDelayedDependency.class, "DelayedDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDelayedDependency_Delay(), ecorePackage.getEInt(), "delay", null, 0, 1, IDelayedDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(customDependencyEClass, ICustomDependency.class, "CustomDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCustomDependency_ExtensionId(), ecorePackage.getEString(), "extensionId", null, 0, 1, ICustomDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCustomDependency_ExtensionAttribute(), ecorePackage.getEString(), "extensionAttribute", null, 0, 1, ICustomDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCustomDependency_Dependency(), this.getDependency(), null, "dependency", null, 0, 1, ICustomDependency.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getCustomDependency_Definition(), this.getCustomDependencyDefinition(), "definition", null, 0, 1, ICustomDependency.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(dependencyWithAttributesEClass, IDependencyWithAttributes.class, "DependencyWithAttributes", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getDependencyWithAttributes__SetAttributes__String(), null, "setAttributes", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "attributes", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(sequenceDependencyEClass, ISequenceDependency.class, "SequenceDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSequenceDependency_Completed(), ecorePackage.getEInt(), "completed", null, 0, 1, ISequenceDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(completeIncludedTasksDependencyEClass, ICompleteIncludedTasksDependency.class, "CompleteIncludedTasksDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(taskDependencyEClass, ITaskDependency.class, "TaskDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTaskDependency_Task(), this.getTask(), null, "task", null, 1, 1, ITaskDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTaskDependency_Completed(), ecorePackage.getEBoolean(), "completed", "true", 0, 1, ITaskDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(skillDependencyEClass, ISkillDependency.class, "SkillDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSkillDependency_MinExperience(), ecorePackage.getEInt(), "minExperience", "0", 0, 1, ISkillDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSkillDependency_MinLevel(), ecorePackage.getEInt(), "minLevel", "0", 0, 1, ISkillDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSkillDependency_Skill(), this.getSkill(), null, "skill", null, 1, 1, ISkillDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hintEClass, IHint.class, "Hint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHint_Penalty(), ecorePackage.getEInt(), "penalty", "10", 0, 1, IHint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(userDependencyEClass, IUserDependency.class, "UserDependency", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUserDependency_User(), this.getUser(), null, "user", null, 0, 1, IUserDependency.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(levelProgressionEClass, ILevelProgression.class, "LevelProgression", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getLevelProgression__GetMinimumXpForLevel__int(), ecorePackage.getEInt(), "getMinimumXpForLevel", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "level", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getLevelProgression__GetLevel__int(), ecorePackage.getEInt(), "getLevel", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "xp", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(factorProgressionEClass, IFactorProgression.class, "FactorProgression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFactorProgression_BaseXpNeeded(), ecorePackage.getEInt(), "baseXpNeeded", "100", 1, 1, IFactorProgression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFactorProgression_XpFactor(), ecorePackage.getEDouble(), "xpFactor", "2.4", 1, 1, IFactorProgression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(userTitleEClass, IUserTitle.class, "UserTitle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUserTitle_MinLevel(), ecorePackage.getEInt(), "minLevel", null, 1, 1, IUserTitle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUserTitle_Skill(), this.getSkill(), null, "skill", null, 0, 1, IUserTitle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserTitle_DisplayString(), ecorePackage.getEString(), "displayString", null, 1, 1, IUserTitle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(levelNameEEnum, LevelName.class, "LevelName");
		addEEnumLiteral(levelNameEEnum, LevelName.APPRENTICE);
		addEEnumLiteral(levelNameEEnum, LevelName.EXPERT);

		// Initialize data types
		initEDataType(dateEDataType, Date.class, "Date", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(customDependencyDefinitionEDataType, CustomDependencyDefinition.class, "CustomDependencyDefinition", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(imageDescriptorEDataType, ImageDescriptor.class, "ImageDescriptor", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(iSkillServiceEDataType, ISkillService.class, "ISkillService", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //MSkillsPackage
