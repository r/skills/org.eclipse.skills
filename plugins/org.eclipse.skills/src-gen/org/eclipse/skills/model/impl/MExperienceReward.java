/**
 */
package org.eclipse.skills.model.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.skills.model.IExperienceReward;
import org.eclipse.skills.model.ISkillsPackage;
import org.eclipse.skills.model.IUser;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Experience Reward</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.impl.MExperienceReward#getExperience <em>Experience</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MExperienceReward extends MinimalEObjectImpl.Container implements IExperienceReward {
	/**
	 * The default value of the '{@link #getExperience() <em>Experience</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getExperience()
	 * @generated
	 * @ordered
	 */
	protected static final int EXPERIENCE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getExperience() <em>Experience</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getExperience()
	 * @generated
	 * @ordered
	 */
	protected int experience = EXPERIENCE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MExperienceReward() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ISkillsPackage.Literals.EXPERIENCE_REWARD;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getExperience() {
		return experience;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setExperience(int newExperience) {
		int oldExperience = experience;
		experience = newExperience;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.EXPERIENCE_REWARD__EXPERIENCE, oldExperience, experience));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public void payOut(IUser user) {
		user.getExperience().addExperience(getExperience());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ISkillsPackage.EXPERIENCE_REWARD__EXPERIENCE:
				return getExperience();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ISkillsPackage.EXPERIENCE_REWARD__EXPERIENCE:
				setExperience((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ISkillsPackage.EXPERIENCE_REWARD__EXPERIENCE:
				setExperience(EXPERIENCE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ISkillsPackage.EXPERIENCE_REWARD__EXPERIENCE:
				return experience != EXPERIENCE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ISkillsPackage.EXPERIENCE_REWARD___PAY_OUT__IUSER:
				payOut((IUser)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (experience: ");
		result.append(experience);
		result.append(')');
		return result.toString();
	}

} // MExperienceReward
