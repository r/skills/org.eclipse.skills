/**
 */
package org.eclipse.skills.model.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.skills.model.IQuest;
import org.eclipse.skills.model.ISkill;
import org.eclipse.skills.model.ISkillsPackage;
import org.eclipse.skills.model.ITask;
import org.eclipse.skills.model.IUserTitle;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Quest</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.impl.MQuest#getTasks <em>Tasks</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MQuest#getSkills <em>Skills</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MQuest#getTitle <em>Title</em>}</li>
 *   <li>{@link org.eclipse.skills.model.impl.MQuest#getUserTitles <em>User Titles</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MQuest extends MinimalEObjectImpl.Container implements IQuest {
	/**
	 * The cached value of the '{@link #getTasks() <em>Tasks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTasks()
	 * @generated
	 * @ordered
	 */
	protected EList<ITask> tasks;

	/**
	 * The cached value of the '{@link #getSkills() <em>Skills</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSkills()
	 * @generated
	 * @ordered
	 */
	protected EList<ISkill> skills;

	/**
	 * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected static final String TITLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected String title = TITLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getUserTitles() <em>User Titles</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserTitles()
	 * @generated
	 * @ordered
	 */
	protected EList<IUserTitle> userTitles;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MQuest() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ISkillsPackage.Literals.QUEST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ITask> getTasks() {
		if (tasks == null) {
			tasks = new EObjectContainmentEList<ITask>(ITask.class, this, ISkillsPackage.QUEST__TASKS);
		}
		return tasks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISkill> getSkills() {
		if (skills == null) {
			skills = new EObjectContainmentEList<ISkill>(ISkill.class, this, ISkillsPackage.QUEST__SKILLS);
		}
		return skills;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTitle() {
		return title;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTitle(String newTitle) {
		String oldTitle = title;
		title = newTitle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ISkillsPackage.QUEST__TITLE, oldTitle, title));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<IUserTitle> getUserTitles() {
		if (userTitles == null) {
			userTitles = new EObjectContainmentEList<IUserTitle>(IUserTitle.class, this, ISkillsPackage.QUEST__USER_TITLES);
		}
		return userTitles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ISkillsPackage.QUEST__TASKS:
				return ((InternalEList<?>)getTasks()).basicRemove(otherEnd, msgs);
			case ISkillsPackage.QUEST__SKILLS:
				return ((InternalEList<?>)getSkills()).basicRemove(otherEnd, msgs);
			case ISkillsPackage.QUEST__USER_TITLES:
				return ((InternalEList<?>)getUserTitles()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ISkillsPackage.QUEST__TASKS:
				return getTasks();
			case ISkillsPackage.QUEST__SKILLS:
				return getSkills();
			case ISkillsPackage.QUEST__TITLE:
				return getTitle();
			case ISkillsPackage.QUEST__USER_TITLES:
				return getUserTitles();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ISkillsPackage.QUEST__TASKS:
				getTasks().clear();
				getTasks().addAll((Collection<? extends ITask>)newValue);
				return;
			case ISkillsPackage.QUEST__SKILLS:
				getSkills().clear();
				getSkills().addAll((Collection<? extends ISkill>)newValue);
				return;
			case ISkillsPackage.QUEST__TITLE:
				setTitle((String)newValue);
				return;
			case ISkillsPackage.QUEST__USER_TITLES:
				getUserTitles().clear();
				getUserTitles().addAll((Collection<? extends IUserTitle>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ISkillsPackage.QUEST__TASKS:
				getTasks().clear();
				return;
			case ISkillsPackage.QUEST__SKILLS:
				getSkills().clear();
				return;
			case ISkillsPackage.QUEST__TITLE:
				setTitle(TITLE_EDEFAULT);
				return;
			case ISkillsPackage.QUEST__USER_TITLES:
				getUserTitles().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ISkillsPackage.QUEST__TASKS:
				return tasks != null && !tasks.isEmpty();
			case ISkillsPackage.QUEST__SKILLS:
				return skills != null && !skills.isEmpty();
			case ISkillsPackage.QUEST__TITLE:
				return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
			case ISkillsPackage.QUEST__USER_TITLES:
				return userTitles != null && !userTitles.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (title: ");
		result.append(title);
		result.append(')');
		return result.toString();
	}

} //MQuest
