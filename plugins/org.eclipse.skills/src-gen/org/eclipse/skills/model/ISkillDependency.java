/**
 */
package org.eclipse.skills.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Skill Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.ISkillDependency#getMinExperience <em>Min Experience</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ISkillDependency#getMinLevel <em>Min Level</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ISkillDependency#getSkill <em>Skill</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getSkillDependency()
 * @model
 * @generated
 */
public interface ISkillDependency extends IUserDependency {
	/**
	 * Returns the value of the '<em><b>Min Experience</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Experience</em>' attribute.
	 * @see #setMinExperience(int)
	 * @see org.eclipse.skills.model.ISkillsPackage#getSkillDependency_MinExperience()
	 * @model default="0"
	 * @generated
	 */
	int getMinExperience();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ISkillDependency#getMinExperience <em>Min Experience</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Experience</em>' attribute.
	 * @see #getMinExperience()
	 * @generated
	 */
	void setMinExperience(int value);

	/**
	 * Returns the value of the '<em><b>Min Level</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Level</em>' attribute.
	 * @see #setMinLevel(int)
	 * @see org.eclipse.skills.model.ISkillsPackage#getSkillDependency_MinLevel()
	 * @model default="0"
	 * @generated
	 */
	int getMinLevel();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ISkillDependency#getMinLevel <em>Min Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Level</em>' attribute.
	 * @see #getMinLevel()
	 * @generated
	 */
	void setMinLevel(int value);

	/**
	 * Returns the value of the '<em><b>Skill</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Skill</em>' reference.
	 * @see #setSkill(ISkill)
	 * @see org.eclipse.skills.model.ISkillsPackage#getSkillDependency_Skill()
	 * @model required="true"
	 * @generated
	 */
	ISkill getSkill();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ISkillDependency#getSkill <em>Skill</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Skill</em>' reference.
	 * @see #getSkill()
	 * @generated
	 */
	void setSkill(ISkill value);

} // ISkillDependency
