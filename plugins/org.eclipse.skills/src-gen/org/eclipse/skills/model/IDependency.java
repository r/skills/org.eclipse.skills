/**
 */
package org.eclipse.skills.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.IDependency#isFulfilled <em>Fulfilled</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getDependency()
 * @model abstract="true"
 * @generated
 */
public interface IDependency extends EObject {
	/**
	 * Returns the value of the '<em><b>Fulfilled</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fulfilled</em>' attribute.
	 * @see #setFulfilled(boolean)
	 * @see org.eclipse.skills.model.ISkillsPackage#getDependency_Fulfilled()
	 * @model default="false" transient="true"
	 * @generated
	 */
	boolean isFulfilled();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IDependency#isFulfilled <em>Fulfilled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fulfilled</em>' attribute.
	 * @see #isFulfilled()
	 * @generated
	 */
	void setFulfilled(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void activate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void deactivate();

} // IDependency
