/**
 */
package org.eclipse.skills.model;

import java.util.Date;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.IUserTask#getTask <em>Task</em>}</li>
 *   <li>{@link org.eclipse.skills.model.IUserTask#getStarted <em>Started</em>}</li>
 *   <li>{@link org.eclipse.skills.model.IUserTask#getFinished <em>Finished</em>}</li>
 *   <li>{@link org.eclipse.skills.model.IUserTask#getHintsDisplayed <em>Hints Displayed</em>}</li>
 *   <li>{@link org.eclipse.skills.model.IUserTask#getAdded <em>Added</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getUserTask()
 * @model
 * @generated
 */
public interface IUserTask extends EObject {
	/**
	 * Returns the value of the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task</em>' reference.
	 * @see #setTask(ITask)
	 * @see org.eclipse.skills.model.ISkillsPackage#getUserTask_Task()
	 * @model required="true"
	 * @generated
	 */
	ITask getTask();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IUserTask#getTask <em>Task</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task</em>' reference.
	 * @see #getTask()
	 * @generated
	 */
	void setTask(ITask value);

	/**
	 * Returns the value of the '<em><b>Started</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Started</em>' attribute.
	 * @see #setStarted(Date)
	 * @see org.eclipse.skills.model.ISkillsPackage#getUserTask_Started()
	 * @model dataType="org.eclipse.skills.model.Date"
	 * @generated
	 */
	Date getStarted();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IUserTask#getStarted <em>Started</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Started</em>' attribute.
	 * @see #getStarted()
	 * @generated
	 */
	void setStarted(Date value);

	/**
	 * Returns the value of the '<em><b>Finished</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Finished</em>' attribute.
	 * @see #setFinished(Date)
	 * @see org.eclipse.skills.model.ISkillsPackage#getUserTask_Finished()
	 * @model dataType="org.eclipse.skills.model.Date"
	 * @generated
	 */
	Date getFinished();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IUserTask#getFinished <em>Finished</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Finished</em>' attribute.
	 * @see #getFinished()
	 * @generated
	 */
	void setFinished(Date value);

	/**
	 * Returns the value of the '<em><b>Hints Displayed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hints Displayed</em>' attribute.
	 * @see #setHintsDisplayed(int)
	 * @see org.eclipse.skills.model.ISkillsPackage#getUserTask_HintsDisplayed()
	 * @model
	 * @generated
	 */
	int getHintsDisplayed();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IUserTask#getHintsDisplayed <em>Hints Displayed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hints Displayed</em>' attribute.
	 * @see #getHintsDisplayed()
	 * @generated
	 */
	void setHintsDisplayed(int value);

	/**
	 * Returns the value of the '<em><b>Added</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Added</em>' attribute.
	 * @see org.eclipse.skills.model.ISkillsPackage#getUserTask_Added()
	 * @model dataType="org.eclipse.skills.model.Date" required="true" changeable="false"
	 * @generated
	 */
	Date getAdded();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isCompleted();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void activate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	IUser getUser();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isStarted();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void revealNextHint();

} // IUserTask
