/**
 */
package org.eclipse.skills.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Contained Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.IContainedDependency#getDependencies <em>Dependencies</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getContainedDependency()
 * @model abstract="true"
 * @generated
 */
public interface IContainedDependency extends IDependency {
	/**
	 * Returns the value of the '<em><b>Dependencies</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.skills.model.IDependency}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependencies</em>' containment reference list.
	 * @see org.eclipse.skills.model.ISkillsPackage#getContainedDependency_Dependencies()
	 * @model containment="true"
	 * @generated
	 */
	EList<IDependency> getDependencies();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void evaluateDependencies();

} // IContainedDependency
