/**
 */
package org.eclipse.skills.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Skill Goal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.ISkillGoal#getExperience <em>Experience</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ISkillGoal#getLevel <em>Level</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getSkillGoal()
 * @model
 * @generated
 */
public interface ISkillGoal extends IDependency {
	/**
	 * Returns the value of the '<em><b>Experience</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Experience</em>' attribute.
	 * @see #setExperience(int)
	 * @see org.eclipse.skills.model.ISkillsPackage#getSkillGoal_Experience()
	 * @model default="0"
	 * @generated
	 */
	int getExperience();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ISkillGoal#getExperience <em>Experience</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Experience</em>' attribute.
	 * @see #getExperience()
	 * @generated
	 */
	void setExperience(int value);

	/**
	 * Returns the value of the '<em><b>Level</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level</em>' attribute.
	 * @see #setLevel(int)
	 * @see org.eclipse.skills.model.ISkillsPackage#getSkillGoal_Level()
	 * @model default="0"
	 * @generated
	 */
	int getLevel();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ISkillGoal#getLevel <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level</em>' attribute.
	 * @see #getLevel()
	 * @generated
	 */
	void setLevel(int value);

} // ISkillGoal
