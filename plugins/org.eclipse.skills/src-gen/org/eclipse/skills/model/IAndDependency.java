/**
 */
package org.eclipse.skills.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>And Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getAndDependency()
 * @model
 * @generated
 */
public interface IAndDependency extends IContainedDependency {
} // IAndDependency
