/**
 */
package org.eclipse.skills.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dependency With Attributes</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getDependencyWithAttributes()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IDependencyWithAttributes extends IDependency {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void setAttributes(String attributes);

} // IDependencyWithAttributes
