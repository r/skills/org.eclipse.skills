/**
 */
package org.eclipse.skills.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User Title</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.IUserTitle#getMinLevel <em>Min Level</em>}</li>
 *   <li>{@link org.eclipse.skills.model.IUserTitle#getSkill <em>Skill</em>}</li>
 *   <li>{@link org.eclipse.skills.model.IUserTitle#getDisplayString <em>Display String</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getUserTitle()
 * @model
 * @generated
 */
public interface IUserTitle extends EObject {
	/**
	 * Returns the value of the '<em><b>Min Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Level</em>' attribute.
	 * @see #setMinLevel(int)
	 * @see org.eclipse.skills.model.ISkillsPackage#getUserTitle_MinLevel()
	 * @model required="true"
	 * @generated
	 */
	int getMinLevel();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IUserTitle#getMinLevel <em>Min Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Level</em>' attribute.
	 * @see #getMinLevel()
	 * @generated
	 */
	void setMinLevel(int value);

	/**
	 * Returns the value of the '<em><b>Skill</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Skill</em>' reference.
	 * @see #setSkill(ISkill)
	 * @see org.eclipse.skills.model.ISkillsPackage#getUserTitle_Skill()
	 * @model
	 * @generated
	 */
	ISkill getSkill();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IUserTitle#getSkill <em>Skill</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Skill</em>' reference.
	 * @see #getSkill()
	 * @generated
	 */
	void setSkill(ISkill value);

	/**
	 * Returns the value of the '<em><b>Display String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Display String</em>' attribute.
	 * @see #setDisplayString(String)
	 * @see org.eclipse.skills.model.ISkillsPackage#getUserTitle_DisplayString()
	 * @model required="true"
	 * @generated
	 */
	String getDisplayString();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IUserTitle#getDisplayString <em>Display String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Display String</em>' attribute.
	 * @see #getDisplayString()
	 * @generated
	 */
	void setDisplayString(String value);

} // IUserTitle
