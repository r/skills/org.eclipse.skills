/**
 */
package org.eclipse.skills.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Description</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.IDescription#getText <em>Text</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getDescription()
 * @model
 * @generated
 */
public interface IDescription extends EObject {

	/**
	 * Returns the value of the '<em><b>Text</b></em>' attribute.
	 * The default value is <code>"<description>"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Text</em>' attribute.
	 * @see #setText(String)
	 * @see org.eclipse.skills.model.ISkillsPackage#getDescription_Text()
	 * @model default="&lt;description&gt;"
	 * @generated
	 */
	String getText();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IDescription#getText <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Text</em>' attribute.
	 * @see #getText()
	 * @generated
	 */
	void setText(String value);
} // IDescription
