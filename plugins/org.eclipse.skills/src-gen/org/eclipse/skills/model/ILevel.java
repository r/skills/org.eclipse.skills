/**
 */
package org.eclipse.skills.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Level</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.ILevel#getExperiencePerLevel <em>Experience Per Level</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ILevel#getLevelFactor <em>Level Factor</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ILevel#getLevel <em>Level</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getLevel()
 * @model
 * @generated
 */
public interface ILevel extends EObject {
	/**
	 * Returns the value of the '<em><b>Experience Per Level</b></em>' attribute.
	 * The default value is <code>"100"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Experience Per Level</em>' attribute.
	 * @see #setExperiencePerLevel(int)
	 * @see org.eclipse.skills.model.ISkillsPackage#getLevel_ExperiencePerLevel()
	 * @model default="100"
	 * @generated
	 */
	int getExperiencePerLevel();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ILevel#getExperiencePerLevel <em>Experience Per Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Experience Per Level</em>' attribute.
	 * @see #getExperiencePerLevel()
	 * @generated
	 */
	void setExperiencePerLevel(int value);

	/**
	 * Returns the value of the '<em><b>Level Factor</b></em>' attribute.
	 * The default value is <code>"1.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level Factor</em>' attribute.
	 * @see #setLevelFactor(double)
	 * @see org.eclipse.skills.model.ISkillsPackage#getLevel_LevelFactor()
	 * @model default="1.0"
	 * @generated
	 */
	double getLevelFactor();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ILevel#getLevelFactor <em>Level Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level Factor</em>' attribute.
	 * @see #getLevelFactor()
	 * @generated
	 */
	void setLevelFactor(double value);

	/**
	 * Returns the value of the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level</em>' attribute.
	 * @see org.eclipse.skills.model.ISkillsPackage#getLevel_Level()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	int getLevel();

} // ILevel
