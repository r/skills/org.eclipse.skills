/**
 */
package org.eclipse.skills.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Factor Progression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.IFactorProgression#getBaseXpNeeded <em>Base Xp Needed</em>}</li>
 *   <li>{@link org.eclipse.skills.model.IFactorProgression#getXpFactor <em>Xp Factor</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getFactorProgression()
 * @model
 * @generated
 */
public interface IFactorProgression extends ILevelProgression {
	/**
	 * Returns the value of the '<em><b>Base Xp Needed</b></em>' attribute.
	 * The default value is <code>"100"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Xp Needed</em>' attribute.
	 * @see #setBaseXpNeeded(int)
	 * @see org.eclipse.skills.model.ISkillsPackage#getFactorProgression_BaseXpNeeded()
	 * @model default="100" required="true"
	 * @generated
	 */
	int getBaseXpNeeded();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IFactorProgression#getBaseXpNeeded <em>Base Xp Needed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Xp Needed</em>' attribute.
	 * @see #getBaseXpNeeded()
	 * @generated
	 */
	void setBaseXpNeeded(int value);

	/**
	 * Returns the value of the '<em><b>Xp Factor</b></em>' attribute.
	 * The default value is <code>"2.4"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Xp Factor</em>' attribute.
	 * @see #setXpFactor(double)
	 * @see org.eclipse.skills.model.ISkillsPackage#getFactorProgression_XpFactor()
	 * @model default="2.4" required="true"
	 * @generated
	 */
	double getXpFactor();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IFactorProgression#getXpFactor <em>Xp Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Xp Factor</em>' attribute.
	 * @see #getXpFactor()
	 * @generated
	 */
	void setXpFactor(double value);

} // IFactorProgression
