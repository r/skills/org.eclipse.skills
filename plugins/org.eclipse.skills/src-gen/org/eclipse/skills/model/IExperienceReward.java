/**
 */
package org.eclipse.skills.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Experience Reward</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.IExperienceReward#getExperience <em>Experience</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getExperienceReward()
 * @model
 * @generated
 */
public interface IExperienceReward extends IReward {
	/**
	 * Returns the value of the '<em><b>Experience</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Experience</em>' attribute.
	 * @see #setExperience(int)
	 * @see org.eclipse.skills.model.ISkillsPackage#getExperienceReward_Experience()
	 * @model
	 * @generated
	 */
	int getExperience();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IExperienceReward#getExperience <em>Experience</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Experience</em>' attribute.
	 * @see #getExperience()
	 * @generated
	 */
	void setExperience(int value);

} // IExperienceReward
