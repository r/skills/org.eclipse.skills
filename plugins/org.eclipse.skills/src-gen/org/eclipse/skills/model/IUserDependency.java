/**
 */
package org.eclipse.skills.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.IUserDependency#getUser <em>User</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getUserDependency()
 * @model abstract="true"
 * @generated
 */
public interface IUserDependency extends IDependency {
	/**
	 * Returns the value of the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User</em>' reference.
	 * @see #setUser(IUser)
	 * @see org.eclipse.skills.model.ISkillsPackage#getUserDependency_User()
	 * @model resolveProxies="false" transient="true" ordered="false"
	 * @generated
	 */
	IUser getUser();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IUserDependency#getUser <em>User</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User</em>' reference.
	 * @see #getUser()
	 * @generated
	 */
	void setUser(IUser value);

} // IUserDependency
