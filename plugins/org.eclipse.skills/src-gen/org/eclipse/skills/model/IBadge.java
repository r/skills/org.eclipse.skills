/**
 */
package org.eclipse.skills.model;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.resource.ImageDescriptor;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Badge</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.IBadge#getTitle <em>Title</em>}</li>
 *   <li>{@link org.eclipse.skills.model.IBadge#getImageURL <em>Image URL</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getBadge()
 * @model
 * @generated
 */
public interface IBadge extends EObject {
	/**
	 * Returns the value of the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the value of the '<em>Title</em>' attribute.
	 * @see #setTitle(String)
	 * @see org.eclipse.skills.model.ISkillsPackage#getBadge_Title()
	 * @model
	 * @generated
	 */
	String getTitle();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IBadge#getTitle <em>Title</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title</em>' attribute.
	 * @see #getTitle()
	 * @generated
	 */
	void setTitle(String value);

	/**
	 * Returns the value of the '<em><b>Image URL</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the value of the '<em>Image URL</em>' attribute.
	 * @see #setImageURL(String)
	 * @see org.eclipse.skills.model.ISkillsPackage#getBadge_ImageURL()
	 * @model
	 * @generated
	 */
	String getImageURL();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IBadge#getImageURL <em>Image URL</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image URL</em>' attribute.
	 * @see #getImageURL()
	 * @generated
	 */
	void setImageURL(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="org.eclipse.skills.model.ImageDescriptor"
	 * @generated
	 */
	ImageDescriptor getImageDescriptor();

} // IBadge
