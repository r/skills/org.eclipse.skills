/**
 */
package org.eclipse.skills.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reward</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getReward()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IReward extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void payOut(IUser user);

} // IReward
