/**
 */
package org.eclipse.skills.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.IHint#getPenalty <em>Penalty</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getHint()
 * @model
 * @generated
 */
public interface IHint extends IDescription {
	/**
	 * Returns the value of the '<em><b>Penalty</b></em>' attribute.
	 * The default value is <code>"10"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Penalty</em>' attribute.
	 * @see #setPenalty(int)
	 * @see org.eclipse.skills.model.ISkillsPackage#getHint_Penalty()
	 * @model default="10"
	 * @generated
	 */
	int getPenalty();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IHint#getPenalty <em>Penalty</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Penalty</em>' attribute.
	 * @see #getPenalty()
	 * @generated
	 */
	void setPenalty(int value);

} // IHint
