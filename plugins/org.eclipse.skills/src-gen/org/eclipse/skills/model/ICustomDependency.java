/**
 */
package org.eclipse.skills.model;

import org.eclipse.skills.dependencies.CustomDependencyDefinition;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Custom Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.ICustomDependency#getExtensionId <em>Extension Id</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ICustomDependency#getExtensionAttribute <em>Extension Attribute</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ICustomDependency#getDependency <em>Dependency</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ICustomDependency#getDefinition <em>Definition</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getCustomDependency()
 * @model
 * @generated
 */
public interface ICustomDependency extends IDependency {
	/**
	 * Returns the value of the '<em><b>Extension Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extension Id</em>' attribute.
	 * @see #setExtensionId(String)
	 * @see org.eclipse.skills.model.ISkillsPackage#getCustomDependency_ExtensionId()
	 * @model
	 * @generated
	 */
	String getExtensionId();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ICustomDependency#getExtensionId <em>Extension Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extension Id</em>' attribute.
	 * @see #getExtensionId()
	 * @generated
	 */
	void setExtensionId(String value);

	/**
	 * Returns the value of the '<em><b>Extension Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extension Attribute</em>' attribute.
	 * @see #setExtensionAttribute(String)
	 * @see org.eclipse.skills.model.ISkillsPackage#getCustomDependency_ExtensionAttribute()
	 * @model
	 * @generated
	 */
	String getExtensionAttribute();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ICustomDependency#getExtensionAttribute <em>Extension Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extension Attribute</em>' attribute.
	 * @see #getExtensionAttribute()
	 * @generated
	 */
	void setExtensionAttribute(String value);

	/**
	 * Returns the value of the '<em><b>Dependency</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependency</em>' containment reference.
	 * @see org.eclipse.skills.model.ISkillsPackage#getCustomDependency_Dependency()
	 * @model containment="true" transient="true" changeable="false" derived="true"
	 * @generated
	 */
	IDependency getDependency();

	/**
	 * Returns the value of the '<em><b>Definition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition</em>' attribute.
	 * @see #setDefinition(CustomDependencyDefinition)
	 * @see org.eclipse.skills.model.ISkillsPackage#getCustomDependency_Definition()
	 * @model dataType="org.eclipse.skills.model.CustomDependencyDefinition" transient="true" derived="true"
	 * @generated
	 */
	CustomDependencyDefinition getDefinition();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ICustomDependency#getDefinition <em>Definition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definition</em>' attribute.
	 * @see #getDefinition()
	 * @generated
	 */
	void setDefinition(CustomDependencyDefinition value);

} // ICustomDependency
