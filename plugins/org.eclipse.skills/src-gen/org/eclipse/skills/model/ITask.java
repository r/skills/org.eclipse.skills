/**
 */
package org.eclipse.skills.model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.skills.service.ISkillService;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.ITask#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ITask#getGoal <em>Goal</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ITask#getRewards <em>Rewards</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ITask#getTitle <em>Title</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ITask#getRequirement <em>Requirement</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ITask#getTasks <em>Tasks</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ITask#getHints <em>Hints</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ITask#isAutoActivation <em>Auto Activation</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ITask#getSkillService <em>Skill Service</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ITask#getId <em>Id</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getTask()
 * @model
 * @generated
 */
public interface ITask extends EObject {
	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference.
	 * @see #setDescription(IDescription)
	 * @see org.eclipse.skills.model.ISkillsPackage#getTask_Description()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IDescription getDescription();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ITask#getDescription <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' containment reference.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(IDescription value);

	/**
	 * Returns the value of the '<em><b>Goal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Goal</em>' containment reference.
	 * @see #setGoal(IDependency)
	 * @see org.eclipse.skills.model.ISkillsPackage#getTask_Goal()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IDependency getGoal();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ITask#getGoal <em>Goal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Goal</em>' containment reference.
	 * @see #getGoal()
	 * @generated
	 */
	void setGoal(IDependency value);

	/**
	 * Returns the value of the '<em><b>Rewards</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.skills.model.IReward}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rewards</em>' containment reference list.
	 * @see org.eclipse.skills.model.ISkillsPackage#getTask_Rewards()
	 * @model containment="true"
	 * @generated
	 */
	EList<IReward> getRewards();

	/**
	 * Returns the value of the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Title</em>' attribute.
	 * @see #setTitle(String)
	 * @see org.eclipse.skills.model.ISkillsPackage#getTask_Title()
	 * @model
	 * @generated
	 */
	String getTitle();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ITask#getTitle <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title</em>' attribute.
	 * @see #getTitle()
	 * @generated
	 */
	void setTitle(String value);

	/**
	 * Returns the value of the '<em><b>Requirement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requirement</em>' containment reference.
	 * @see #setRequirement(IDependency)
	 * @see org.eclipse.skills.model.ISkillsPackage#getTask_Requirement()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IDependency getRequirement();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ITask#getRequirement <em>Requirement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Requirement</em>' containment reference.
	 * @see #getRequirement()
	 * @generated
	 */
	void setRequirement(IDependency value);

	/**
	 * Returns the value of the '<em><b>Tasks</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.skills.model.ITask}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tasks</em>' containment reference list.
	 * @see org.eclipse.skills.model.ISkillsPackage#getTask_Tasks()
	 * @model containment="true"
	 * @generated
	 */
	EList<ITask> getTasks();

	/**
	 * Returns the value of the '<em><b>Hints</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.skills.model.IHint}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hints</em>' containment reference list.
	 * @see org.eclipse.skills.model.ISkillsPackage#getTask_Hints()
	 * @model containment="true"
	 * @generated
	 */
	EList<IHint> getHints();

	/**
	 * Returns the value of the '<em><b>Auto Activation</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Auto Activation</em>' attribute.
	 * @see #setAutoActivation(boolean)
	 * @see org.eclipse.skills.model.ISkillsPackage#getTask_AutoActivation()
	 * @model default="true"
	 * @generated
	 */
	boolean isAutoActivation();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ITask#isAutoActivation <em>Auto Activation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Auto Activation</em>' attribute.
	 * @see #isAutoActivation()
	 * @generated
	 */
	void setAutoActivation(boolean value);

	/**
	 * Returns the value of the '<em><b>Skill Service</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Skill Service</em>' attribute.
	 * @see #setSkillService(ISkillService)
	 * @see org.eclipse.skills.model.ISkillsPackage#getTask_SkillService()
	 * @model dataType="org.eclipse.skills.model.ISkillService" transient="true"
	 * @generated
	 */
	ISkillService getSkillService();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ITask#getSkillService <em>Skill Service</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Skill Service</em>' attribute.
	 * @see #getSkillService()
	 * @generated
	 */
	void setSkillService(ISkillService value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see org.eclipse.skills.model.ISkillsPackage#getTask_Id()
	 * @model id="true" required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getId();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isAvailable();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	String getShortTitle(int maxLength);

} // ITask
