/**
 */
package org.eclipse.skills.model;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.skills.model.ISkillsPackage
 * @generated
 */
public interface ISkillsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ISkillsFactory eINSTANCE = org.eclipse.skills.model.impl.MSkillsFactory.init();

	/**
	 * Returns a new object of class '<em>Task</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Task</em>'.
	 * @generated
	 */
	ITask createTask();

	/**
	 * Returns a new object of class '<em>Description</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Description</em>'.
	 * @generated
	 */
	IDescription createDescription();

	/**
	 * Returns a new object of class '<em>Badge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Badge</em>'.
	 * @generated
	 */
	IBadge createBadge();

	/**
	 * Returns a new object of class '<em>User</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>User</em>'.
	 * @generated
	 */
	IUser createUser();

	/**
	 * Returns a new object of class '<em>Skill</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Skill</em>'.
	 * @generated
	 */
	ISkill createSkill();

	/**
	 * Returns a new object of class '<em>User Task</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>User Task</em>'.
	 * @generated
	 */
	IUserTask createUserTask();

	/**
	 * Returns a new object of class '<em>Quest</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Quest</em>'.
	 * @generated
	 */
	IQuest createQuest();

	/**
	 * Returns a new object of class '<em>Badge Reward</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Badge Reward</em>'.
	 * @generated
	 */
	IBadgeReward createBadgeReward();

	/**
	 * Returns a new object of class '<em>Experience Reward</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Experience Reward</em>'.
	 * @generated
	 */
	IExperienceReward createExperienceReward();

	/**
	 * Returns a new object of class '<em>Skill Reward</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Skill Reward</em>'.
	 * @generated
	 */
	ISkillReward createSkillReward();

	/**
	 * Returns a new object of class '<em>And Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>And Dependency</em>'.
	 * @generated
	 */
	IAndDependency createAndDependency();

	/**
	 * Returns a new object of class '<em>Or Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Or Dependency</em>'.
	 * @generated
	 */
	IOrDependency createOrDependency();

	/**
	 * Returns a new object of class '<em>Not Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Not Dependency</em>'.
	 * @generated
	 */
	INotDependency createNotDependency();

	/**
	 * Returns a new object of class '<em>Delayed Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Delayed Dependency</em>'.
	 * @generated
	 */
	IDelayedDependency createDelayedDependency();

	/**
	 * Returns a new object of class '<em>Custom Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Custom Dependency</em>'.
	 * @generated
	 */
	ICustomDependency createCustomDependency();

	/**
	 * Returns a new object of class '<em>Sequence Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sequence Dependency</em>'.
	 * @generated
	 */
	ISequenceDependency createSequenceDependency();

	/**
	 * Returns a new object of class '<em>Complete Included Tasks Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Complete Included Tasks Dependency</em>'.
	 * @generated
	 */
	ICompleteIncludedTasksDependency createCompleteIncludedTasksDependency();

	/**
	 * Returns a new object of class '<em>Task Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Task Dependency</em>'.
	 * @generated
	 */
	ITaskDependency createTaskDependency();

	/**
	 * Returns a new object of class '<em>Skill Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Skill Dependency</em>'.
	 * @generated
	 */
	ISkillDependency createSkillDependency();

	/**
	 * Returns a new object of class '<em>Hint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hint</em>'.
	 * @generated
	 */
	IHint createHint();

	/**
	 * Returns a new object of class '<em>Factor Progression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Factor Progression</em>'.
	 * @generated
	 */
	IFactorProgression createFactorProgression();

	/**
	 * Returns a new object of class '<em>User Title</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>User Title</em>'.
	 * @generated
	 */
	IUserTitle createUserTitle();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ISkillsPackage getSkillsPackage();

} //ISkillsFactory
