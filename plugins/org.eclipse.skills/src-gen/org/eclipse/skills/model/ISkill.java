/**
 */
package org.eclipse.skills.model;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.resource.ImageDescriptor;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Skill</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.ISkill#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ISkill#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ISkill#getExperience <em>Experience</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ISkill#getProgression <em>Progression</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ISkill#isBaseSkill <em>Base Skill</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ISkill#getImageURI <em>Image URI</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getSkill()
 * @model
 * @generated
 */
public interface ISkill extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * The default value is <code>"<unnamed>"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.skills.model.ISkillsPackage#getSkill_Name()
	 * @model default="&lt;unnamed&gt;" id="true" required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ISkill#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference.
	 * @see #setDescription(IDescription)
	 * @see org.eclipse.skills.model.ISkillsPackage#getSkill_Description()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IDescription getDescription();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ISkill#getDescription <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' containment reference.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(IDescription value);

	/**
	 * Returns the value of the '<em><b>Experience</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Experience</em>' attribute.
	 * @see #setExperience(int)
	 * @see org.eclipse.skills.model.ISkillsPackage#getSkill_Experience()
	 * @model
	 * @generated
	 */
	int getExperience();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ISkill#getExperience <em>Experience</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Experience</em>' attribute.
	 * @see #getExperience()
	 * @generated
	 */
	void setExperience(int value);

	/**
	 * Returns the value of the '<em><b>Progression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Progression</em>' containment reference.
	 * @see #setProgression(ILevelProgression)
	 * @see org.eclipse.skills.model.ISkillsPackage#getSkill_Progression()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ILevelProgression getProgression();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ISkill#getProgression <em>Progression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Progression</em>' containment reference.
	 * @see #getProgression()
	 * @generated
	 */
	void setProgression(ILevelProgression value);

	/**
	 * Returns the value of the '<em><b>Base Skill</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Skill</em>' attribute.
	 * @see #setBaseSkill(boolean)
	 * @see org.eclipse.skills.model.ISkillsPackage#getSkill_BaseSkill()
	 * @model default="false"
	 * @generated
	 */
	boolean isBaseSkill();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ISkill#isBaseSkill <em>Base Skill</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Skill</em>' attribute.
	 * @see #isBaseSkill()
	 * @generated
	 */
	void setBaseSkill(boolean value);

	/**
	 * Returns the value of the '<em><b>Image URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image URI</em>' attribute.
	 * @see #setImageURI(String)
	 * @see org.eclipse.skills.model.ISkillsPackage#getSkill_ImageURI()
	 * @model
	 * @generated
	 */
	String getImageURI();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ISkill#getImageURI <em>Image URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image URI</em>' attribute.
	 * @see #getImageURI()
	 * @generated
	 */
	void setImageURI(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void addExperience(int amount);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="org.eclipse.skills.model.ImageDescriptor"
	 * @generated
	 */
	ImageDescriptor getImageDescriptor();

} // ISkill
