/**
 */
package org.eclipse.skills.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.ITaskDependency#getTask <em>Task</em>}</li>
 *   <li>{@link org.eclipse.skills.model.ITaskDependency#isCompleted <em>Completed</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getTaskDependency()
 * @model
 * @generated
 */
public interface ITaskDependency extends IUserDependency {
	/**
	 * Returns the value of the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task</em>' reference.
	 * @see #setTask(ITask)
	 * @see org.eclipse.skills.model.ISkillsPackage#getTaskDependency_Task()
	 * @model required="true"
	 * @generated
	 */
	ITask getTask();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ITaskDependency#getTask <em>Task</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task</em>' reference.
	 * @see #getTask()
	 * @generated
	 */
	void setTask(ITask value);

	/**
	 * Returns the value of the '<em><b>Completed</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Completed</em>' attribute.
	 * @see #setCompleted(boolean)
	 * @see org.eclipse.skills.model.ISkillsPackage#getTaskDependency_Completed()
	 * @model default="true"
	 * @generated
	 */
	boolean isCompleted();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.ITaskDependency#isCompleted <em>Completed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Completed</em>' attribute.
	 * @see #isCompleted()
	 * @generated
	 */
	void setCompleted(boolean value);

} // ITaskDependency
