/**
 */
package org.eclipse.skills.model.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.skills.model.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.skills.model.ISkillsPackage
 * @generated
 */
public class SkillsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ISkillsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SkillsSwitch() {
		if (modelPackage == null) {
			modelPackage = ISkillsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ISkillsPackage.TASK: {
				ITask task = (ITask)theEObject;
				T result = caseTask(task);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.DESCRIPTION: {
				IDescription description = (IDescription)theEObject;
				T result = caseDescription(description);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.REWARD: {
				IReward reward = (IReward)theEObject;
				T result = caseReward(reward);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.BADGE: {
				IBadge badge = (IBadge)theEObject;
				T result = caseBadge(badge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.USER: {
				IUser user = (IUser)theEObject;
				T result = caseUser(user);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.SKILL: {
				ISkill skill = (ISkill)theEObject;
				T result = caseSkill(skill);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.USER_TASK: {
				IUserTask userTask = (IUserTask)theEObject;
				T result = caseUserTask(userTask);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.QUEST: {
				IQuest quest = (IQuest)theEObject;
				T result = caseQuest(quest);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.DEPENDENCY: {
				IDependency dependency = (IDependency)theEObject;
				T result = caseDependency(dependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.BADGE_REWARD: {
				IBadgeReward badgeReward = (IBadgeReward)theEObject;
				T result = caseBadgeReward(badgeReward);
				if (result == null) result = caseReward(badgeReward);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.EXPERIENCE_REWARD: {
				IExperienceReward experienceReward = (IExperienceReward)theEObject;
				T result = caseExperienceReward(experienceReward);
				if (result == null) result = caseReward(experienceReward);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.SKILL_REWARD: {
				ISkillReward skillReward = (ISkillReward)theEObject;
				T result = caseSkillReward(skillReward);
				if (result == null) result = caseExperienceReward(skillReward);
				if (result == null) result = caseReward(skillReward);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.CONTAINED_DEPENDENCY: {
				IContainedDependency containedDependency = (IContainedDependency)theEObject;
				T result = caseContainedDependency(containedDependency);
				if (result == null) result = caseDependency(containedDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.AND_DEPENDENCY: {
				IAndDependency andDependency = (IAndDependency)theEObject;
				T result = caseAndDependency(andDependency);
				if (result == null) result = caseContainedDependency(andDependency);
				if (result == null) result = caseDependency(andDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.OR_DEPENDENCY: {
				IOrDependency orDependency = (IOrDependency)theEObject;
				T result = caseOrDependency(orDependency);
				if (result == null) result = caseContainedDependency(orDependency);
				if (result == null) result = caseDependency(orDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.NOT_DEPENDENCY: {
				INotDependency notDependency = (INotDependency)theEObject;
				T result = caseNotDependency(notDependency);
				if (result == null) result = caseContainedDependency(notDependency);
				if (result == null) result = caseDependency(notDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.DELAYED_DEPENDENCY: {
				IDelayedDependency delayedDependency = (IDelayedDependency)theEObject;
				T result = caseDelayedDependency(delayedDependency);
				if (result == null) result = caseContainedDependency(delayedDependency);
				if (result == null) result = caseDependency(delayedDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.CUSTOM_DEPENDENCY: {
				ICustomDependency customDependency = (ICustomDependency)theEObject;
				T result = caseCustomDependency(customDependency);
				if (result == null) result = caseDependency(customDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.DEPENDENCY_WITH_ATTRIBUTES: {
				IDependencyWithAttributes dependencyWithAttributes = (IDependencyWithAttributes)theEObject;
				T result = caseDependencyWithAttributes(dependencyWithAttributes);
				if (result == null) result = caseDependency(dependencyWithAttributes);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.SEQUENCE_DEPENDENCY: {
				ISequenceDependency sequenceDependency = (ISequenceDependency)theEObject;
				T result = caseSequenceDependency(sequenceDependency);
				if (result == null) result = caseAndDependency(sequenceDependency);
				if (result == null) result = caseContainedDependency(sequenceDependency);
				if (result == null) result = caseDependency(sequenceDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.COMPLETE_INCLUDED_TASKS_DEPENDENCY: {
				ICompleteIncludedTasksDependency completeIncludedTasksDependency = (ICompleteIncludedTasksDependency)theEObject;
				T result = caseCompleteIncludedTasksDependency(completeIncludedTasksDependency);
				if (result == null) result = caseUserDependency(completeIncludedTasksDependency);
				if (result == null) result = caseDependency(completeIncludedTasksDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.TASK_DEPENDENCY: {
				ITaskDependency taskDependency = (ITaskDependency)theEObject;
				T result = caseTaskDependency(taskDependency);
				if (result == null) result = caseUserDependency(taskDependency);
				if (result == null) result = caseDependency(taskDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.SKILL_DEPENDENCY: {
				ISkillDependency skillDependency = (ISkillDependency)theEObject;
				T result = caseSkillDependency(skillDependency);
				if (result == null) result = caseUserDependency(skillDependency);
				if (result == null) result = caseDependency(skillDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.HINT: {
				IHint hint = (IHint)theEObject;
				T result = caseHint(hint);
				if (result == null) result = caseDescription(hint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.USER_DEPENDENCY: {
				IUserDependency userDependency = (IUserDependency)theEObject;
				T result = caseUserDependency(userDependency);
				if (result == null) result = caseDependency(userDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.LEVEL_PROGRESSION: {
				ILevelProgression levelProgression = (ILevelProgression)theEObject;
				T result = caseLevelProgression(levelProgression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.FACTOR_PROGRESSION: {
				IFactorProgression factorProgression = (IFactorProgression)theEObject;
				T result = caseFactorProgression(factorProgression);
				if (result == null) result = caseLevelProgression(factorProgression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ISkillsPackage.USER_TITLE: {
				IUserTitle userTitle = (IUserTitle)theEObject;
				T result = caseUserTitle(userTitle);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTask(ITask object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Description</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Description</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDescription(IDescription object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reward</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reward</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReward(IReward object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Badge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Badge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBadge(IBadge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>User</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUser(IUser object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Skill</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Skill</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSkill(ISkill object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>User Task</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User Task</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUserTask(IUserTask object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Quest</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Quest</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQuest(IQuest object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDependency(IDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Badge Reward</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Badge Reward</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBadgeReward(IBadgeReward object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Experience Reward</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Experience Reward</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExperienceReward(IExperienceReward object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Skill Reward</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Skill Reward</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSkillReward(ISkillReward object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contained Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contained Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContainedDependency(IContainedDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>And Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>And Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAndDependency(IAndDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Or Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Or Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrDependency(IOrDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Not Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Not Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNotDependency(INotDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Delayed Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Delayed Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDelayedDependency(IDelayedDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Custom Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Custom Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCustomDependency(ICustomDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dependency With Attributes</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dependency With Attributes</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDependencyWithAttributes(IDependencyWithAttributes object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sequence Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sequence Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSequenceDependency(ISequenceDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Complete Included Tasks Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Complete Included Tasks Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompleteIncludedTasksDependency(ICompleteIncludedTasksDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaskDependency(ITaskDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Skill Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Skill Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSkillDependency(ISkillDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHint(IHint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>User Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUserDependency(IUserDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Level Progression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Level Progression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLevelProgression(ILevelProgression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Factor Progression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Factor Progression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFactorProgression(IFactorProgression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>User Title</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User Title</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUserTitle(IUserTitle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //SkillsSwitch
