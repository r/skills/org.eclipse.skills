/**
 */
package org.eclipse.skills.model.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.skills.model.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.skills.model.ISkillsPackage
 * @generated
 */
public class SkillsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ISkillsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SkillsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ISkillsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SkillsSwitch<Adapter> modelSwitch =
		new SkillsSwitch<Adapter>() {
			@Override
			public Adapter caseTask(ITask object) {
				return createTaskAdapter();
			}
			@Override
			public Adapter caseDescription(IDescription object) {
				return createDescriptionAdapter();
			}
			@Override
			public Adapter caseReward(IReward object) {
				return createRewardAdapter();
			}
			@Override
			public Adapter caseBadge(IBadge object) {
				return createBadgeAdapter();
			}
			@Override
			public Adapter caseUser(IUser object) {
				return createUserAdapter();
			}
			@Override
			public Adapter caseSkill(ISkill object) {
				return createSkillAdapter();
			}
			@Override
			public Adapter caseUserTask(IUserTask object) {
				return createUserTaskAdapter();
			}
			@Override
			public Adapter caseQuest(IQuest object) {
				return createQuestAdapter();
			}
			@Override
			public Adapter caseDependency(IDependency object) {
				return createDependencyAdapter();
			}
			@Override
			public Adapter caseBadgeReward(IBadgeReward object) {
				return createBadgeRewardAdapter();
			}
			@Override
			public Adapter caseExperienceReward(IExperienceReward object) {
				return createExperienceRewardAdapter();
			}
			@Override
			public Adapter caseSkillReward(ISkillReward object) {
				return createSkillRewardAdapter();
			}
			@Override
			public Adapter caseContainedDependency(IContainedDependency object) {
				return createContainedDependencyAdapter();
			}
			@Override
			public Adapter caseAndDependency(IAndDependency object) {
				return createAndDependencyAdapter();
			}
			@Override
			public Adapter caseOrDependency(IOrDependency object) {
				return createOrDependencyAdapter();
			}
			@Override
			public Adapter caseNotDependency(INotDependency object) {
				return createNotDependencyAdapter();
			}
			@Override
			public Adapter caseDelayedDependency(IDelayedDependency object) {
				return createDelayedDependencyAdapter();
			}
			@Override
			public Adapter caseCustomDependency(ICustomDependency object) {
				return createCustomDependencyAdapter();
			}
			@Override
			public Adapter caseDependencyWithAttributes(IDependencyWithAttributes object) {
				return createDependencyWithAttributesAdapter();
			}
			@Override
			public Adapter caseSequenceDependency(ISequenceDependency object) {
				return createSequenceDependencyAdapter();
			}
			@Override
			public Adapter caseCompleteIncludedTasksDependency(ICompleteIncludedTasksDependency object) {
				return createCompleteIncludedTasksDependencyAdapter();
			}
			@Override
			public Adapter caseTaskDependency(ITaskDependency object) {
				return createTaskDependencyAdapter();
			}
			@Override
			public Adapter caseSkillDependency(ISkillDependency object) {
				return createSkillDependencyAdapter();
			}
			@Override
			public Adapter caseHint(IHint object) {
				return createHintAdapter();
			}
			@Override
			public Adapter caseUserDependency(IUserDependency object) {
				return createUserDependencyAdapter();
			}
			@Override
			public Adapter caseLevelProgression(ILevelProgression object) {
				return createLevelProgressionAdapter();
			}
			@Override
			public Adapter caseFactorProgression(IFactorProgression object) {
				return createFactorProgressionAdapter();
			}
			@Override
			public Adapter caseUserTitle(IUserTitle object) {
				return createUserTitleAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.ITask <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.ITask
	 * @generated
	 */
	public Adapter createTaskAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IDescription
	 * @generated
	 */
	public Adapter createDescriptionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IReward <em>Reward</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IReward
	 * @generated
	 */
	public Adapter createRewardAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IBadge <em>Badge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IBadge
	 * @generated
	 */
	public Adapter createBadgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IUser <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IUser
	 * @generated
	 */
	public Adapter createUserAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.ISkill <em>Skill</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.ISkill
	 * @generated
	 */
	public Adapter createSkillAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IUserTask <em>User Task</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IUserTask
	 * @generated
	 */
	public Adapter createUserTaskAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IQuest <em>Quest</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IQuest
	 * @generated
	 */
	public Adapter createQuestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IDependency <em>Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IDependency
	 * @generated
	 */
	public Adapter createDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IBadgeReward <em>Badge Reward</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IBadgeReward
	 * @generated
	 */
	public Adapter createBadgeRewardAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IExperienceReward <em>Experience Reward</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IExperienceReward
	 * @generated
	 */
	public Adapter createExperienceRewardAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.ISkillReward <em>Skill Reward</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.ISkillReward
	 * @generated
	 */
	public Adapter createSkillRewardAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IContainedDependency <em>Contained Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IContainedDependency
	 * @generated
	 */
	public Adapter createContainedDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IAndDependency <em>And Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IAndDependency
	 * @generated
	 */
	public Adapter createAndDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IOrDependency <em>Or Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IOrDependency
	 * @generated
	 */
	public Adapter createOrDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.INotDependency <em>Not Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.INotDependency
	 * @generated
	 */
	public Adapter createNotDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IDelayedDependency <em>Delayed Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IDelayedDependency
	 * @generated
	 */
	public Adapter createDelayedDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.ICustomDependency <em>Custom Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.ICustomDependency
	 * @generated
	 */
	public Adapter createCustomDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IDependencyWithAttributes <em>Dependency With Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IDependencyWithAttributes
	 * @generated
	 */
	public Adapter createDependencyWithAttributesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.ISequenceDependency <em>Sequence Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.ISequenceDependency
	 * @generated
	 */
	public Adapter createSequenceDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.ICompleteIncludedTasksDependency <em>Complete Included Tasks Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.ICompleteIncludedTasksDependency
	 * @generated
	 */
	public Adapter createCompleteIncludedTasksDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.ITaskDependency <em>Task Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.ITaskDependency
	 * @generated
	 */
	public Adapter createTaskDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.ISkillDependency <em>Skill Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.ISkillDependency
	 * @generated
	 */
	public Adapter createSkillDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IHint <em>Hint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IHint
	 * @generated
	 */
	public Adapter createHintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IUserDependency <em>User Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IUserDependency
	 * @generated
	 */
	public Adapter createUserDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.ILevelProgression <em>Level Progression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.ILevelProgression
	 * @generated
	 */
	public Adapter createLevelProgressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IFactorProgression <em>Factor Progression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IFactorProgression
	 * @generated
	 */
	public Adapter createFactorProgressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.skills.model.IUserTitle <em>User Title</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.skills.model.IUserTitle
	 * @generated
	 */
	public Adapter createUserTitleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //SkillsAdapterFactory
