/**
 */
package org.eclipse.skills.model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.resource.ImageDescriptor;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.IUser#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.skills.model.IUser#getSkills <em>Skills</em>}</li>
 *   <li>{@link org.eclipse.skills.model.IUser#getUsertasks <em>Usertasks</em>}</li>
 *   <li>{@link org.eclipse.skills.model.IUser#getImageLocation <em>Image Location</em>}</li>
 *   <li>{@link org.eclipse.skills.model.IUser#getExperience <em>Experience</em>}</li>
 *   <li>{@link org.eclipse.skills.model.IUser#getBadges <em>Badges</em>}</li>
 *   <li>{@link org.eclipse.skills.model.IUser#getTitle <em>Title</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getUser()
 * @model
 * @generated
 */
public interface IUser extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.skills.model.ISkillsPackage#getUser_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IUser#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Skills</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.skills.model.ISkill}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Skills</em>' containment reference list.
	 * @see org.eclipse.skills.model.ISkillsPackage#getUser_Skills()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISkill> getSkills();

	/**
	 * Returns the value of the '<em><b>Usertasks</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.skills.model.IUserTask}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Usertasks</em>' containment reference list.
	 * @see org.eclipse.skills.model.ISkillsPackage#getUser_Usertasks()
	 * @model containment="true"
	 * @generated
	 */
	EList<IUserTask> getUsertasks();

	/**
	 * Returns the value of the '<em><b>Image Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image Location</em>' attribute.
	 * @see #setImageLocation(String)
	 * @see org.eclipse.skills.model.ISkillsPackage#getUser_ImageLocation()
	 * @model
	 * @generated
	 */
	String getImageLocation();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IUser#getImageLocation <em>Image Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image Location</em>' attribute.
	 * @see #getImageLocation()
	 * @generated
	 */
	void setImageLocation(String value);

	/**
	 * Returns the value of the '<em><b>Experience</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Experience</em>' containment reference.
	 * @see #setExperience(ISkill)
	 * @see org.eclipse.skills.model.ISkillsPackage#getUser_Experience()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISkill getExperience();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IUser#getExperience <em>Experience</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Experience</em>' containment reference.
	 * @see #getExperience()
	 * @generated
	 */
	void setExperience(ISkill value);

	/**
	 * Returns the value of the '<em><b>Badges</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.skills.model.IBadge}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Badges</em>' containment reference list.
	 * @see org.eclipse.skills.model.ISkillsPackage#getUser_Badges()
	 * @model containment="true"
	 * @generated
	 */
	EList<IBadge> getBadges();

	/**
	 * Returns the value of the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Title</em>' attribute.
	 * @see #setTitle(String)
	 * @see org.eclipse.skills.model.ISkillsPackage#getUser_Title()
	 * @model required="true"
	 * @generated
	 */
	String getTitle();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IUser#getTitle <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title</em>' attribute.
	 * @see #getTitle()
	 * @generated
	 */
	void setTitle(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	IUserTask addTask(ITask task);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	ISkill getSkill(ISkill skill);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void consume(IReward reward);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="org.eclipse.skills.model.ImageDescriptor"
	 * @generated
	 */
	ImageDescriptor getAvatar();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	ISkill getSkill(String name);

} // IUser
