/**
 */
package org.eclipse.skills.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Or Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getOrDependency()
 * @model
 * @generated
 */
public interface IOrDependency extends IContainedDependency {
} // IOrDependency
