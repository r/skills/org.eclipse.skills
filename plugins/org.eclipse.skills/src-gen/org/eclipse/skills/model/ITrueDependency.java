/**
 */
package org.eclipse.skills.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>True Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getTrueDependency()
 * @model
 * @generated
 */
public interface ITrueDependency extends IDependency {
} // ITrueDependency
