/**
 */
package org.eclipse.skills.model;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Quest</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.skills.model.IQuest#getTasks <em>Tasks</em>}</li>
 *   <li>{@link org.eclipse.skills.model.IQuest#getSkills <em>Skills</em>}</li>
 *   <li>{@link org.eclipse.skills.model.IQuest#getTitle <em>Title</em>}</li>
 *   <li>{@link org.eclipse.skills.model.IQuest#getUserTitles <em>User Titles</em>}</li>
 * </ul>
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getQuest()
 * @model
 * @generated
 */
public interface IQuest extends EObject {
	/**
	 * Returns the value of the '<em><b>Tasks</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.skills.model.ITask}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tasks</em>' containment reference list.
	 * @see org.eclipse.skills.model.ISkillsPackage#getQuest_Tasks()
	 * @model containment="true"
	 * @generated
	 */
	EList<ITask> getTasks();

	/**
	 * Returns the value of the '<em><b>Skills</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.skills.model.ISkill}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Skills</em>' containment reference list.
	 * @see org.eclipse.skills.model.ISkillsPackage#getQuest_Skills()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISkill> getSkills();

	/**
	 * Returns the value of the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Title</em>' attribute.
	 * @see #setTitle(String)
	 * @see org.eclipse.skills.model.ISkillsPackage#getQuest_Title()
	 * @model
	 * @generated
	 */
	String getTitle();

	/**
	 * Sets the value of the '{@link org.eclipse.skills.model.IQuest#getTitle <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title</em>' attribute.
	 * @see #getTitle()
	 * @generated
	 */
	void setTitle(String value);

	/**
	 * Returns the value of the '<em><b>User Titles</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.skills.model.IUserTitle}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Titles</em>' containment reference list.
	 * @see org.eclipse.skills.model.ISkillsPackage#getQuest_UserTitles()
	 * @model containment="true"
	 * @generated
	 */
	EList<IUserTitle> getUserTitles();

} // IQuest
