/**
 */
package org.eclipse.skills.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Level Progression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.skills.model.ISkillsPackage#getLevelProgression()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ILevelProgression extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 * @generated
	 */
	int getMinimumXpForLevel(int level);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 * @generated
	 */
	int getLevel(int xp);

} // ILevelProgression
