<?xml version='1.0' encoding='UTF-8'?>
<!-- Schema file written by PDE -->
<schema targetNamespace="org.eclipse.skills" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appInfo>
         <meta.schema plugin="org.eclipse.skills" id="quest" name="Quest"/>
      </appInfo>
      <documentation>
         Define a quest containing skills, tasks, and badges.
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <appInfo>
            <meta.element />
         </appInfo>
      </annotation>
      <complexType>
         <choice minOccurs="0" maxOccurs="unbounded">
            <element ref="resource"/>
         </choice>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
               <appInfo>
                  <meta.attribute translatable="true"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="quest">
      <annotation>
         <documentation>
            A quest stored as EMF resource file.
         </documentation>
      </annotation>
      <complexType>
         <attribute name="resource" type="string" use="required">
            <annotation>
               <documentation>
                  A resource containing the quest description.
               </documentation>
               <appInfo>
                  <meta.attribute kind="resource"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="task">
      <annotation>
         <documentation>
            Definition of a task. Once completed, the user will be rewarded with EXP, skills and badges. May contain other tasks.
         </documentation>
      </annotation>
      <complexType>
         <choice minOccurs="0" maxOccurs="1">
            <element ref="andRequirement"/>
            <element ref="orRequirement"/>
            <element ref="notRequirement"/>
            <element ref="requirement"/>
         </choice>
         <attribute name="ID" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="description" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="image" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="badge">
      <complexType>
         <attribute name="ID" type="string">
            <annotation>
               <documentation>
                  Unique ID of this badge. To be referred by tasks
               </documentation>
            </annotation>
         </attribute>
         <attribute name="title" type="string" use="required">
            <annotation>
               <documentation>
                  Name of this badge
               </documentation>
            </annotation>
         </attribute>
         <attribute name="image" type="string" use="required">
            <annotation>
               <documentation>
                  Image location to be displayed. The image will be scaled to 64x64 pixels.
               </documentation>
               <appInfo>
                  <meta.attribute kind="resource"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="requirement">
      <annotation>
         <documentation>
            Custom requirement implementation.
         </documentation>
      </annotation>
      <complexType>
         <attribute name="class" type="string" use="required">
            <annotation>
               <documentation>
                  Requirement implementation.
               </documentation>
               <appInfo>
                  <meta.attribute kind="java" basedOn=":org.eclipse.skills.model.IRequirement"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="andRequirement">
      <annotation>
         <documentation>
            Requires all contained requirements to be fulfilled.
         </documentation>
      </annotation>
      <complexType>
         <choice minOccurs="1" maxOccurs="unbounded">
            <element ref="andRequirement"/>
            <element ref="orRequirement"/>
            <element ref="notRequirement"/>
            <element ref="requirement"/>
         </choice>
      </complexType>
   </element>

   <element name="orRequirement">
      <complexType>
         <choice minOccurs="1" maxOccurs="unbounded">
            <element ref="andRequirement"/>
            <element ref="orRequirement"/>
            <element ref="notRequirement"/>
            <element ref="requirement"/>
         </choice>
      </complexType>
   </element>

   <element name="notRequirement">
      <annotation>
         <documentation>
            Requires the contained requirements to not be fulfilled.
         </documentation>
      </annotation>
      <complexType>
         <choice>
            <element ref="andRequirement"/>
            <element ref="orRequirement"/>
            <element ref="notRequirement"/>
            <element ref="requirement"/>
         </choice>
      </complexType>
   </element>

   <element name="resource">
      <annotation>
         <documentation>
            Quest stored in a file.
         </documentation>
      </annotation>
      <complexType>
         <attribute name="URI" type="string" use="required">
            <annotation>
               <documentation>
                  URI to the quest definition.
               </documentation>
               <appInfo>
                  <meta.attribute kind="resource"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appInfo>
         <meta.section type="since"/>
      </appInfo>
      <documentation>
         [Enter the first release in which this extension point appears.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="examples"/>
      </appInfo>
      <documentation>
         [Enter extension point usage example here.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="apiinfo"/>
      </appInfo>
      <documentation>
         [Enter API information here.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="implementation"/>
      </appInfo>
      <documentation>
         [Enter information about supplied implementation of this extension point.]
      </documentation>
   </annotation>


</schema>
