/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.skills.model.IQuest;
import org.eclipse.skills.model.ISkill;
import org.eclipse.skills.model.IUser;
import org.eclipse.skills.model.IUserTitle;
import org.eclipse.skills.service.questprovider.IQuestProvider;

public class UserTitleGenerator {

	private static IUser getServiceUser() {
		return SkillService.getInstance().getUser();
	}

	private static IQuestProvider getServiceQuestProvider() {
		return SkillService.getInstance().getQuestProvider();
	}

	private final IUser fUser;
	private final IQuestProvider fQuestProvider;

	public UserTitleGenerator(IUser user, IQuestProvider questProvider) {
		if (user == null)
			throw new IllegalArgumentException("user cannot be null");

		if (questProvider == null)
			throw new IllegalArgumentException("questProvider cannot be null");

		fUser = user;
		fQuestProvider = questProvider;
	}

	public UserTitleGenerator() {
		this(getServiceUser(), getServiceQuestProvider());
	}

	public String createUserTitle() {

		final IUserTitle userTitle = getBestMatchingUserTitle();

		if (userTitle != null)
			return StringReplacer.replace(userTitle.getDisplayString(), getReplacementList(userTitle.getSkill()));

		return "";
	}

	private Map<String, String> getReplacementList(ISkill skill) {
		final Map<String, String> replacementList = new HashMap<>();

		replacementList.put("skill.name", (skill != null) ? skill.getName() : "");

		return replacementList;
	}

	private IUserTitle getBestMatchingUserTitle() {
		final List<IUserTitle> userTitles = getUserTitles();

		Collections.sort(userTitles, (o1, o2) -> o2.getMinLevel() - o1.getMinLevel());

		for (final IUserTitle userTitle : userTitles) {
			final ISkill userSkill = getUserSkill(userTitle.getSkill());
			if (userSkill != null) {
				if (userTitle.getMinLevel() <= userSkill.getProgression().getLevel(userSkill.getExperience()))
					return userTitle;
			}
		}

		return null;
	}

	private ISkill getUserSkill(ISkill skill) {
		if (skill != null)
			return fUser.getSkill(skill.getName());

		return fUser.getExperience();
	}

	private List<IUserTitle> getUserTitles() {
		final List<IUserTitle> userTitles = new ArrayList<>();
		for (final IQuest quest : fQuestProvider.getQuests())
			userTitles.addAll(quest.getUserTitles());

		return userTitles;
	}
}
