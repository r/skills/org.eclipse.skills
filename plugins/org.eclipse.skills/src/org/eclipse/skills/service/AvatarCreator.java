/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.skills.Activator;
import org.osgi.framework.Bundle;

public class AvatarCreator {

	private static final List<String> AVATAR_LOCATIONS = Arrays.asList("doll_nietzsche.png", "gentleman.png", "hooded_monster.png", "merman.png", "octopus.png",
			"orc.png", "pacman.png", "rat.png", "robot.png", "roman.png", "rtfm.png", "skeleton.png", "smiley_spy.png", "stone_block.png", "superhero_girl.png",
			"usb_stick.png", "woman.png");

	public static List<Path> getImageLocations() {
		return AVATAR_LOCATIONS.stream().map(l -> new Path("/resources/defaultAvatars/" + l)).collect(Collectors.toList());
	}

	public static byte[] getRandomAvatarData() throws IOException {
		final int index = new Random().nextInt(AVATAR_LOCATIONS.size());

		final String location = AVATAR_LOCATIONS.get(index);

		final Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
		final URL imagePath = FileLocator.find(bundle, new Path("/resources/defaultAvatars/" + location), null);
		return readStream(imagePath.openStream());
	}

	private static byte[] readStream(final InputStream stream) throws IOException {
		if (!(stream instanceof BufferedInputStream))
			return readStream(new BufferedInputStream(stream));

		final byte[] buffer = new byte[1024];

		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		int read;
		do {
			read = stream.read(buffer);
			if (read > 0)
				out.write(buffer, 0, read);

		} while (read != -1);

		return out.toByteArray();
	}
}
