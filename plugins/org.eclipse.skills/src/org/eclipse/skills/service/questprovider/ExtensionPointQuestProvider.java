/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service.questprovider;

import java.util.Collection;
import java.util.HashSet;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.skills.model.IQuest;

public class ExtensionPointQuestProvider implements IQuestProvider {

	@Override
	public Collection<IQuest> getQuests() {
		final Collection<IQuest> quests = new HashSet<>();

		final IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor("org.eclipse.skills.quest");
		for (final IConfigurationElement e : config) {
			if (e.getName().equals("resource")) {
				final String location = e.getAttribute("URI");

				URI uri = URI.createURI(location);
				if (uri.isRelative())
					uri = URI.createURI("platform:/plugin/" + e.getContributor().getName() + "/" + location);

				final URIQuestProvider questProvider = new URIQuestProvider(uri);
				quests.addAll(questProvider.getQuests());
			}
		}

		return quests;
	}
}
