/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service.questprovider;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.skills.model.IQuest;
import org.eclipse.skills.model.ISkillsPackage;

public class URIQuestProvider implements IQuestProvider {

	private final URI fQuestDataUri;

	public URIQuestProvider(URI questDataUri) {
		fQuestDataUri = questDataUri;
	}

	public URIQuestProvider(String questDataUri) {
		this(URI.createURI(questDataUri));
	}

	@Override
	public Collection<IQuest> getQuests() {
		final Collection<IQuest> quests = new HashSet<>();

		// initialize the model
		ISkillsPackage.eINSTANCE.eClass();

		final Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		final Map<String, Object> extensionsMap = reg.getExtensionToFactoryMap();
		extensionsMap.put("skills", new XMIResourceFactoryImpl());

		final ResourceSet resourceSet = new ResourceSetImpl();
		final Resource resource = resourceSet.getResource(fQuestDataUri, true);

		final IQuest quest = (IQuest) resource.getContents().get(0);
		quests.add(quest);

		return quests;
	}
}
