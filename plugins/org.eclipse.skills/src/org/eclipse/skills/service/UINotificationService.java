/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.jface.util.Throttler;
import org.eclipse.skills.BrokerTools;
import org.eclipse.skills.model.IUserTask;
import org.eclipse.skills.ui.notifications.MultiTaskNotification;
import org.eclipse.swt.widgets.Display;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

public class UINotificationService implements EventHandler {

	private static IUserTask getUserTask(Event event) {
		final Object data = event.getProperty(IEventBroker.DATA);
		if (data instanceof IUserTask)
			return (IUserTask) data;

		return null;
	}

	private final List<Event> fScheduledEvents = new ArrayList<>();

	private Throttler fDisplayThrottler = null;

	public UINotificationService() {
		BrokerTools.subscribe(ISkillService.EVENT_TASK_COMPLETED, this);
	}

	public void dispose() {
		BrokerTools.unsubscribe(this);
	}

	/**
	 * {@link EventHandler} for broker service events.
	 */
	@Override
	public void handleEvent(Event event) {
		synchronized (fScheduledEvents) {
			fScheduledEvents.add(event);
		}

		getDisplayThrottler().throttledExec();
	}

	private Throttler getDisplayThrottler() {
		if (fDisplayThrottler == null)
			fDisplayThrottler = new Throttler(Display.getDefault(), Duration.ofSeconds(1), this::displayNotification);

		return fDisplayThrottler;
	}

	public void displayNotification() {
		final List<Event> events = copyAndClearScheduledEvents();

		final List<IUserTask> finishedTasks = events.stream().map(e -> getUserTask(e)).collect(Collectors.toList());

		MultiTaskNotification.display(finishedTasks);
	}

	private List<Event> copyAndClearScheduledEvents() {
		List<Event> events;

		synchronized (fScheduledEvents) {
			events = new ArrayList<>(fScheduledEvents);
			fScheduledEvents.clear();
		}

		return events;
	}
}