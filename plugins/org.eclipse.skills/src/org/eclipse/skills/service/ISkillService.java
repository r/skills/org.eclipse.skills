/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service;

import java.util.Collection;

import org.eclipse.core.runtime.Platform;
import org.eclipse.skills.Activator;
import org.eclipse.skills.model.ITask;
import org.eclipse.skills.model.IUser;
import org.eclipse.skills.model.IUserTask;
import org.eclipse.skills.service.questprovider.IQuestProvider;
import org.eclipse.skills.service.storage.IDataStorage;

/**
 * Global service to manage user statistics, available quests and skills. To get the service instance use
 *
 * <pre>
 * final ISkillService skillService = PlatformUI.getWorkbench().getService(ISkillService.class);
 * </pre>
 */
public interface ISkillService extends IDataStorage {

	/** Trace enablement for the skills service. */
	boolean TRACE_SKILLS_SERVICE = Activator.getDefault().isDebugging()
			&& "true".equalsIgnoreCase(Platform.getDebugOption("org.eclipse.skills/debug/skillService"));

	String EVENT_BASE = "org/eclipse/skills";

	String EVENT_TASK_READY = EVENT_BASE + "/Task/ready";
	String EVENT_TASK_STARTED = EVENT_BASE + "/Task/started";
	String EVENT_TASK_COMPLETED = EVENT_BASE + "/Task/completed";

	String EVENT_USER_UPDATE = EVENT_BASE + "/User/update";

	String EVENT_DEPENDENCY_FULFILLED = EVENT_BASE + "/Dependency/fulfilled";

	String RESOURCE_AVATAR = "avatar.pic";

	void activateService();

	void deactivateService();

	IUser getUser();

	/**
	 * Reset all user progress.
	 */
	void resetProgress();

	/**
	 * Callback to report when tasks are ready to be started. Such a task has all its requirements met.
	 *
	 * @param task
	 *            task being ready
	 * @return usertask instance once task got assigned to the current user
	 */
	IUserTask notifyTaskReady(ITask task);

	void notifyTaskCompleted(IUserTask userTask);

	Collection<ITask> getOpenTasks();

	void setQuestProvider(IQuestProvider questProvider);

	IQuestProvider getQuestProvider();

	Collection<ITask> getAllAvailableTasks();

	void startTask(IUserTask userTask);

	void setStorage(IDataStorage storage);

}
