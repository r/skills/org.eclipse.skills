/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service;

import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class StringReplacer {

	@Deprecated
	private StringReplacer() {
		throw new IllegalArgumentException("Contructor not meant to be called");
	}

	private static final Pattern WILDCARD_PATTERN = Pattern.compile("\\$\\{(.*?)\\}");

	public static String replace(String data, Map<String, String> replacements) {
		if (data != null) {
			final Matcher matcher = WILDCARD_PATTERN.matcher(data);
			return matcher.replaceAll(matchResult -> getReplacement(matchResult.group(1), replacements)).trim();
		}

		return data;
	}

	private static String getReplacement(String key, Map<String, String> replacements) {
		final String candidate = (replacements != null) ? replacements.get(key) : "";
		return Objects.requireNonNullElseGet(candidate, () -> "");
	}
}
