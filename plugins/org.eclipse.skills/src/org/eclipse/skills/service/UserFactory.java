/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.skills.model.IDescription;
import org.eclipse.skills.model.IQuest;
import org.eclipse.skills.model.ISkill;
import org.eclipse.skills.model.ISkillsFactory;
import org.eclipse.skills.model.ISkillsPackage;
import org.eclipse.skills.model.IUser;

public class UserFactory {

	private static List<ISkill> getGeneratedDefaultSkills() {
		final List<ISkill> skills = new ArrayList<>();
		skills.add(createSkill("Dextery"));
		skills.add(createSkill("Strength"));
		skills.add(createSkill("Wisdom"));

		return skills;
	}

	private static ISkill createSkill(String name) {
		final IDescription description = ISkillsFactory.eINSTANCE.createDescription();
		description.setText("<description missing>");

		final ISkill skill = ISkillsFactory.eINSTANCE.createSkill();
		skill.setName(name);
		skill.setDescription(description);

		return skill;
	}

	private static String capitalizeFirstLetter(String word) {
		if ((word != null) && (!word.isEmpty()))
			return word.substring(0, 1).toUpperCase() + word.substring(1);

		return word;
	}

	public IUser createUser() {
		final IUser user = ISkillsFactory.eINSTANCE.createUser();

		user.setName(capitalizeFirstLetter(System.getProperty("user.name")));

		final ISkill experience = createSkill("Experience");
		experience.setImageURI("platform:/plugin/org.eclipse.skills/icons/full/obj32/skills/experience.png");
		user.setExperience(experience);

		user.getExperience().getDescription().setText("Your overall progress");

		for (final ISkill skill : getDefaultSkills())
			user.getSkills().add(EcoreUtil.copy(skill));

		user.setTitle("");

		return user;
	}

	private List<ISkill> getDefaultSkills() {
		// initialize the model
		ISkillsPackage.eINSTANCE.eClass();

		final Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		final Map<String, Object> extensionsMap = reg.getExtensionToFactoryMap();
		extensionsMap.put("skills", new XMIResourceFactoryImpl());

		final ResourceSet resourceSet = new ResourceSetImpl();
		final Resource resource = resourceSet.getResource(URI.createURI("platform:/plugin/org.eclipse.skills/resources/default.skills"), true);

		final EObject root = resource.getContents().get(0);
		if (root instanceof IQuest)
			return ((IQuest) root).getSkills();

		return getGeneratedDefaultSkills();
	}
}
