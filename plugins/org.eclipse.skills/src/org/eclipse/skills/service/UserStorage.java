/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.skills.Activator;
import org.eclipse.skills.Logger;
import org.eclipse.skills.model.ISkillsPackage;
import org.eclipse.skills.model.IUser;
import org.eclipse.skills.service.storage.DataStorageProxy;
import org.eclipse.skills.service.storage.IDataStorage;

public class UserStorage extends DataStorageProxy {

	public static final String BACKUP_SUFFIX = ".backup.";

	private static IUser createDefaultUser() {
		return new UserFactory().createUser();
	}

	private IUser fUser = null;

	public UserStorage(IDataStorage storage) {
		super(storage);
	}

	public IUser getUser() {
		if (fUser == null) {
			if (hasResource(USER_PROFILE)) {
				try {
					fUser = loadUser();
				} catch (final IOException e) {
					Logger.error(Activator.PLUGIN_ID, "The user profile is damaged. A new profile will be created and a backup of the old profile is stored.",
							e);
					backupUserProfile();
				}
			}

			if (fUser == null)
				fUser = createDefaultUser();
		}

		return fUser;
	}

	private void backupUserProfile() {
		try {
			int counter = 1;
			while (hasResource(USER_PROFILE + BACKUP_SUFFIX + counter))
				counter++;

			storeResource(USER_PROFILE + BACKUP_SUFFIX + counter, loadResource(USER_PROFILE));

		} catch (final IOException e) {
			Logger.error(Activator.PLUGIN_ID, "The user storage is damaged. A backup could not be created.", e);
		}
	}

	private IUser loadUser() throws IOException {
		if (hasResource(USER_PROFILE)) {
			try (InputStream userData = new ByteArrayInputStream(loadResource(USER_PROFILE))) {

				// initialize the model
				ISkillsPackage.eINSTANCE.eClass();

				final Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
				final Map<String, Object> extensionsMap = reg.getExtensionToFactoryMap();
				extensionsMap.put("skills", new XMIResourceFactoryImpl());

				final ResourceSet resourceSet = new ResourceSetImpl();
				final Resource resource = resourceSet.createResource(URI.createURI("user.skills"));
				resource.load(userData, new HashMap<>());

				return (IUser) resource.getContents().get(0);
			}
		}

		throw new FileNotFoundException("User profile could not be found");
	}

	public void storeUser(IUser user) throws IOException {
		final Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		final Map<String, Object> extensionsMap = reg.getExtensionToFactoryMap();
		extensionsMap.put("skills", new XMIResourceFactoryImpl());

		final Resource resource = new XMIResourceImpl();
		resource.getContents().add(user);

		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			resource.save(out, extensionsMap);
			getStorage().storeResource(USER_PROFILE, out.toByteArray());
		}
	}

	public void resetProgress() {
		final IUser newUser = createDefaultUser();
		if (fUser != null) {
			newUser.setName(fUser.getName());
			newUser.setImageLocation(fUser.getImageLocation());
		}

		fUser = newUser;
	}
}
