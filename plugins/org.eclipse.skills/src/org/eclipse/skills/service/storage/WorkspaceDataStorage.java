/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service.storage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.eclipse.core.runtime.IPath;
import org.eclipse.skills.Activator;

public class WorkspaceDataStorage implements IDataStorage {

	private File getUserStorageLocation() {
		final IPath userStoragePath = Activator.getDefault().getStateLocation();
		return userStoragePath.toFile();
	}

	@Override
	public void storeResource(String name, byte[] data) throws IOException {
		final Path target = Paths.get(getUserStorageLocation().getAbsolutePath(), name);
		Files.write(target, data, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
	}

	@Override
	public byte[] loadResource(String name) throws IOException {
		final Path target = Paths.get(getUserStorageLocation().getAbsolutePath(), name);
		return Files.readAllBytes(target);
	}

	@Override
	public InputStream openResource(String name) throws IOException {
		final Path target = Paths.get(getUserStorageLocation().getAbsolutePath(), name);
		return new FileInputStream(target.toFile());
	}

	@Override
	public boolean hasResource(String name) {
		final Path target = Paths.get(getUserStorageLocation().getAbsolutePath(), name);
		return target.toFile().exists();
	}
}
