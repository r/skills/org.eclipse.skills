/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service.storage;

import java.io.IOException;
import java.io.InputStream;

public interface IDataStorage {

	public static final String USER_PROFILE = "user.skills";

	/**
	 * Stores a resource in the user storage location.
	 *
	 * @param name
	 *            name to be used for storage
	 * @param data
	 *            to be stored
	 * @throws IOException
	 *             when data cannot be stored
	 */
	public void storeResource(String name, byte[] data) throws IOException;

	/**
	 * Loads a resource from the user storage location.
	 *
	 * @param name
	 *            name of resource in storage
	 * @throws IOException
	 *             when data cannot be loaded
	 * @return resource binary data
	 */
	public byte[] loadResource(String name) throws IOException;

	/**
	 * Get an input stream to a resource.
	 *
	 * @param name
	 *            name of resource in storage
	 * @throws IOException
	 *             when data cannot be loaded
	 * @return resource input stream
	 */
	InputStream openResource(String name) throws IOException;

	/**
	 * Check if a resource exists in the storage.
	 *
	 * @param name
	 *            mane of resource to look up
	 * @return <code>true</code> when resource exists
	 */
	public boolean hasResource(String name);
}
