/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.service.storage;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class DataStorageProxy implements IDataStorage {

	private IDataStorage fBaseStorage;

	public DataStorageProxy(IDataStorage baseStorage) {
		setStorage(baseStorage);
	}

	@Override
	public void storeResource(String name, byte[] data) throws IOException {
		getStorage().storeResource(name, data);
	}

	@Override
	public byte[] loadResource(String name) throws IOException {
		if (hasResource(name))
			return getStorage().loadResource(name);

		throw new FileNotFoundException(String.format("Resource \"%s\" could not be found", name));
	}

	@Override
	public boolean hasResource(String name) {
		return getStorage().hasResource(name);
	}

	public IDataStorage getStorage() {
		return fBaseStorage;
	}

	public void setStorage(IDataStorage baseStorage) {
		if (baseStorage == null)
			throw new IllegalArgumentException("baseStorage cannot be null");

		fBaseStorage = baseStorage;
	}

	@Override
	public InputStream openResource(String name) throws IOException {
		if (hasResource(name))
			return getStorage().openResource(name);

		throw new FileNotFoundException(String.format("Resource \"%s\" could not be found", name));
	}
}
