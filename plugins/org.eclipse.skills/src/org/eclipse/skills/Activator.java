/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

public class Activator extends AbstractUIPlugin {

	public static final String PLUGIN_ID = "org.eclipse.skills";

	private static Activator fInstance;

	public static Activator getDefault() {
		return fInstance;
	}

	private BundleContext fContext;

	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);

		fContext = context;
		fInstance = this;
	}

	@Override
	public void stop(final BundleContext context) throws Exception {
		fInstance = null;
		fContext = null;

		super.stop(context);
	}

	public BundleContext getContext() {
		return fContext;
	}

	/**
	 * Get {@link ImageDescriptor} for a given bundle/path location.
	 *
	 * @param bundleID
	 * @param path
	 * @return
	 */
	public static ImageDescriptor getImageDescriptor(final String bundleID, final String path) {
		assert (bundleID != null) : "No bundle defined";
		assert (path != null) : "No path defined";

		// if the bundle is not ready then there is no image
		final Bundle bundle = Platform.getBundle(bundleID);
		final int bundleState = bundle.getState();
		if ((bundleState != Bundle.ACTIVE) && (bundleState != Bundle.STARTING) && (bundleState != Bundle.RESOLVED))
			return null;

		// look for the image (this will check both the plugin and fragment folders)
		final URL imagePath = FileLocator.find(bundle, new Path(path), null);

		if (imagePath != null)
			return ImageDescriptor.createFromURL(imagePath);

		return null;
	}

	/**
	 * Provide an input stream to a bundle resource.
	 *
	 * @param path
	 *            path within the current bundle
	 * @return input stream to resource data
	 * @throws IOException
	 *             when resource is not available/readable
	 */
	public InputStream loadResource(String path) throws IOException {
		final URL resourceLocation = FileLocator.find(getBundle(), new Path(path), null);

		return resourceLocation.openStream();
	}
}
