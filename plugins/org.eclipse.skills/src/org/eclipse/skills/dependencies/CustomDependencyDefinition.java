/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.dependencies;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.skills.Activator;
import org.eclipse.skills.Logger;
import org.eclipse.skills.model.IDependency;
import org.eclipse.skills.model.IDependencyWithAttributes;

public class CustomDependencyDefinition {

	private static Set<CustomDependencyDefinition> fDefintions = null;
	private final IConfigurationElement fConfiguration;

	public static Set<CustomDependencyDefinition> getDefinedDefinitions() {
		if (fDefintions == null) {
			fDefintions = new HashSet<>();

			final IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor("org.eclipse.skills.dependency");
			for (final IConfigurationElement e : config) {
				if (e.getName().equals("definition"))
					fDefintions.add(new CustomDependencyDefinition(e));
			}
		}

		return fDefintions;
	}

	public static CustomDependencyDefinition getDefinitionFor(String nameOrId) {
		for (final CustomDependencyDefinition definition : getDefinedDefinitions()) {
			if (Objects.equals(definition.getName(), nameOrId))
				return definition;

			if (Objects.equals(definition.getId(), nameOrId))
				return definition;
		}

		return null;
	}

	public CustomDependencyDefinition(IConfigurationElement configuration) {
		fConfiguration = configuration;
	}

	public String getId() {
		return fConfiguration.getAttribute("id");
	}

	public String getName() {
		return fConfiguration.getAttribute("name");
	}

	public String getDescription() {
		return fConfiguration.getAttribute("description");
	}

	public ImageDescriptor getImageDescriptor() {
		final String imageLocation = fConfiguration.getAttribute("image");
		if ((imageLocation != null) && (!(imageLocation.isEmpty()))) {
			try {
				return ImageDescriptor.createFromURL(new URL("platform:/plugin/" + fConfiguration.getContributor().getName() + "/" + imageLocation));
			} catch (final MalformedURLException e) {
				Logger.warning(Activator.PLUGIN_ID, "Could not load image for custom dependency: " + getId());
			}
		}

		return null;
	}

	public IDependency createInstance(String attributes) {
		try {
			final Object implementation = fConfiguration.createExecutableExtension("class");
			if (implementation instanceof IDependency) {
				if (implementation instanceof IDependencyWithAttributes)
					((IDependencyWithAttributes) implementation).setAttributes(attributes);

				return (IDependency) implementation;

			}

			throw new RuntimeException("Invalid Skills dependency definition: " + fConfiguration.getAttribute("id"));

		} catch (final CoreException e) {
			throw new RuntimeException("Invalid Skills dependency definition: " + fConfiguration.getAttribute("id"), e);
		}
	}
}
