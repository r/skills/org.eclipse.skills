/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.dependencies;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Pattern;

import org.eclipse.skills.Activator;
import org.eclipse.skills.Logger;
import org.eclipse.skills.model.IDependencyWithAttributes;
import org.eclipse.skills.model.impl.MDependency;

public abstract class AbstractCustomDependency extends MDependency implements IDependencyWithAttributes {

	private final Properties fProperties = new Properties();

	@Override
	public void setAttributes(String attributes) {
		try {
			fProperties.load(new ByteArrayInputStream(attributes.getBytes()));
		} catch (final IOException e) {
			Logger.error(Activator.PLUGIN_ID, "Could not load dependency properties: " + attributes);
		}
	}

	protected String getProperty(String name) {
		final Object value = fProperties.get(name);
		return (value != null) ? value.toString() : null;
	}

	protected String getProperty(String name, String defaultValue) {
		final String value = getProperty(name);
		return (value != null) ? value : defaultValue;
	}

	protected boolean getProperty(String name, boolean defaultValue) {
		final String value = getProperty(name);
		return (value != null) ? Boolean.parseBoolean(value) : defaultValue;
	}

	protected Properties getProperties() {
		return fProperties;
	}

	protected boolean matchesWithWildcard(String property, String actualValue) {
		if (property == null)
			return true;

		return Pattern.matches(property.replaceAll("\\*", ".*"), actualValue);
	}
}
