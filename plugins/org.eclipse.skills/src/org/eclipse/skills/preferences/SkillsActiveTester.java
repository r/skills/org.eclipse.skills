package org.eclipse.skills.preferences;

import java.util.Objects;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.skills.Activator;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.services.IEvaluationService;

public class SkillsActiveTester extends PropertyTester implements IPropertyChangeListener {

	public SkillsActiveTester() {
		final IPreferenceStore preferences = Activator.getDefault().getPreferenceStore();
		preferences.addPropertyChangeListener(this);
	}

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		if (property.equals("active")) {
			final IPreferenceStore preferences = Activator.getDefault().getPreferenceStore();
			return Objects.equals(expectedValue, preferences.getBoolean(IPreferenceConstants.ACTIVATE_SKILLS));
		}

		return false;
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (IPreferenceConstants.ACTIVATE_SKILLS.equals(event.getProperty())) {
			final IEvaluationService service = PlatformUI.getWorkbench().getService(IEvaluationService.class);
			service.requestEvaluation("org.eclipse.skills.active");
		}
	}
}
