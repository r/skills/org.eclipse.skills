/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.startup;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.skills.Activator;
import org.eclipse.skills.preferences.IPreferenceConstants;
import org.eclipse.skills.service.ISkillService;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.PlatformUI;

/**
 * Activates the skill service on startup depending on preference settings. Also monitors preferences and starts/stops the service accordingly.
 */
public class SkillsLauncher implements IStartup, IPropertyChangeListener {

	private static final long ACTIVATION_RETRY_DELAY = 5 * 1000;

	private static boolean isServiceEnabledByPreferences() {
		return Activator.getDefault().getPreferenceStore().getBoolean(IPreferenceConstants.ACTIVATE_SKILLS);
	}

	public SkillsLauncher() {
		Activator.getDefault().getPreferenceStore().addPropertyChangeListener(this);
	}

	@Override
	public void earlyStartup() {
		checkServiceState();
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		checkServiceState();
	}

	private void checkServiceState() {
		if (isServiceEnabledByPreferences())
			activateService();
		else
			disableService();
	}

	private void disableService() {
		final ISkillService skillService = PlatformUI.getWorkbench().getService(ISkillService.class);
		if (skillService != null)
			skillService.deactivateService();
	}

	private void activateService() {
		new Job("Launching Skills System") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				final ISkillService skillService = PlatformUI.getWorkbench().getService(ISkillService.class);
				if (skillService != null)
					skillService.activateService();
				else
					schedule(ACTIVATION_RETRY_DELAY);

				return Status.OK_STATUS;
			}
		}.schedule();
	}
}
