/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.ui.PlatformUI;
import org.osgi.service.event.EventHandler;

public class BrokerTools {

	private static IEventBroker fCustomBroker = null;

	/**
	 * Set a custom broker instance used in case the default broker is not available. Typically used for headless unittest execution.
	 *
	 * @param customBroker
	 *            custom broker instance (<code>null</code> to use default broker)
	 */
	public static void setCustomBroker(IEventBroker customBroker) {
		fCustomBroker = customBroker;
	}

	public static IEventBroker getBroker() {
		if (fCustomBroker != null)
			return fCustomBroker;

		if (PlatformUI.isWorkbenchRunning())
			return PlatformUI.getWorkbench().getService(IEventBroker.class);

		return null;
	}

	/**
	 * Gracefully post a message (asynchronously) to the event broker service. Does nothing if the broker service is not available.
	 *
	 * @param topic
	 *            broker topic
	 * @param data
	 *            message data
	 */
	public static void post(String topic, Object data) {
		final IEventBroker broker = getBroker();
		if (broker != null)
			broker.post(topic, data);
	}

	/**
	 * Gracefully send a message (synchronously) to the event broker service. Does nothing if the broker service is not available.
	 *
	 * @param topic
	 *            broker topic
	 * @param data
	 *            message data
	 */
	public static void send(String topic, Object data) {
		final IEventBroker broker = getBroker();
		if (broker != null)
			broker.send(topic, data);
	}

	/**
	 * Gracefully subscribe a handler to a topic. Does nothing if the broker service is not available.
	 *
	 * @param topic
	 *            broker topic to subscribe to
	 * @param eventHandler
	 *            handler to subscribe
	 */
	public static void subscribe(String topic, EventHandler eventHandler) {
		final IEventBroker broker = getBroker();
		if (broker != null)
			broker.subscribe(topic, eventHandler);
	}

	/**
	 * Unsubscribe a handler from the broker.
	 *
	 * @param eventHandler
	 *            handler to unsubscribe
	 */
	public static void unsubscribe(EventHandler eventHandler) {
		final IEventBroker broker = getBroker();
		if (broker != null)
			broker.unsubscribe(eventHandler);
	}
}
