/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.views.character;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.skills.model.IUser;
import org.eclipse.skills.service.ISkillService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

public class CharacterView extends ViewPart {

	public static IUser getUser() {
		final ISkillService skillService = PlatformUI.getWorkbench().getService(ISkillService.class);
		if (skillService != null)
			return skillService.getUser();

		return null;
	}

	public CharacterView() {
	}

	@Override
	public void createPartControl(Composite parent) {

		final Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(2, false));

		final LocalResourceManager resourceManager = new LocalResourceManager(JFaceResources.getResources(), composite);

		final TitleComposite titleComposite = new TitleComposite(composite, SWT.NONE, resourceManager);
		titleComposite.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).span(2, 1).indent(20, 0).create());

		final CharacterComposite characterComposite = new CharacterComposite(composite, SWT.NONE, resourceManager);
		characterComposite.setLayoutData(GridDataFactory.fillDefaults().grab(false, true).create());

		final StatsComposite statsComposite = new StatsComposite(composite, SWT.NONE, resourceManager);
		statsComposite.setLayoutData(GridDataFactory.fillDefaults().grab(true, true).create());
	}

	@Override
	public void setFocus() {
	}
}
