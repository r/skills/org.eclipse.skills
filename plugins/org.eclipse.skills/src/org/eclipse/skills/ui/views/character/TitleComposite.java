/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.views.character;

import java.time.Duration;
import java.util.Objects;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.resource.FontDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.jface.util.Throttler;
import org.eclipse.jface.window.Window;
import org.eclipse.skills.model.ISkillsPackage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

public class TitleComposite extends Composite {

	private final Throttler fUiUpdateThrottler = new Throttler(Display.getDefault(), Duration.ofMillis(500), this::updateTitle);

	private final Label fLblName;
	private final Label fLblTitle;

	public TitleComposite(Composite parent, int style, ResourceManager resourceManager) {
		super(parent, style);

		setLayout(new GridLayout(2, false));

		fLblName = createNameLabel(resourceManager);
		fLblTitle = createTitleLabel(resourceManager);

		CharacterView.getUser().eAdapters().add(new UserTitleAdapter());
	}

	private Label createNameLabel(ResourceManager resourceManager) {
		FontDescriptor fontDescriptor = JFaceResources.getHeaderFontDescriptor();
		fontDescriptor = fontDescriptor.setHeight(28);

		final Label lblName = new Label(this, SWT.NONE);
		lblName.setFont(resourceManager.createFont(fontDescriptor));
		lblName.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_TITLE_BACKGROUND));
		lblName.setLayoutData(GridDataFactory.fillDefaults().create());

		lblName.setText(CharacterView.getUser().getName());

		lblName.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				final InputDialog inputDialog = new InputDialog(getShell(), "Change user name", "Please enter your user name",
						CharacterView.getUser().getName(), newText -> newText.isEmpty() ? "An empty user name is not valid." : null);

				if (inputDialog.open() == Window.OK)
					CharacterView.getUser().setName(inputDialog.getValue());
			}
		});

		return lblName;
	}

	private Label createTitleLabel(ResourceManager resourceManager) {
		final FontDescriptor fontDescriptor = JFaceResources.getHeaderFontDescriptor();

		final Label lblTitle = new Label(this, SWT.NONE);
		lblTitle.setFont(resourceManager.createFont(fontDescriptor));
		lblTitle.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_TITLE_BACKGROUND));
		lblTitle.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).create());

		lblTitle.setText(CharacterView.getUser().getTitle());

		return lblTitle;
	}

	private void updateTitle() {
		if (!fLblName.isDisposed())
			fLblName.setText(CharacterView.getUser().getName());

		if (!fLblTitle.isDisposed())
			fLblTitle.setText(CharacterView.getUser().getTitle());

		layout();
	}

	private class UserTitleAdapter extends AdapterImpl {
		@Override
		public void notifyChanged(Notification msg) {
			if (affectsTitle(msg))
				fUiUpdateThrottler.throttledExec();
		}

		private boolean affectsTitle(Notification msg) {
			return (Objects.equals(msg.getFeature(), ISkillsPackage.eINSTANCE.getUser_Title()))
					|| (Objects.equals(msg.getFeature(), ISkillsPackage.eINSTANCE.getUser_Name()));
		}
	}
}
