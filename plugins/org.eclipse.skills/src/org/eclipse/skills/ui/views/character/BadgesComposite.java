/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.views.character;

import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.skills.model.IBadge;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public class BadgesComposite extends Composite {

	public BadgesComposite(Composite parent, int style, ResourceManager resourceManager) {
		super(parent, style);

		final FillLayout fillLayout = new FillLayout(SWT.HORIZONTAL);
		fillLayout.spacing = 20;
		setLayout(fillLayout);

		for (final IBadge badge : CharacterView.getUser().getBadges()) {
			final Label badgeLabel = new Label(this, SWT.NONE);
			badgeLabel.setImage(resourceManager.createImage(badge.getImageDescriptor()));
			badgeLabel.setToolTipText(badge.getTitle());
		}
	}
}
