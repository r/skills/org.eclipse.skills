/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.views.journal;

import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.skills.model.IUserTask;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormText;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

public class DescriptionSection {

	public static Composite create(Composite parent, FormToolkit toolkit, ISelectionProvider selectionProvider) {
		final Section section = toolkit.createSection(parent, ExpandableComposite.TWISTIE | ExpandableComposite.TITLE_BAR);
		toolkit.paintBordersFor(section);
		section.setText("Description");
		section.setLayout(new FillLayout());

		final FormText formText = toolkit.createFormText(section, true);
		formText.setText("<html></html>", true, true);

		section.setClient(formText);

		selectionProvider.addSelectionChangedListener(event -> {
			final Object element = event.getStructuredSelection().getFirstElement();
			if (element instanceof IUserTask)
				formText.setText("<html>" + ((IUserTask) element).getTask().getDescription().getText() + "</html>", true, true);
			else
				formText.setText("<html></html>", true, true);
		});

		return section;
	}

	private DescriptionSection() {
		throw new RuntimeException("Not to be used");
	}
}
