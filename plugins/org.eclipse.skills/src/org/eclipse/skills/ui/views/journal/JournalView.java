/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.views.journal;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collectors;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.jface.util.Throttler;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.skills.Activator;
import org.eclipse.skills.model.IUser;
import org.eclipse.skills.model.IUserTask;
import org.eclipse.skills.service.ISkillService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.ViewPart;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

public class JournalView extends ViewPart implements ISelectionChangedListener, EventHandler {

	private Viewer fListViewer;

	private final Throttler fUpdateThrottler = new Throttler(Display.getDefault(), Duration.ofMillis(500), this::refresh);

	public JournalView() {
	}

	@Override
	public void createPartControl(Composite parent) {

		final FormToolkit toolkit = new FormToolkit(parent.getDisplay());
		final Form form = toolkit.createForm(parent);

		final Composite body = form.getBody();
		toolkit.decorateFormHeading(form);
		toolkit.paintBordersFor(body);
		body.setLayout(new FillLayout(SWT.HORIZONTAL));

		final SashForm sashForm = new SashForm(body, SWT.NONE);
		toolkit.adapt(sashForm);
		toolkit.paintBordersFor(sashForm);

		fListViewer = createTaskList(sashForm);
		refresh();

		createDetailComposite(sashForm, toolkit, fListViewer);
		sashForm.setWeights(new int[] { 2, 5 });

		registerForSkillEvents();
	}

	private void registerForSkillEvents() {
		final IEventBroker service = PlatformUI.getWorkbench().getService(IEventBroker.class);
		service.subscribe(ISkillService.EVENT_BASE + "/*", this);
	}

	@Override
	public void dispose() {
		final IEventBroker service = PlatformUI.getWorkbench().getService(IEventBroker.class);
		service.unsubscribe(this);
	}

	@Override
	public void handleEvent(Event event) {
		fUpdateThrottler.throttledExec();
	}

	/**
	 * Refresh task list. Needs to be executed in the UI thread.
	 */
	public void refresh() {
		final ArrayList<IUserTask> input = new ArrayList<>();
		input.addAll(getRunningTasks());
		input.addAll(getOpenTasks());
		input.addAll(getCompletedTasks());

		fListViewer.setInput(input);
		fListViewer.refresh();
	}

	private Viewer createTaskList(Composite parent) {
		final TableViewer listViewer = new TableViewer(parent, SWT.BORDER | SWT.V_SCROLL);
		listViewer.setContentProvider(ArrayContentProvider.getInstance());
		listViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof IUserTask)
					return ((IUserTask) element).getTask().getTitle();

				return super.getText(element);
			}

			@Override
			public Image getImage(Object element) {
				if (element instanceof IUserTask) {
					if (!((IUserTask) element).isStarted())
						return Activator.getImageDescriptor(Activator.PLUGIN_ID, "/icons/full/obj16/available_task.png").createImage();

					if (((IUserTask) element).isStarted() && !(((IUserTask) element).isCompleted()))
						return Activator.getImageDescriptor(Activator.PLUGIN_ID, "/icons/full/obj16/running_task.png").createImage();

					return Activator.getImageDescriptor(Activator.PLUGIN_ID, "/icons/full/obj16/finished_task.png").createImage();
				}

				return super.getImage(element);
			}
		});

		return listViewer;
	}

	private Composite createDetailComposite(Composite parent, FormToolkit toolkit, ISelectionProvider selectionProvider) {
		final Composite composite = DetailComposite.create(parent, toolkit, selectionProvider);

		// createDetailHeaderComposite(composite, toolkit);

		final Composite headerComposite = DetailHeaderComposite.create(composite, toolkit, selectionProvider);
		headerComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		final Composite descriptionSection = DescriptionSection.create(composite, toolkit, selectionProvider);
		descriptionSection.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		final Composite hintsSection = HintsSection.create(composite, toolkit, selectionProvider);
		hintsSection.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		return composite;
	}

	private java.util.List<IUserTask> getOpenTasks() {
		final java.util.List<IUserTask> tasks = getUser().getUsertasks().stream().filter(t -> !t.isStarted()).collect(Collectors.toList());

		return tasks;
	}

	private java.util.List<IUserTask> getRunningTasks() {
		final java.util.List<IUserTask> tasks = getUser().getUsertasks().stream().filter(t -> t.isStarted()).filter(t -> !t.isCompleted())
				.collect(Collectors.toList());
		Collections.sort(tasks, (t1, t2) -> {
			return t2.getStarted().compareTo(t1.getStarted());
		});

		return tasks;
	}

	private java.util.List<IUserTask> getCompletedTasks() {
		final java.util.List<IUserTask> tasks = getUser().getUsertasks().stream().filter(t -> t.isCompleted()).collect(Collectors.toList());
		Collections.sort(tasks, (t1, t2) -> t2.getFinished().compareTo(t1.getFinished()));

		return tasks;
	}

	@Override
	public void setFocus() {
	}

	public IUser getUser() {
		final ISkillService skillService = PlatformUI.getWorkbench().getService(ISkillService.class);
		if (skillService != null)
			return skillService.getUser();

		return null;
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		final Object userTask = event.getStructuredSelection().getFirstElement();
		if (userTask instanceof IUserTask) {
			displayTaskDetails((IUserTask) userTask);
		}
	}

	private void displayTaskDetails(IUserTask userTask) {
		// fTaskDetails.setUserTask(userTask);
	}
}
