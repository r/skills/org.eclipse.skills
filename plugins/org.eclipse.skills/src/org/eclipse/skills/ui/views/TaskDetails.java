/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.views;

import java.text.DateFormat;
import java.util.Date;

import org.eclipse.skills.model.IUserTask;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Hyperlink;
import org.eclipse.ui.forms.widgets.ScrolledForm;

public class TaskDetails extends Composite {

	private static String formatDate(Date date) {
		return DateFormat.getDateTimeInstance().format(date);
	}

	private ScrolledForm form;
	private IUserTask fTask = null;
	private Composite fHeaderComposite;
	private Label fLblActiveStartDate;
	private Label fLblShownHits;
	private Label fLblCompletedStartDate;
	private Label fLblCompletedFinishedDate;
	private Composite fActiveTaskComposite;
	private Composite fCompletedTaskComposite;
	private Composite fAvailableTaskComposite;

	public TaskDetails(Composite parent, int style) {
		super(parent, style);

		createPartControl();
	}

	public IUserTask getUserTask() {
		return fTask;
	}

	public void createPartControl() {
		final FormToolkit toolkit = new FormToolkit(getDisplay());
		form = toolkit.createScrolledForm(this);
		form.setText("");

		final Composite body = form.getBody();
		toolkit.decorateFormHeading(form.getForm());
		toolkit.paintBordersFor(body);
		form.getBody().setLayout(new GridLayout(1, false));

		createHeaderComposite(toolkit);

		final ExpandableComposite xpndblcmpstNewExpandablecomposite = toolkit.createExpandableComposite(form.getBody(), ExpandableComposite.TWISTIE);
		xpndblcmpstNewExpandablecomposite.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false, 1, 1));
		toolkit.paintBordersFor(xpndblcmpstNewExpandablecomposite);
		xpndblcmpstNewExpandablecomposite.setText("Description");

		final ExpandableComposite xpndblcmpstNewExpandablecomposite_1 = toolkit.createExpandableComposite(form.getBody(), ExpandableComposite.TWISTIE);
		toolkit.paintBordersFor(xpndblcmpstNewExpandablecomposite_1);
		xpndblcmpstNewExpandablecomposite_1.setText("Hints");
	}

	private void createHeaderComposite(final FormToolkit toolkit) {
		fHeaderComposite = new Composite(form.getBody(), SWT.NONE);
		fHeaderComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(fHeaderComposite);
		toolkit.paintBordersFor(fHeaderComposite);
		fHeaderComposite.setLayout(new StackLayout());

		createAvailableTaskComposite(toolkit);
		createActiveTaskComposite(toolkit);
		createCompletedTaskComposite(toolkit);

		((StackLayout) fHeaderComposite.getLayout()).topControl = fAvailableTaskComposite;
	}

	private Composite createAvailableTaskComposite(final FormToolkit toolkit) {
		fAvailableTaskComposite = toolkit.createComposite(fHeaderComposite, SWT.NONE);
		fAvailableTaskComposite.setLayout(new GridLayout(2, false));
		fAvailableTaskComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		toolkit.paintBordersFor(fAvailableTaskComposite);

		final Label lblNewLabel = new Label(fAvailableTaskComposite, SWT.NONE);
		toolkit.adapt(lblNewLabel, true, true);
		lblNewLabel.setText("img");

		final Hyperlink hprlnkNewHyperlink = toolkit.createHyperlink(fAvailableTaskComposite, "start task", SWT.NONE);
		hprlnkNewHyperlink.addHyperlinkListener(new HyperlinkAdapter() {

			@Override
			public void linkActivated(HyperlinkEvent e) {
				getUserTask().setStarted(new Date());
				getUserTask().activate();

				updateUI();
			}
		});
		toolkit.paintBordersFor(hprlnkNewHyperlink);

		return fAvailableTaskComposite;
	}

	private Composite createActiveTaskComposite(final FormToolkit toolkit) {
		fActiveTaskComposite = toolkit.createComposite(fHeaderComposite, SWT.NONE);
		fActiveTaskComposite.setLayout(new GridLayout(3, false));
		fActiveTaskComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		toolkit.paintBordersFor(fActiveTaskComposite);

		toolkit.createLabel(fActiveTaskComposite, "started:", SWT.NONE);
		fLblActiveStartDate = toolkit.createLabel(fActiveTaskComposite, "2020/12/23 17:36", SWT.NONE);

		new Label(fActiveTaskComposite, SWT.NONE);

		toolkit.createLabel(fActiveTaskComposite, "hints used:", SWT.NONE);
		fLblShownHits = toolkit.createLabel(fActiveTaskComposite, "2/5", SWT.NONE);

		final Hyperlink hprlnkNewHyperlink_1 = toolkit.createHyperlink(fActiveTaskComposite, "show hint", SWT.NONE);
		hprlnkNewHyperlink_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		toolkit.paintBordersFor(hprlnkNewHyperlink_1);

		return fActiveTaskComposite;
	}

	private Composite createCompletedTaskComposite(final FormToolkit toolkit) {
		fCompletedTaskComposite = toolkit.createComposite(fHeaderComposite, SWT.NONE);
		fCompletedTaskComposite.setLayout(new GridLayout(2, false));
		fCompletedTaskComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		toolkit.paintBordersFor(fCompletedTaskComposite);

		toolkit.createLabel(fCompletedTaskComposite, "started:", SWT.NONE);
		fLblCompletedStartDate = toolkit.createLabel(fCompletedTaskComposite, "2020/12/23 17:36", SWT.NONE);

		toolkit.createLabel(fCompletedTaskComposite, "completed:", SWT.NONE);
		fLblCompletedFinishedDate = toolkit.createLabel(fCompletedTaskComposite, "2020/12/23 17:36", SWT.NONE);

		return fCompletedTaskComposite;
	}

	public void setUserTask(IUserTask userTask) {
		fTask = userTask;

		updateUI();
	}

	private void updateUI() {
		form.setText(getUserTask().getTask().getTitle());

		if (getUserTask().isCompleted())
			updateCompletedTaskHeader();

		else if (getUserTask().isStarted())
			updateActiveTaskHeader();

		else
			updateAvailableTaskHeader();

		layout(true);
	}

	private void updateAvailableTaskHeader() {
		((StackLayout) fHeaderComposite.getLayout()).topControl = fAvailableTaskComposite;
	}

	private void updateActiveTaskHeader() {
		fLblActiveStartDate.setText(formatDate(getUserTask().getStarted()));
		fLblShownHits.setText(getUserTask().getHintsDisplayed() + "/" + getUserTask().getTask().getHints().size());

		((StackLayout) fHeaderComposite.getLayout()).topControl = fActiveTaskComposite;
	}

	private void updateCompletedTaskHeader() {
		fLblCompletedStartDate.setText(formatDate(getUserTask().getStarted()));
		fLblCompletedFinishedDate.setText(formatDate(getUserTask().getFinished()));

		((StackLayout) fHeaderComposite.getLayout()).topControl = fCompletedTaskComposite;
	}
}
