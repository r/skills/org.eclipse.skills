/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.views.journal;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.skills.model.IHint;
import org.eclipse.skills.model.IUserTask;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormText;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

public class HintsSection {

	public static Composite create(Composite parent, FormToolkit toolkit, ISelectionProvider selectionProvider) {
		final Section section = toolkit.createSection(parent, ExpandableComposite.TWISTIE | ExpandableComposite.TITLE_BAR);
		toolkit.paintBordersFor(section);
		section.setText("Hints");
		section.setLayout(new FillLayout());

		final Composite composite = toolkit.createComposite(section);
		composite.setLayout(new RowLayout(SWT.VERTICAL));

		section.setClient(composite);

		selectionProvider.addSelectionChangedListener(event -> {
			for (final Control child : composite.getChildren())
				child.dispose();

			final Object element = event.getStructuredSelection().getFirstElement();
			if (element instanceof IUserTask) {
				final EList<IHint> hints = ((IUserTask) element).getTask().getHints();
				for (int index = 0; index < ((IUserTask) element).getHintsDisplayed(); index++) {
					if (hints.size() > index) {
						final FormText formText = toolkit.createFormText(composite, true);
						formText.setText("<html>" + hints.get(index).getText() + "</html>", true, true);
					}
				}
			}
		});

		return section;
	}

	private HintsSection() {
		throw new RuntimeException("Not to be used");
	}
}
