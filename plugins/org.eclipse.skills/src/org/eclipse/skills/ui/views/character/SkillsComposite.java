/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.views.character;

import java.time.Duration;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.jface.util.Throttler;
import org.eclipse.skills.model.IDescription;
import org.eclipse.skills.model.ISkill;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;

public class SkillsComposite extends Composite {

	private static final String FLAG_WITH_PROGRESSBAR = "hasProgressbar";

	private final Throttler fUiUpdateThrottler = new Throttler(Display.getDefault(), Duration.ofMillis(500), this::updateControls);

	private final Adapter fSkillAdapter = new SkillAdapter();

	private final ResourceManager fResourceManager;

	public SkillsComposite(Composite parent, int style, ResourceManager resourceManager, List<ISkill> skills) {
		super(parent, style);

		fResourceManager = resourceManager;

		setLayout(new GridLayout(3, false));

		setSkills(skills);
	}

	public void setSkills(Collection<ISkill> skills) {

		for (final ISkill skill : skills) {
			createSkillElement(skill);
			skill.eAdapters().add(fSkillAdapter);
		}

		// trigger fresh layout
		getParent().layout();
	}

	private void createSkillElement(ISkill skill) {

		final Label lblImage = createImageLabel(skill);
		lblImage.setLayoutData(GridDataFactory.fillDefaults().span(1, 2).align(SWT.FILL, SWT.DOWN).indent(0, 6).create());

		final Label lblTitle = createSkillNameLabel(skill);
		lblTitle.setLayoutData(GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.DOWN).grab(true, true).indent(10, 0).create());

		final Label lblLevel = createSkillLevelLabel(skill);
		lblLevel.setLayoutData(GridDataFactory.fillDefaults().span(1, 2).align(SWT.RIGHT, SWT.DOWN).grab(false, true).indent(10, 0).create());

		final ProgressBar progressBar = createProgressBar(skill);
		progressBar.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).align(SWT.FILL, SWT.FILL).indent(10, 0).create());
	}

	private ProgressBar createProgressBar(ISkill skill) {
		final ProgressBar progressBar = new ProgressBar(this, SWT.NONE);
		progressBar.setData(skill);
		updateControl(progressBar, skill);

		return progressBar;
	}

	private Label createSkillLevelLabel(ISkill skill) {
		final Label lblLevel = new Label(this, SWT.NONE);
		lblLevel.setData(skill);
		lblLevel.setFont(fResourceManager.createFont(JFaceResources.getDefaultFontDescriptor().setHeight(12)));

		updateControl(lblLevel, skill);

		return lblLevel;
	}

	private Label createSkillNameLabel(ISkill skill) {
		final Label lblTitle = new Label(this, SWT.NONE);
		lblTitle.setFont(fResourceManager.createFont(JFaceResources.getTextFontDescriptor()));
		lblTitle.setText(skill.getName().toUpperCase());
		lblTitle.setToolTipText(getSkillDescription(skill));

		return lblTitle;
	}

	private Label createImageLabel(ISkill skill) {
		final Label lblImage = new Label(this, SWT.NONE);
		lblImage.setToolTipText(getSkillDescription(skill));

		final ImageDescriptor imageDescriptor = skill.getImageDescriptor();
		if (imageDescriptor != null)
			lblImage.setImage(fResourceManager.createImage(imageDescriptor));
		else
			lblImage.setImage(fResourceManager.createImage(ImageDescriptor.getMissingImageDescriptor()));

		return lblImage;
	}

	private String getSkillDescription(ISkill skill) {
		final IDescription description = skill.getDescription();
		if (description != null) {
			final String text = description.getText();
			if (text != null)
				return text;
		}

		return "";
	}

	public void updateControls() {
		for (final Control control : getChildren()) {
			if (control.getData() instanceof ISkill)
				updateControl(control, (ISkill) control.getData());
		}

		layout();
	}

	public void updateControl(Control control, ISkill skill) {
		final int level = skill.getProgression().getLevel(skill.getExperience());
		final int currentLevelMinXP = skill.getProgression().getMinimumXpForLevel(level);
		final int nextLevelMinXP = skill.getProgression().getMinimumXpForLevel(level + 1);

		if (control instanceof Label) {
			((Label) control).setText(String.format("Lvl %d", level));
			((Label) control)
					.setToolTipText(String.format("Earn %d more %s to reach level %d", (nextLevelMinXP - skill.getExperience()), skill.getName(), level + 1));

		} else if (control instanceof ProgressBar) {
			((ProgressBar) control).setMinimum(currentLevelMinXP);
			((ProgressBar) control).setMaximum(nextLevelMinXP);
			((ProgressBar) control).setSelection(skill.getExperience());
			((ProgressBar) control).setToolTipText(String.format("%d %s XP", skill.getExperience(), skill.getName()));
		}
	}

	private class SkillAdapter extends AdapterImpl {
		@Override
		public void notifyChanged(Notification msg) {

			if (msg.getNotifier() instanceof ISkill)
				fUiUpdateThrottler.throttledExec();
		}
	}
}
