/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.views.character;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.skills.model.ISkill;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public class StatsComposite extends Composite {

	private static final int INDENTATION = 30;

	private static int compareSkillsByName(ISkill arg0, ISkill arg1) {
		return arg0.getName().compareTo(arg1.getName());
	}

	private static List<ISkill> getPrimarySkills(Collection<ISkill> skills) {
		final List<ISkill> primarySkills = skills.stream().filter(s -> s.isBaseSkill()).collect(Collectors.toList());
		Collections.sort(primarySkills, StatsComposite::compareSkillsByName);

		return primarySkills;
	}

	private static List<ISkill> getSecondarySkills(Collection<ISkill> skills) {
		final List<ISkill> secondarySkills = skills.stream().filter(s -> !s.isBaseSkill()).collect(Collectors.toList());
		Collections.sort(secondarySkills, StatsComposite::compareSkillsByName);

		return secondarySkills;
	}

	public StatsComposite(Composite parent, int style, ResourceManager resourceManager) {
		super(parent, style);

		setLayout(new GridLayout());

		final SkillsComposite xp = new SkillsComposite(this, SWT.NONE, resourceManager, Arrays.asList(CharacterView.getUser().getExperience()));
		xp.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).align(SWT.FILL, SWT.FILL).create());

		addSeparator();

		final Label lblPrimarySkills = new Label(this, SWT.NONE);
		lblPrimarySkills.setText("Your Stats:");

		final List<ISkill> primarySkills = getPrimarySkills(CharacterView.getUser().getSkills());
		final SkillsComposite primary = new SkillsComposite(this, SWT.NONE, resourceManager, primarySkills);
		primary.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).indent(INDENTATION, 0).create());

		addSeparator();

		final Label lblSecondarySkills = new Label(this, SWT.NONE);
		lblSecondarySkills.setText("Your Skills:");

		final List<ISkill> secondarySkills = getSecondarySkills(CharacterView.getUser().getSkills());
		final SkillsComposite secondary = new SkillsComposite(this, SWT.NONE, resourceManager, secondarySkills);
		secondary.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).indent(INDENTATION, 0).create());

		final BadgesComposite badgesComposite = new BadgesComposite(this, SWT.NONE, resourceManager);
		badgesComposite.setLayoutData(GridDataFactory.fillDefaults().align(SWT.CENTER, SWT.BOTTOM).grab(true, true).create());
	}

	private void addSeparator() {
		final Label separator = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		separator.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).align(SWT.FILL, SWT.FILL).indent(0, 6).create());
	}
}
