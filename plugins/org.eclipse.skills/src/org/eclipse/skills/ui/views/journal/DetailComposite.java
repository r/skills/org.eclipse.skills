/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.views.journal;

import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.skills.model.IUserTask;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;

public class DetailComposite {

	private static final String DEFAULT_TITLE = "<Please select a task from the list>";

	public static Composite create(Composite parent, FormToolkit toolkit, ISelectionProvider selectionProvider) {
		final ScrolledForm scrolledForm = toolkit.createScrolledForm(parent);
		scrolledForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		toolkit.paintBordersFor(scrolledForm);
		scrolledForm.setText(DEFAULT_TITLE);
		scrolledForm.getBody().setLayout(new GridLayout(1, false));

		selectionProvider.addSelectionChangedListener(event -> {
			final Object element = event.getStructuredSelection().getFirstElement();
			if (element instanceof IUserTask)
				scrolledForm.setText(((IUserTask) element).getTask().getTitle());
			else
				scrolledForm.setText(DEFAULT_TITLE);
		});

		return scrolledForm.getBody();
	}

	private DetailComposite() {
		throw new RuntimeException("Not to be used");
	}
}
