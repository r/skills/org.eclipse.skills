/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.views.character;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.skills.Activator;
import org.eclipse.skills.Logger;
import org.eclipse.skills.service.AvatarCreator;
import org.eclipse.skills.service.ISkillService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.PlatformUI;

public class CharacterComposite extends Composite {

	public CharacterComposite(Composite parent, int style, ResourceManager resourceManager) {
		super(parent, style);

		setLayout(new GridLayout(2, false));

		final Label lblCharacterImage = createImageLabel(resourceManager);
		createLoadImageLink(lblCharacterImage, resourceManager);
		createRandomImageLink(lblCharacterImage, resourceManager);
	}

	private Label createImageLabel(ResourceManager resourceManager) {
		final Label avatarLabel = new Label(this, SWT.NONE);
		avatarLabel.setLayoutData(GridDataFactory.fillDefaults().align(SWT.CENTER, SWT.CENTER).grab(false, true).span(2, 1).indent(0, 20).create());

		updateAvatarImage(avatarLabel, resourceManager);

		return avatarLabel;
	}

	private void createRandomImageLink(Label lblCharacterImage, ResourceManager resourceManager) {
		final Link lnkRandomImage = new Link(this, SWT.NONE);
		lnkRandomImage.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lnkRandomImage.setText("<a>Random image</a>");
		lnkRandomImage.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {

				try {
					storeAvatarImage(AvatarCreator.getRandomAvatarData());

					updateAvatarImage(lblCharacterImage, resourceManager);

				} catch (final IOException ex) {
					Logger.error(Activator.PLUGIN_ID, "Could not load random avatar image", ex);
				}
			}

		});
	}

	private void createLoadImageLink(Label lblCharacterImage, ResourceManager resourceManager) {
		final Link lnkLoadImage = new Link(this, SWT.NONE);
		lnkLoadImage.setText("<a>Load image...</a>");
		lnkLoadImage.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				final FileDialog fileDialog = new FileDialog(Display.getDefault().getActiveShell());
				final String location = fileDialog.open();
				if (location != null) {
					try {
						final File imageFile = new File(location);
						final ISkillService skillService = PlatformUI.getWorkbench().getService(ISkillService.class);
						skillService.storeResource(ISkillService.RESOURCE_AVATAR, Files.readAllBytes(imageFile.toPath()));

						updateAvatarImage(lblCharacterImage, resourceManager);

					} catch (final IOException ex) {
						Logger.error(Activator.PLUGIN_ID, "Could not load avatar image from file: " + location, ex);
					}
				}
			}

		});
	}

	private void storeAvatarImage(final byte[] imageData) throws IOException {
		final ISkillService skillService = PlatformUI.getWorkbench().getService(ISkillService.class);
		skillService.storeResource(ISkillService.RESOURCE_AVATAR, imageData);
	}

	private void updateAvatarImage(Label avatarLabel, ResourceManager resourceManager) {
		avatarLabel.setImage(getAvatarImage(resourceManager));
		avatarLabel.getParent().layout(true);
	}

	private Image getAvatarImage(ResourceManager resourceManager) {
		final ISkillService skillService = PlatformUI.getWorkbench().getService(ISkillService.class);
		if (skillService != null)
			return resourceManager.createImage(skillService.getUser().getAvatar());

		return null;
	}
}
