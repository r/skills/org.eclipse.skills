/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.views.journal;

import java.text.DateFormat;
import java.util.Date;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.skills.model.IUserTask;
import org.eclipse.skills.service.ISkillService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Hyperlink;

public class DetailHeaderComposite extends Composite {

	private static String formatDate(Date date) {
		return DateFormat.getDateTimeInstance().format(date);
	}

	public static Composite create(Composite parent, FormToolkit toolkit, ISelectionProvider selectionProvider) {
		return new DetailHeaderComposite(parent, toolkit, selectionProvider);
	}

	private final Composite fBlindComposite;

	private IUserTask fTask = null;

	private final Composite fAvailableTaskComposite;

	private final Composite fActiveTaskComposite;

	private final Composite fCompletedTaskComposite;

	public DetailHeaderComposite(Composite parent, FormToolkit toolkit, ISelectionProvider selectionProvider) {
		super(parent, toolkit.getOrientation());

		toolkit.paintBordersFor(this);

		setLayout(new StackLayout());

		fBlindComposite = createBlindComposite(toolkit);

		fAvailableTaskComposite = createAvailableTaskComposite(toolkit, selectionProvider);
		fActiveTaskComposite = createActiveTaskComposite(toolkit, selectionProvider);
		fCompletedTaskComposite = createCompletedTaskComposite(toolkit, selectionProvider);

		setTopControl(fBlindComposite);

		selectionProvider.addSelectionChangedListener(event -> {
			final Object element = event.getStructuredSelection().getFirstElement();
			if (element instanceof IUserTask) {
				fTask = (IUserTask) element;

				if (getUserTask().isCompleted())
					setTopControl(fCompletedTaskComposite);

				else if (getUserTask().isStarted())
					setTopControl(fActiveTaskComposite);

				else
					setTopControl(fAvailableTaskComposite);

			} else
				setTopControl(fBlindComposite);

			layout(true);
		});
	}

	private void setTopControl(Control control) {
		((StackLayout) getLayout()).topControl = control;
	}

	private Composite createBlindComposite(FormToolkit toolkit) {
		final Composite composite = toolkit.createComposite(this, SWT.NONE);
		toolkit.paintBordersFor(composite);

		return composite;
	}

	private Composite createAvailableTaskComposite(final FormToolkit toolkit, ISelectionProvider selectionProvider) {
		final Composite composite = toolkit.createComposite(this, SWT.NONE);
		composite.setLayout(new GridLayout(2, false));
		toolkit.paintBordersFor(composite);

		final Label lblNewLabel = new Label(composite, SWT.NONE);
		toolkit.adapt(lblNewLabel, true, true);
		lblNewLabel.setText("img");

		final Hyperlink hprlnkStartTask = toolkit.createHyperlink(composite, "start task", SWT.NONE);
		hprlnkStartTask.addHyperlinkListener(new HyperlinkAdapter() {

			@Override
			public void linkActivated(HyperlinkEvent e) {
				new Job("Start skill task") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						PlatformUI.getWorkbench().getService(ISkillService.class).startTask(getUserTask());

						return Status.OK_STATUS;
					}
				}.schedule();
			}
		});
		toolkit.paintBordersFor(hprlnkStartTask);

		return composite;
	}

	public IUserTask getUserTask() {
		return fTask;
	}

	private Composite createActiveTaskComposite(final FormToolkit toolkit, ISelectionProvider selectionProvider) {
		final Composite composite = toolkit.createComposite(this, SWT.NONE);
		composite.setLayout(new GridLayout(3, false));
		toolkit.paintBordersFor(composite);

		toolkit.createLabel(composite, "started:", SWT.NONE);
		final Label lblActiveStartDate = toolkit.createLabel(composite, "", SWT.NONE);

		new Label(composite, SWT.NONE);

		toolkit.createLabel(composite, "hints used:", SWT.NONE);
		final Label lblShownHints = toolkit.createLabel(composite, "", SWT.NONE);

		final Hyperlink hprlnkShowHint = toolkit.createHyperlink(composite, "show hint", SWT.NONE);
		hprlnkShowHint.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		toolkit.paintBordersFor(hprlnkShowHint);
		hprlnkShowHint.addHyperlinkListener(new HyperlinkAdapter() {

			@Override
			public void linkActivated(HyperlinkEvent e) {
				getUserTask().revealNextHint();
				layout(true);
			}
		});

		selectionProvider.addSelectionChangedListener(event -> {
			final Object element = event.getStructuredSelection().getFirstElement();
			if (element instanceof IUserTask) {
				if (((IUserTask) element).isStarted()) {
					lblActiveStartDate.setText(formatDate(((IUserTask) element).getStarted()));

					final int hintsDisplayed = ((IUserTask) element).getHintsDisplayed();
					final int hintsCount = ((IUserTask) element).getTask().getHints().size();

					lblShownHints.setText(hintsDisplayed + "/" + hintsCount);

					hprlnkShowHint.setVisible(hintsDisplayed < hintsCount);

					lblActiveStartDate.requestLayout();
					lblShownHints.requestLayout();
					hprlnkShowHint.requestLayout();
				}
			}
		});

		return composite;
	}

	private Composite createCompletedTaskComposite(final FormToolkit toolkit, ISelectionProvider selectionProvider) {
		final Composite composite = toolkit.createComposite(this, SWT.NONE);
		composite.setLayout(new GridLayout(2, false));
		toolkit.paintBordersFor(composite);

		toolkit.createLabel(composite, "started:", SWT.NONE);
		final Label lblStartDate = toolkit.createLabel(composite, "", SWT.NONE);

		toolkit.createLabel(composite, "completed:", SWT.NONE);
		final Label lblCompletionDate = toolkit.createLabel(composite, "", SWT.NONE);

		selectionProvider.addSelectionChangedListener(event -> {
			final Object element = event.getStructuredSelection().getFirstElement();
			if (element instanceof IUserTask) {
				if (((IUserTask) element).isCompleted()) {
					lblStartDate.setText(formatDate(((IUserTask) element).getStarted()));
					lblCompletionDate.setText(formatDate(((IUserTask) element).getFinished()));

					lblStartDate.requestLayout();
					lblCompletionDate.requestLayout();
				}
			}
		});

		return composite;
	}
}
