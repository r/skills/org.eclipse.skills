/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.preferences;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.skills.Activator;
import org.eclipse.skills.preferences.IPreferenceConstants;
import org.eclipse.skills.service.ISkillService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

public class SkillsPage extends PreferencePage implements IWorkbenchPreferencePage {

	private Button fChkActivateSkills;

	public SkillsPage() {
	}

	@Override
	public void init(IWorkbench workbench) {
		setPreferenceStore(getPreferenceStore());
	}

	@Override
	protected Control createContents(Composite parent) {
		final Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new GridLayout(1, false));

		fChkActivateSkills = new Button(container, SWT.CHECK);
		fChkActivateSkills.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		fChkActivateSkills.setText("Activate skill system");

		final Button btnResetProgress = new Button(container, SWT.NONE);
		btnResetProgress.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		btnResetProgress.setText("Reset progress");
		btnResetProgress.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (MessageDialog.openConfirm(getShell(), "Reset all progress?",
						"Do you want to reset all your progress and start from scratch?\nRemember resetting is irreversible!")) {
					final ISkillService skillService = PlatformUI.getWorkbench().getService(ISkillService.class);
					skillService.resetProgress();
				}
			}
		});

		performDefaults();

		return container;
	}

	@Override
	protected void performDefaults() {
		final boolean activation = getPreferenceStore().getBoolean(IPreferenceConstants.ACTIVATE_SKILLS);
		fChkActivateSkills.setSelection(activation);

		super.performDefaults();
	}

	@Override
	public boolean performOk() {
		final boolean activationStatus = fChkActivateSkills.getSelection();
		getPreferenceStore().setValue(IPreferenceConstants.ACTIVATE_SKILLS, activationStatus);

		return super.performOk();
	}

	@Override
	public IPreferenceStore getPreferenceStore() {
		return Activator.getDefault().getPreferenceStore();
	}
}
