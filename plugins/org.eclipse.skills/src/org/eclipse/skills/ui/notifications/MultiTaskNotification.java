/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.notifications;

import java.util.List;

import org.eclipse.skills.model.IUserTask;
import org.eclipse.swt.widgets.Display;

public class MultiTaskNotification extends AbstractNotification {
	private final List<IUserTask> fFinishedTasks;

	public static void display(List<IUserTask> finishedTasks) {
		Display.getDefault().asyncExec(() -> {
			AbstractNotification.display(new MultiTaskNotification(finishedTasks));
		});
	}

	public MultiTaskNotification(List<IUserTask> finishedTasks) {
		super();

		fFinishedTasks = finishedTasks;
	}

	@Override
	protected String getTitle() {
		return "Task Update";
	}

	@Override
	protected String getMessage() {
		return "Multiple tasks changed.";
	}
}
