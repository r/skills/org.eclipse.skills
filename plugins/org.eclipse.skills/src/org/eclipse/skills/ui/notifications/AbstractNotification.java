/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.notifications;

import org.eclipse.jface.notifications.AbstractNotificationPopup;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public abstract class AbstractNotification extends AbstractNotificationPopup {

	public static void display(AbstractNotificationPopup popup) {
		popup.setFadingEnabled(true);
		popup.setDelayClose(3000);
		popup.open();
	}

	public AbstractNotification() {
		super(Display.getDefault());
	}

	@Override
	protected Shell getParentShell() {
		return Display.getDefault().getActiveShell();
	}

	@Override
	protected String getPopupShellTitle() {
		return getTitle();
	}

	@Override
	protected void createContentArea(Composite parent) {

		final Composite container = new Composite(parent, SWT.NULL);

		final GridData data = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		container.setLayoutData(data);

		container.setLayout(new GridLayout(1, false));

		final Label successMsg = new Label(container, SWT.NULL);
		successMsg.setText(getTitle());

		new Label(container, SWT.NONE);
		final Text messageLabel = new Text(container, SWT.MULTI);
		messageLabel.setText(getMessage());
	}

	protected abstract String getTitle();

	protected abstract String getMessage();
}
