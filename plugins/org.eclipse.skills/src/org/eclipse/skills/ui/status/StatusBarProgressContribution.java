package org.eclipse.skills.ui.status;

import java.time.Duration;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.jface.util.Throttler;
import org.eclipse.skills.BrokerTools;
import org.eclipse.skills.model.ISkill;
import org.eclipse.skills.model.ISkillsPackage;
import org.eclipse.skills.model.IUser;
import org.eclipse.skills.service.ISkillService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.menus.WorkbenchWindowControlContribution;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

public class StatusBarProgressContribution extends WorkbenchWindowControlContribution implements EventHandler {

	private final Throttler fUpdateThrottler = new Throttler(Display.getDefault(), Duration.ofMillis(500), this::refreshProgressBar);

	private final Adapter fExperienceChangeAdapter = new AdapterImpl() {
		@Override
		public void notifyChanged(Notification msg) {
			if (ISkillsPackage.eINSTANCE.getSkill_Experience().equals(msg.getFeature())) {
				if (msg.getNotifier() instanceof ISkill)
					fUpdateThrottler.throttledExec();
			}
		}
	};

	private CustomDrawProgressBar fProgressBar;

	public StatusBarProgressContribution() {
	}

	public StatusBarProgressContribution(String id) {
		super(id);
	}

	@Override
	protected Control createControl(Composite parent) {
		fProgressBar = new CustomDrawProgressBar(parent);

		fProgressBar.setMinimum(0);
		fProgressBar.setMaximum(100);
		fProgressBar.setSelection(0);
		fProgressBar.setStatusText("Level 1");
		fProgressBar.setToolTipText("");
		fProgressBar.setForeground(parent.getDisplay().getSystemColor(SWT.COLOR_YELLOW));

		addChangeListener();

		return fProgressBar;
	}

	private void addChangeListener() {
		final ISkillService skillService = PlatformUI.getWorkbench().getService(ISkillService.class);
		if (skillService != null)
			skillService.getUser().getExperience().eAdapters().add(fExperienceChangeAdapter);

		BrokerTools.subscribe(ISkillService.EVENT_USER_UPDATE, this);
	}

	@Override
	public void dispose() {
		BrokerTools.unsubscribe(this);

		final ISkillService skillService = PlatformUI.getWorkbench().getService(ISkillService.class);
		if (skillService != null)
			skillService.getUser().getExperience().eAdapters().remove(fExperienceChangeAdapter);

		super.dispose();
	}

	@Override
	public void handleEvent(Event event) {
		final Object user = event.getProperty(IEventBroker.DATA);
		if (user instanceof IUser)
			fUpdateThrottler.throttledExec();
	}

	public void refreshProgressBar() {
		if (!fProgressBar.isDisposed()) {
			final ISkillService skillService = PlatformUI.getWorkbench().getService(ISkillService.class);
			if (skillService != null) {
				final ISkill experience = skillService.getUser().getExperience();

				final int currentLevel = experience.getProgression().getLevel(experience.getExperience());
				final int levelStart = experience.getProgression().getMinimumXpForLevel(currentLevel);
				final int nextLevelStart = experience.getProgression().getMinimumXpForLevel(currentLevel + 1);

				fProgressBar.setMinimum(levelStart);
				fProgressBar.setMaximum(nextLevelStart);
				fProgressBar.setSelection(experience.getExperience());

				fProgressBar.setStatusText(String.format("Level %d", currentLevel));
				fProgressBar.setToolTipText(String.format("%d XP to reach next level", (nextLevelStart - experience.getExperience())));

				fProgressBar.redraw();
			}
		}
	}
}
