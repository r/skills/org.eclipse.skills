/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.status;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.internal.TrimUtil;

/**
 * The Heap Status control, which shows the heap usage statistics in the window trim.
 *
 * @since 3.1
 */
public class CustomDrawProgressBar extends Canvas {

	private static final int MIN_WIDTH = 150;
	private String fStatusText = null;
	private int fMinimum = 0;
	private int fMaximum = 100;
	private int fSelection = 0;

	/**
	 * Creates a new heap status control with the given parent, and using the given preference store to obtain settings such as the refresh interval.
	 *
	 * @param parent
	 *            the parent composite
	 * @param prefStore
	 *            the preference store
	 */
	public CustomDrawProgressBar(Composite parent) {
		super(parent, SWT.NONE);

		addPaintListener(e -> {
			final GC gc = e.gc;
			final Rectangle area = getClientArea();

			gc.setBackground(getBackground());
			gc.setForeground(getBackground());
			gc.fillRectangle(area);

			final int fillWidth = (int) (getFillPercentage() * area.width);
			gc.setBackground(getForeground());
			gc.fillRectangle(area.x, area.y, fillWidth, area.height);

			final String statusText = getStatusText();
			if (statusText != null) {
				final Point textSize = gc.textExtent(statusText);
				final int sx = ((area.width - textSize.x) / 2) + area.x + 1;
				final int sy = ((area.height - textSize.y) / 2) + area.y + 1;
				gc.setForeground(getDisplay().getSystemColor(SWT.COLOR_BLACK));
				gc.drawString(statusText, sx, sy, true);
			}
		});
	}

	private double getFillPercentage() {
		return ((double) (fSelection - fMinimum) / (fMaximum - fMinimum));
	}

	@Override
	public Point computeSize(int wHint, int hHint, boolean changed) {
		final String statusText = getStatusText();
		final GC gc = new GC(this);
		final Point p = gc.textExtent((statusText != null) ? statusText : "Lev");
		int height = p.y + 4;
		height = Math.max(TrimUtil.TRIM_DEFAULT_HEIGHT, height);
		final int width = Math.max(p.x, MIN_WIDTH);
		gc.dispose();

		return new Point(width, height);
	}

	public String getStatusText() {
		return fStatusText;
	}

	public void setStatusText(String statusText) {
		fStatusText = statusText;
		// update();
		getParent().requestLayout();
	}

	public void setMinimum(int minimum) {
		fMinimum = minimum;
	}

	public void setMaximum(int maximum) {
		fMaximum = maximum;
	}

	public void setSelection(int selection) {
		fSelection = selection;
	}
}