/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.questeditor.wizards;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.skills.Activator;
import org.eclipse.skills.Logger;
import org.eclipse.skills.model.IQuest;
import org.eclipse.skills.model.ISkillsFactory;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;

public class QuestCreationPage extends WizardNewFileCreationPage {

	public QuestCreationPage(final String pageName, final IStructuredSelection selection) {
		super(pageName, selection);
	}

	@Override
	protected InputStream getInitialContents() {
		final IQuest quest = ISkillsFactory.eINSTANCE.createQuest();
		quest.setTitle("New Quest");

		try {
			return new ByteArrayInputStream(serializeQuest(quest));

		} catch (final IOException e) {
			Logger.error(Activator.PLUGIN_ID, "Could not create initial Skills quest data");
			return null;
		}
	}

	/**
	 * Serialize a quest definition to a byte array.
	 *
	 * @param quest
	 *            quest to serialize
	 * @return xml representation of definition
	 * @throws IOException
	 *             not expected as ByteArrayOutputStream does not throw
	 */
	public static byte[] serializeQuest(IQuest quest) throws IOException {

		final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		final Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		final Map<String, Object> extensionsMap = reg.getExtensionToFactoryMap();
		extensionsMap.put("skills", new XMIResourceFactoryImpl());

		final ResourceSet resourceSet = new ResourceSetImpl();
		final Resource resource = resourceSet.createResource(URI.createURI("*.skills"));
		resource.getContents().add(quest);

		resource.save(outputStream, Collections.EMPTY_MAP);
		return outputStream.toByteArray();
	}
}
