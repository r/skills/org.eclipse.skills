/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.questeditor.editor;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emfforms.spi.editor.GenericEditor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PartInitException;

public class QuestEditor extends GenericEditor {

	@Override
	protected ResourceSet loadResource(IEditorInput editorInput) throws PartInitException {
		final ResourceSet resourceSet = super.loadResource(editorInput);

		resourceSet.getResource(URI.createURI("platform:/plugin/org.eclipse.skills/resources/default.skills"), true);

		return resourceSet;
	}
}
