/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.questeditor.renderers.dependencytree;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.emf.databinding.IEMFObservable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.core.swt.SimpleControlSWTControlSWTRenderer;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.template.model.VTViewTemplateProvider;
import org.eclipse.emfforms.internal.swt.treemasterdetail.DefaultTreeViewerCustomization;
import org.eclipse.emfforms.spi.common.report.ReportService;
import org.eclipse.emfforms.spi.core.services.databinding.DatabindingFailedException;
import org.eclipse.emfforms.spi.core.services.databinding.EMFFormsDatabinding;
import org.eclipse.emfforms.spi.core.services.editsupport.EMFFormsEditSupport;
import org.eclipse.emfforms.spi.core.services.label.EMFFormsLabelProvider;
import org.eclipse.emfforms.spi.core.services.label.NoLabelFoundException;
import org.eclipse.emfforms.spi.swt.treemasterdetail.TreeViewerCustomization;
import org.eclipse.emfforms.spi.swt.treemasterdetail.TreeViewerSWTFactory;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/**
 * A renderer to render task dependencies/goals.
 */
public class DependencyControlRenderer extends SimpleControlSWTControlSWTRenderer {

	/**
	 * Default constructor.
	 *
	 * @param vElement
	 *            the view model element to be rendered
	 * @param viewContext
	 *            the view context
	 * @param reportService
	 *            The {@link ReportService}
	 * @param emfFormsDatabinding
	 *            The {@link EMFFormsDatabinding}
	 * @param emfFormsLabelProvider
	 *            The {@link EMFFormsLabelProvider}
	 * @param vtViewTemplateProvider
	 *            The {@link VTViewTemplateProvider}
	 * @param emfFormsEditSupport
	 *            The {@link EMFFormsEditSupport}
	 */
	@Inject
	public DependencyControlRenderer(VControl vElement, ViewModelContext viewContext, ReportService reportService, EMFFormsDatabinding emfFormsDatabinding,
			EMFFormsLabelProvider emfFormsLabelProvider, VTViewTemplateProvider vtViewTemplateProvider, EMFFormsEditSupport emfFormsEditSupport) {
		super(vElement, viewContext, reportService, emfFormsDatabinding, emfFormsLabelProvider, vtViewTemplateProvider);
	}

	@Override
	protected Control createSWTControl(Composite parent) {
		final TreeViewer treeViewer = TreeViewerSWTFactory.createTreeViewer(parent, getInput(), getTreeCustomization());

		return treeViewer.getTree();
	}

	private TreeViewerCustomization getTreeCustomization() {
		final DefaultTreeViewerCustomization customization = new DefaultTreeViewerCustomization();

		customization.setMenu(new DependencyControlMenuProvider(getInput()));
		customization.setTree(new DependencyControlTreeViewerBuilder(getColumnTitle()));

		return customization;
	}

	protected int getTableHeightHint() {
		return 400;
	}

	private Object getInput() {

		try {
			final IEMFObservable emfObservable = (IEMFObservable) getModelValue();
			final EObject domainObject = (EObject) emfObservable.getObserved(); // MTask
			final EStructuralFeature feature = emfObservable.getStructuralFeature();

			final Object eGet = domainObject.eGet(feature);

			return eGet;

		} catch (final DatabindingFailedException e) {
			// TODO handle this exception (but for now, at least know it happened)
			throw new RuntimeException(e);
		}
	}

	private String getColumnTitle() {
		try {
			return String.valueOf(
					getEMFFormsLabelProvider().getDisplayName(getVElement().getDomainModelReference(), getViewModelContext().getDomainModel()).getValue())
					+ "s";

		} catch (final NoLabelFoundException e) {
		}

		return "Dependencies";
	}

	@Override
	protected String getUnsetText() {
		return "Please select";
	}

	@Override
	protected Binding[] createBindings(Control control) throws DatabindingFailedException {
		// TODO Auto-generated method stub
		return null;
	}

}