/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.questeditor.renderers.dependencytree;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecp.view.spi.table.swt.DetailDialog;
import org.eclipse.emfforms.spi.swt.treemasterdetail.TreeViewerBuilder;
import org.eclipse.jface.layout.TreeColumnLayout;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.skills.model.ICustomDependency;
import org.eclipse.skills.model.IDelayedDependency;
import org.eclipse.skills.model.ISkillDependency;
import org.eclipse.skills.model.ITaskDependency;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;

public class DependencyControlTreeViewerBuilder implements TreeViewerBuilder {

	private final String fColumnTitle;

	public DependencyControlTreeViewerBuilder(String columnTitle) {
		fColumnTitle = columnTitle;
	}

	@Override
	public TreeViewer createTree(Composite parent) {

		final Composite composite = new Composite(parent, SWT.NONE);
		final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gridData.minimumHeight = 160;
		composite.setLayoutData(gridData);

		final TreeColumnLayout tcl_composite = new TreeColumnLayout();
		composite.setLayout(tcl_composite);

		final TreeViewer treeViewer = new TreeViewer(composite, SWT.BORDER);
		final Tree tree = treeViewer.getTree();
		treeViewer.setAutoExpandLevel(3);
		tree.setHeaderVisible(true);

		final TreeViewerColumn treeViewerColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		final TreeColumn trclmnTitle = treeViewerColumn.getColumn();
		tcl_composite.setColumnData(trclmnTitle, new ColumnWeightData(1, ColumnWeightData.MINIMUM_WIDTH, true));
		trclmnTitle.setText(fColumnTitle);

		treeViewer.addDoubleClickListener(event -> {
			final Object element = treeViewer.getStructuredSelection().getFirstElement();
			if ((element instanceof ICustomDependency) || (element instanceof ITaskDependency) || (element instanceof ISkillDependency)
					|| (element instanceof IDelayedDependency)) {
				final DetailDialog dialog = new DetailDialog(treeViewer.getTree().getShell(), (EObject) element, null, null);
				if (dialog.open() == Window.OK)
					treeViewer.refresh();
			}
		});

		return treeViewer;
	}
}
