/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.questeditor.renderers.dependencytree;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecp.common.spi.ChildrenDescriptorCollector;
import org.eclipse.emf.ecp.view.spi.table.swt.DetailDialog;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.DeleteCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emfforms.internal.swt.treemasterdetail.defaultprovider.DefaultDeleteActionBuilder;
import org.eclipse.emfforms.spi.swt.treemasterdetail.MenuProvider;
import org.eclipse.emfforms.spi.swt.treemasterdetail.util.CreateChildAction;
import org.eclipse.emfforms.spi.swt.treemasterdetail.util.CreateElementCallback;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.skills.model.IContainedDependency;
import org.eclipse.skills.model.ICustomDependency;
import org.eclipse.skills.model.IDependency;
import org.eclipse.skills.ui.questeditor.actions.ReplaceElementAction;
import org.eclipse.skills.ui.questeditor.actions.WrapElementAction;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TreeItem;

public class DependencyControlMenuProvider implements MenuProvider {

	private final Object fInput;

	public DependencyControlMenuProvider(Object input) {
		fInput = input;
	}

	@Override
	public Menu getMenu(TreeViewer treeViewer, EditingDomain editingDomain) {

		// FIXME only way to register keys on the tree that need the editing domain
		treeViewer.getTree().addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (SWT.DEL == e.character) {
					final ITreeSelection structuredSelection = treeViewer.getStructuredSelection();
					final Command deleteCommand = DeleteCommand.create(editingDomain, structuredSelection.toList());
					editingDomain.getCommandStack().execute(deleteCommand);
				}
			}
		});

		final MenuManager menuMgr = new MenuManager();

		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new Listener(menuMgr, treeViewer, editingDomain));

		return menuMgr.createContextMenu(treeViewer.getControl());
	}

	private class Listener implements IMenuListener {

		private final MenuManager fMenuMgr;
		private final TreeViewer fTreeViewer;
		private final EditingDomain fEditingDomain;

		public Listener(MenuManager menuMgr, TreeViewer treeViewer, EditingDomain editingDomain) {
			fMenuMgr = menuMgr;
			fTreeViewer = treeViewer;
			fEditingDomain = editingDomain;
		}

		@Override
		public void menuAboutToShow(IMenuManager manager) {
			final Point cursorLocation = Display.getDefault().getCursorLocation();

			final Point relativeLocation = fTreeViewer.getTree().toControl(cursorLocation);
			final TreeItem itemUnderCursor = fTreeViewer.getTree().getItem(relativeLocation);
			final Object element = (itemUnderCursor != null) ? itemUnderCursor.getData() : null;

			final boolean noElement = element == null;
			final boolean isContainer = (element instanceof IContainedDependency);

			if (noElement || isContainer) {
				// Add... submenu
				final IContainedDependency selectedDependency = (IContainedDependency) (noElement ? fInput : element);

				final MenuManager addMenu = new MenuManager("Add child...", null, null);
				addNewActions(addMenu, fEditingDomain, selectedDependency);
				fMenuMgr.add(addMenu);
			}

			if (!noElement) {
				// Wrap in... submenu

				final MenuManager wrapMenu = new MenuManager("Wrap in...", null, null);
				addWrapActions(wrapMenu, fEditingDomain, (EObject) element);
				fMenuMgr.add(wrapMenu);
			}

			if (isContainer) {
				// Convert to... submenu

				final MenuManager wrapMenu = new MenuManager("Convert to...", null, null);
				addConvertActions(wrapMenu, fEditingDomain, (EObject) element);
				fMenuMgr.add(wrapMenu);
			}

			if (element instanceof IDependency) {
				manager.add(new Separator());
				addDeleteAction(fEditingDomain, fMenuMgr, fTreeViewer.getStructuredSelection());
			}
		}

		private List<CommandParameter> getDescriptors(EObject eObject, Class<?> clazz) {
			final Collection<?> descriptors = new ChildrenDescriptorCollector().getDescriptors(eObject);

			return descriptors.stream().filter(d -> d instanceof CommandParameter).map(d -> (CommandParameter) d).filter(cp -> cp.getEReference() != null)
					.filter(cp -> !(!cp.getEReference().isMany() && eObject.eIsSet(cp.getEStructuralFeature())))
					.filter(cp -> !(cp.getEReference().isMany() && (cp.getEReference().getUpperBound() != -1)
							&& (cp.getEReference().getUpperBound() <= ((List<?>) eObject.eGet(cp.getEReference())).size())))
					.filter(cp -> clazz.isInstance(cp.getValue())).collect(Collectors.toList());
		}

		private void addNewActions(IMenuManager manager, final EditingDomain domain, final EObject eObject) {
			for (final CommandParameter commandParameter : getDescriptors(eObject, IDependency.class))
				manager.add(createCreateChildAction(domain, eObject, commandParameter));
		}

		private void addWrapActions(IMenuManager manager, final EditingDomain domain, final EObject elementToConvert) {
			final EObject parent = elementToConvert.eContainer();
			for (final CommandParameter commandParameter : getDescriptors(elementToConvert.eContainer(), IContainedDependency.class))
				manager.add(createWrapElementAction(domain, parent, elementToConvert, commandParameter.getEValue()));
		}

		private void addConvertActions(IMenuManager manager, final EditingDomain domain, final EObject elementToConvert) {
			final EObject parent = elementToConvert.eContainer();

			for (final CommandParameter commandParameter : getDescriptors(elementToConvert.eContainer(), IContainedDependency.class)) {
				if (elementToConvert.getClass().equals(commandParameter.getEValue().getClass()))
					// do not allow to convert to the exact same type
					continue;

				manager.add(createConvertElementAction(domain, parent, elementToConvert, commandParameter.getEValue()));
			}
		}

		private void addDeleteAction(final EditingDomain editingDomain, final IMenuManager manager, final IStructuredSelection selection) {
			final Action deleteAction = new DefaultDeleteActionBuilder().createDeleteAction(selection, editingDomain);
			manager.add(deleteAction);
		}

		protected IAction createCreateChildAction(final EditingDomain domain, final EObject eObject, final CommandParameter commandParameter) {
			CreateElementCallback callback = null;
			if (commandParameter.getEValue() instanceof ICustomDependency) {
				callback = new CreateElementCallback() {

					@Override
					public void initElement(EObject parent, EReference reference, EObject newObject) {
					}

					@Override
					public boolean beforeCreateElement(Object newElement) {
						return true;
					}

					@Override
					public void afterCreateElement(Object newElement) {
						Display.getDefault()
								.asyncExec(() -> new DetailDialog(Display.getDefault().getActiveShell(), commandParameter.getEValue(), null, null).open());
					}
				};
			}

			final CreateChildAction action = new CreateChildAction(eObject, domain, fTreeViewer, commandParameter, callback);
			action.configureAction(new StructuredSelection(eObject));

			return action;
		}

		private IAction createWrapElementAction(EditingDomain domain, EObject owner, EObject elementToConvert, EObject newElement) {
			return new WrapElementAction(owner, elementToConvert, newElement, domain, fTreeViewer);
		}

		private IAction createConvertElementAction(EditingDomain domain, EObject owner, EObject elementToConvert, EObject newElement) {
			return new ReplaceElementAction(owner, elementToConvert, newElement, domain, fTreeViewer);
		}
	}
}