/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.questeditor.actions;

import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.CreateChildCommand;
import org.eclipse.emf.edit.command.ReplaceCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.skills.model.provider.SkillsItemProviderAdapterFactory;
import org.eclipse.swt.graphics.Image;

/**
 * A child creation action is implemented by creating a {@link CreateChildCommand}.
 */
public class WrapElementAction extends Action {

	protected static EStructuralFeature getDependenciesFeature(EObject eObject) {
		return eObject.eClass().getEStructuralFeature("dependencies");
	}

	protected final EObject fOwner;
	protected final EObject fOldElement;
	protected final EObject fNewElement;
	protected final EditingDomain fEditingDomain;
	private final TreeViewer fViewer;
	private final Command fCommand;

	public WrapElementAction(EObject owner, EObject oldElement, EObject newElement, EditingDomain editingDomain, TreeViewer viewer) {
		fOwner = owner;
		fOldElement = oldElement;
		fNewElement = newElement;
		fEditingDomain = editingDomain;
		fViewer = viewer;

		fCommand = createCommand();

		configureAction();
	}

	protected void configureAction() {

		final IItemLabelProvider labelProvider = getLabelProvider(fNewElement);
		if (labelProvider != null) {
			setText(labelProvider.getText(fNewElement));

			final Object image = labelProvider.getImage(fNewElement);
			if (image instanceof Image)
				setImageDescriptor(ImageDescriptor.createFromImage((Image) image));
			else if (image instanceof URL)
				setImageDescriptor(ImageDescriptor.createFromURL((URL) image));

		} else
			setText(fNewElement.getClass().getSimpleName());

		setToolTipText(createToolTipText());

		setEnabled(fCommand.canExecute());
	}

	private IItemLabelProvider getLabelProvider(EObject object) {
		final SkillsItemProviderAdapterFactory factory = new SkillsItemProviderAdapterFactory();
		if (factory.isFactoryForType(IItemLabelProvider.class))
			return (IItemLabelProvider) factory.adapt(fNewElement, IItemLabelProvider.class);

		return null;
	}

	protected Command createCommand() {

		final CompoundCommand compoundCommand = new CompoundCommand();

		final ReplaceCommand replaceCommand = new ReplaceCommand(fEditingDomain, fOwner, getDependenciesFeature(fOwner), fOldElement, fNewElement);
		compoundCommand.append(replaceCommand);

		final AddCommand addCommand = new AddCommand(fEditingDomain, fNewElement, getDependenciesFeature(fNewElement), fOldElement);
		compoundCommand.append(addCommand);

		return compoundCommand;
	}

	@Override
	public void run() {
		final Collection<Object> expandedElements = new HashSet<>();
		if (fViewer != null)
			expandedElements.addAll(Arrays.asList(fViewer.getExpandedElements()));

		fEditingDomain.getCommandStack().execute(fCommand);

		if (fViewer != null) {
			fViewer.refresh();

			expandedElements.remove(fOldElement);
			expandedElements.add(fNewElement);
			fViewer.setExpandedElements(expandedElements.toArray());

			fViewer.setSelection(new StructuredSelection(fNewElement));
		}
	}

	protected String createToolTipText() {
		return "Wrap the current dependency with \"" + getText() + "\"";
	}
}
