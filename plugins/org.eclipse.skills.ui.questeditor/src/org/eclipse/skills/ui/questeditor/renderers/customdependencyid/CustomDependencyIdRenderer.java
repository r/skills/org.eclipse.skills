/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.questeditor.renderers.customdependencyid;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.IConverter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.EMFProperties;
import org.eclipse.emf.databinding.IEMFObservable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.core.swt.SimpleControlSWTControlSWTRenderer;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.template.model.VTViewTemplateProvider;
import org.eclipse.emfforms.spi.common.report.ReportService;
import org.eclipse.emfforms.spi.core.services.databinding.DatabindingFailedException;
import org.eclipse.emfforms.spi.core.services.databinding.EMFFormsDatabinding;
import org.eclipse.emfforms.spi.core.services.editsupport.EMFFormsEditSupport;
import org.eclipse.emfforms.spi.core.services.label.EMFFormsLabelProvider;
import org.eclipse.jface.databinding.swt.ISWTObservableValue;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.skills.dependencies.CustomDependencyDefinition;
import org.eclipse.skills.model.ICustomDependency;
import org.eclipse.skills.model.ISkillsPackage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/**
 * A renderer to render task dependencies/goals.
 */
public class CustomDependencyIdRenderer extends SimpleControlSWTControlSWTRenderer {

	/**
	 * Default constructor.
	 *
	 * @param vElement
	 *            the view model element to be rendered
	 * @param viewContext
	 *            the view context
	 * @param reportService
	 *            The {@link ReportService}
	 * @param emfFormsDatabinding
	 *            The {@link EMFFormsDatabinding}
	 * @param emfFormsLabelProvider
	 *            The {@link EMFFormsLabelProvider}
	 * @param vtViewTemplateProvider
	 *            The {@link VTViewTemplateProvider}
	 * @param emfFormsEditSupport
	 *            The {@link EMFFormsEditSupport}
	 */
	@Inject
	public CustomDependencyIdRenderer(VControl vElement, ViewModelContext viewContext, ReportService reportService, EMFFormsDatabinding emfFormsDatabinding,
			EMFFormsLabelProvider emfFormsLabelProvider, VTViewTemplateProvider vtViewTemplateProvider, EMFFormsEditSupport emfFormsEditSupport) {
		super(vElement, viewContext, reportService, emfFormsDatabinding, emfFormsLabelProvider, vtViewTemplateProvider);
	}

	@Override
	protected Control createSWTControl(Composite parent) {
		final ComboViewer comboViewer = new ComboViewer(parent, SWT.NONE);

		comboViewer.setContentProvider(ArrayContentProvider.getInstance());
		comboViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof CustomDependencyDefinition)
					return ((CustomDependencyDefinition) element).getName();

				return super.getText(element);
			}

			@Override
			public Image getImage(Object element) {
				if (element instanceof CustomDependencyDefinition) {
					final ImageDescriptor descriptor = ((CustomDependencyDefinition) element).getImageDescriptor();
					if (descriptor != null)
						// FIXME add resource handling
						return descriptor.createImage();
				}

				return super.getImage(element);
			}

		});

		comboViewer.setComparator(new ViewerComparator() {
			@Override
			public int compare(Viewer viewer, Object e1, Object e2) {
				return ((LabelProvider) comboViewer.getLabelProvider()).getText(e1).compareTo(((LabelProvider) comboViewer.getLabelProvider()).getText(e2));
			}
		});

		comboViewer.setInput(CustomDependencyDefinition.getDefinedDefinitions());

		return comboViewer.getControl();
	}

	private ICustomDependency getInput() {

		try {
			final IEMFObservable emfObservable = (IEMFObservable) getModelValue();
			final EObject domainObject = (EObject) emfObservable.getObserved(); // MTask

			return (ICustomDependency) domainObject;

		} catch (final DatabindingFailedException e) {
			// TODO handle this exception (but for now, at least know it happened)
			throw new RuntimeException(e);
		}
	}

	@Override
	protected String getUnsetText() {
		return "Please select";
	}

	@Override
	protected Binding[] createBindings(Control control) throws DatabindingFailedException {
		final DataBindingContext bindingContext = getDataBindingContext();

		final ISWTObservableValue<String> controlBinding = WidgetProperties.comboSelection().observe((Combo) control);
		final IObservableValue modelBinding = EMFProperties.value(ISkillsPackage.Literals.CUSTOM_DEPENDENCY__EXTENSION_ID).observe(getInput());

		final IConverter<String, String> nameToId = IConverter.create(String.class, String.class, s -> CustomDependencyDefinition.getDefinitionFor(s).getId());

		final IConverter<String, String> idToName = IConverter.create(String.class, String.class,
				s -> CustomDependencyDefinition.getDefinitionFor(s).getName());

		bindingContext.bindValue(controlBinding, modelBinding, UpdateValueStrategy.create(nameToId), UpdateValueStrategy.create(idToName));

		return bindingContext.getBindings().toArray(new Binding[0]);
	}
}