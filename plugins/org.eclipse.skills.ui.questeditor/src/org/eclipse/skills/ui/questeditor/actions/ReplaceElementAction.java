/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.questeditor.actions;

import java.util.Collection;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.ReplaceCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.viewers.TreeViewer;

public class ReplaceElementAction extends WrapElementAction {

	public ReplaceElementAction(EObject owner, EObject oldElement, EObject newElement, EditingDomain editingDomain, TreeViewer viewer) {
		super(owner, oldElement, newElement, editingDomain, viewer);
	}

	@Override
	protected Command createCommand() {

		final CompoundCommand compoundCommand = new CompoundCommand();

		final Object childDependencies = fOldElement.eGet(getDependenciesFeature(fOldElement));
		if ((childDependencies instanceof Collection<?>) && (!((Collection<?>) childDependencies).isEmpty())) {
			final AddCommand addCommand = new AddCommand(fEditingDomain, fNewElement, getDependenciesFeature(fNewElement), (Collection<?>) childDependencies);
			compoundCommand.append(addCommand);
		}

		final ReplaceCommand replaceCommand = new ReplaceCommand(fEditingDomain, fOwner, getDependenciesFeature(fOwner), fOldElement, fNewElement);
		compoundCommand.append(replaceCommand);

		return compoundCommand;
	}

	@Override
	protected String createToolTipText() {
		return "Replace the current dependency with \"" + getText() + "\"";
	}
}
