/*******************************************************************************
 * Copyright (c) 2020 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.skills.ui.questeditor.renderers.customdependencyid;

import org.eclipse.core.databinding.property.value.IValueProperty;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.spi.model.VElement;
import org.eclipse.emfforms.spi.common.report.ReportService;
import org.eclipse.emfforms.spi.core.services.databinding.DatabindingFailedException;
import org.eclipse.emfforms.spi.core.services.databinding.DatabindingFailedReport;
import org.eclipse.emfforms.spi.core.services.databinding.EMFFormsDatabinding;
import org.eclipse.emfforms.spi.swt.core.AbstractSWTRenderer;
import org.eclipse.emfforms.spi.swt.core.di.EMFFormsDIRendererService;
import org.eclipse.skills.model.ISkillsPackage;

public class CustomDependencyIdRendererService implements EMFFormsDIRendererService<VControl> {

	private EMFFormsDatabinding fDatabindingService;
	private ReportService fReportService;

	public CustomDependencyIdRendererService() {
	}

	/**
	 * Called by the initializer to set the EMFFormsDatabinding.
	 */
	protected void setEMFFormsDatabinding(EMFFormsDatabinding databindingService) {
		fDatabindingService = databindingService;
	}

	/**
	 * Called by the initializer to set the ReportService.
	 */
	protected void setReportService(ReportService reportService) {
		fReportService = reportService;
	}

	@Override
	public double isApplicable(VElement vElement, ViewModelContext viewModelContext) {
		if (!VControl.class.isInstance(vElement))
			return NOT_APPLICABLE;

		final VControl control = (VControl) vElement;
		if (control.getDomainModelReference() == null)
			return NOT_APPLICABLE;

		@SuppressWarnings("rawtypes")
		IValueProperty valueProperty;
		try {
			valueProperty = fDatabindingService.getValueProperty(control.getDomainModelReference(), viewModelContext.getDomainModel());
		} catch (final DatabindingFailedException ex) {
			fReportService.report(new DatabindingFailedReport(ex));
			return NOT_APPLICABLE;
		}

		final EStructuralFeature eStructuralFeature = EStructuralFeature.class.cast(valueProperty.getValueType());
		if (ISkillsPackage.eINSTANCE.getCustomDependency_ExtensionId().equals(eStructuralFeature))
			return 1000;

		return NOT_APPLICABLE;
	}

	@Override
	public Class<? extends AbstractSWTRenderer<VControl>> getRendererClass() {
		return CustomDependencyIdRenderer.class;
	}
}
