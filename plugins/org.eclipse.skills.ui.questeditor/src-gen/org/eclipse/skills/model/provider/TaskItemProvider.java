/**
 */
package org.eclipse.skills.model.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.skills.model.ISkillsFactory;
import org.eclipse.skills.model.ISkillsPackage;
import org.eclipse.skills.model.ITask;

/**
 * This is the item provider adapter for a {@link org.eclipse.skills.model.ITask} object.
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class TaskItemProvider extends ItemProviderAdapter
		implements IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TaskItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTitlePropertyDescriptor(object);
			addTasksPropertyDescriptor(object);
			addAutoActivationPropertyDescriptor(object);
			addSkillServicePropertyDescriptor(object);
			addIdPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Title feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTitlePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Task_title_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Task_title_feature", "_UI_Task_type"),
				 ISkillsPackage.Literals.TASK__TITLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Tasks feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTasksPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Task_tasks_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Task_tasks_feature", "_UI_Task_type"),
				 ISkillsPackage.Literals.TASK__TASKS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Auto Activation feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAutoActivationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Task_autoActivation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Task_autoActivation_feature", "_UI_Task_type"),
				 ISkillsPackage.Literals.TASK__AUTO_ACTIVATION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Skill Service feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSkillServicePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Task_skillService_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Task_skillService_feature", "_UI_Task_type"),
				 ISkillsPackage.Literals.TASK__SKILL_SERVICE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Task_id_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Task_id_feature", "_UI_Task_type"),
				 ISkillsPackage.Literals.TASK__ID,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an {@link org.eclipse.emf.edit.command.AddCommand},
	 * {@link org.eclipse.emf.edit.command.RemoveCommand} or {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ISkillsPackage.Literals.TASK__TASKS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Task.gif. <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Task.png"));
	}

	/**
	 * @generated NOT
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		final String label = ((ITask) object).getTitle();
		return (label == null) || (label.length() == 0) ? getString("_UI_Task_type") : label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ITask.class)) {
			case ISkillsPackage.TASK__TITLE:
			case ISkillsPackage.TASK__AUTO_ACTIVATION:
			case ISkillsPackage.TASK__SKILL_SERVICE:
			case ISkillsPackage.TASK__ID:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ISkillsPackage.TASK__DESCRIPTION:
			case ISkillsPackage.TASK__GOAL:
			case ISkillsPackage.TASK__REWARDS:
			case ISkillsPackage.TASK__REQUIREMENT:
			case ISkillsPackage.TASK__TASKS:
			case ISkillsPackage.TASK__HINTS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children that can be created under this object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__DESCRIPTION, ISkillsFactory.eINSTANCE.createDescription()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__DESCRIPTION, ISkillsFactory.eINSTANCE.createHint()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__GOAL, ISkillsFactory.eINSTANCE.createAndDependency()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__GOAL, ISkillsFactory.eINSTANCE.createOrDependency()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__GOAL, ISkillsFactory.eINSTANCE.createNotDependency()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__GOAL, ISkillsFactory.eINSTANCE.createDelayedDependency()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__GOAL, ISkillsFactory.eINSTANCE.createCustomDependency()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__GOAL, ISkillsFactory.eINSTANCE.createSequenceDependency()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__GOAL, ISkillsFactory.eINSTANCE.createCompleteIncludedTasksDependency()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__GOAL, ISkillsFactory.eINSTANCE.createTaskDependency()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__GOAL, ISkillsFactory.eINSTANCE.createSkillDependency()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__REWARDS, ISkillsFactory.eINSTANCE.createBadgeReward()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__REWARDS, ISkillsFactory.eINSTANCE.createExperienceReward()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__REWARDS, ISkillsFactory.eINSTANCE.createSkillReward()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__REQUIREMENT, ISkillsFactory.eINSTANCE.createAndDependency()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__REQUIREMENT, ISkillsFactory.eINSTANCE.createOrDependency()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__REQUIREMENT, ISkillsFactory.eINSTANCE.createNotDependency()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__REQUIREMENT, ISkillsFactory.eINSTANCE.createDelayedDependency()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__REQUIREMENT, ISkillsFactory.eINSTANCE.createCustomDependency()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__REQUIREMENT, ISkillsFactory.eINSTANCE.createSequenceDependency()));

		newChildDescriptors
				.add(createChildParameter(ISkillsPackage.Literals.TASK__REQUIREMENT, ISkillsFactory.eINSTANCE.createCompleteIncludedTasksDependency()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__REQUIREMENT, ISkillsFactory.eINSTANCE.createTaskDependency()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__REQUIREMENT, ISkillsFactory.eINSTANCE.createSkillDependency()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__TASKS, createTask()));

		newChildDescriptors.add(createChildParameter(ISkillsPackage.Literals.TASK__HINTS, ISkillsFactory.eINSTANCE.createHint()));
	}

	/**
	 * @generated NOT
	 */
	private ITask createTask() {
		final ITask task = ISkillsFactory.eINSTANCE.createTask();
		task.setDescription(ISkillsFactory.eINSTANCE.createDescription());

		task.setRequirement(ISkillsFactory.eINSTANCE.createAndDependency());
		task.setGoal(ISkillsFactory.eINSTANCE.createAndDependency());

		return task;
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ISkillsPackage.Literals.TASK__DESCRIPTION ||
			childFeature == ISkillsPackage.Literals.TASK__HINTS ||
			childFeature == ISkillsPackage.Literals.TASK__GOAL ||
			childFeature == ISkillsPackage.Literals.TASK__REQUIREMENT;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return SkillsEditPlugin.INSTANCE;
	}

}
