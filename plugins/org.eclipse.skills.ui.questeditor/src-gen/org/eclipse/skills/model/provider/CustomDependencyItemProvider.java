/**
 */
package org.eclipse.skills.model.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.skills.model.ICustomDependency;
import org.eclipse.skills.model.IDependency;
import org.eclipse.skills.model.ISkillsFactory;
import org.eclipse.skills.model.ISkillsPackage;

/**
 * This is the item provider adapter for a {@link org.eclipse.skills.model.ICustomDependency} object.
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class CustomDependencyItemProvider extends DependencyItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CustomDependencyItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addExtensionIdPropertyDescriptor(object);
			addExtensionAttributePropertyDescriptor(object);
			addDependencyPropertyDescriptor(object);
			addDefinitionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Extension Id feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExtensionIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CustomDependency_extensionId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CustomDependency_extensionId_feature", "_UI_CustomDependency_type"),
				 ISkillsPackage.Literals.CUSTOM_DEPENDENCY__EXTENSION_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Extension Attribute feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExtensionAttributePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CustomDependency_extensionAttribute_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CustomDependency_extensionAttribute_feature", "_UI_CustomDependency_type"),
				 ISkillsPackage.Literals.CUSTOM_DEPENDENCY__EXTENSION_ATTRIBUTE,
				 true,
				 true,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Dependency feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDependencyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CustomDependency_dependency_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CustomDependency_dependency_feature", "_UI_CustomDependency_type"),
				 ISkillsPackage.Literals.CUSTOM_DEPENDENCY__DEPENDENCY,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Definition feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDefinitionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CustomDependency_definition_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CustomDependency_definition_feature", "_UI_CustomDependency_type"),
				 ISkillsPackage.Literals.CUSTOM_DEPENDENCY__DEFINITION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an {@link org.eclipse.emf.edit.command.AddCommand},
	 * {@link org.eclipse.emf.edit.command.RemoveCommand} or {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns CustomDependency.gif. <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		if (object instanceof ICustomDependency) {
			if (((ICustomDependency) object).getDefinition() != null) {
				final ImageDescriptor imageDescriptor = ((ICustomDependency) object).getDefinition().getImageDescriptor();
				if (imageDescriptor != null)
					// FIXME add resource handling
					return overlayImage(object, imageDescriptor.createImage());
			}
		}

		return overlayImage(object, getResourceLocator().getImage("full/obj16/CustomDependency.png"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		if (object instanceof ICustomDependency) {
			final IDependency dependency = ((ICustomDependency) object).getDependency();
			if (dependency != null)
				return dependency.toString();

			return "Invalid ID: " + ((ICustomDependency) object).getExtensionId();
		}

		final ICustomDependency customDependency = (ICustomDependency) object;
		return getString("_UI_CustomDependency_type") + " " + customDependency.isFulfilled();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ICustomDependency.class)) {
			case ISkillsPackage.CUSTOM_DEPENDENCY__EXTENSION_ID:
			case ISkillsPackage.CUSTOM_DEPENDENCY__EXTENSION_ATTRIBUTE:
			case ISkillsPackage.CUSTOM_DEPENDENCY__DEFINITION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ISkillsPackage.CUSTOM_DEPENDENCY__DEPENDENCY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CUSTOM_DEPENDENCY__DEPENDENCY,
				 ISkillsFactory.eINSTANCE.createAndDependency()));

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CUSTOM_DEPENDENCY__DEPENDENCY,
				 ISkillsFactory.eINSTANCE.createOrDependency()));

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CUSTOM_DEPENDENCY__DEPENDENCY,
				 ISkillsFactory.eINSTANCE.createNotDependency()));

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CUSTOM_DEPENDENCY__DEPENDENCY,
				 ISkillsFactory.eINSTANCE.createDelayedDependency()));

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CUSTOM_DEPENDENCY__DEPENDENCY,
				 ISkillsFactory.eINSTANCE.createCustomDependency()));

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CUSTOM_DEPENDENCY__DEPENDENCY,
				 ISkillsFactory.eINSTANCE.createSequenceDependency()));

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CUSTOM_DEPENDENCY__DEPENDENCY,
				 ISkillsFactory.eINSTANCE.createCompleteIncludedTasksDependency()));

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CUSTOM_DEPENDENCY__DEPENDENCY,
				 ISkillsFactory.eINSTANCE.createTaskDependency()));

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CUSTOM_DEPENDENCY__DEPENDENCY,
				 ISkillsFactory.eINSTANCE.createSkillDependency()));
	}

}
