/**
 */
package org.eclipse.skills.model.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.skills.model.IContainedDependency;
import org.eclipse.skills.model.ISkillsFactory;
import org.eclipse.skills.model.ISkillsPackage;

/**
 * This is the item provider adapter for a {@link org.eclipse.skills.model.IContainedDependency} object.
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class ContainedDependencyItemProvider extends DependencyItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ContainedDependencyItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addDependenciesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Dependencies feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDependenciesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ContainedDependency_dependencies_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ContainedDependency_dependencies_feature", "_UI_ContainedDependency_type"),
				 ISkillsPackage.Literals.CONTAINED_DEPENDENCY__DEPENDENCIES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ISkillsPackage.Literals.CONTAINED_DEPENDENCY__DEPENDENCIES);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ContainedDependency.gif.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ContainedDependency"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		IContainedDependency containedDependency = (IContainedDependency)object;
		return getString("_UI_ContainedDependency_type") + " " + containedDependency.isFulfilled();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(IContainedDependency.class)) {
			case ISkillsPackage.CONTAINED_DEPENDENCY__DEPENDENCIES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CONTAINED_DEPENDENCY__DEPENDENCIES,
				 ISkillsFactory.eINSTANCE.createAndDependency()));

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CONTAINED_DEPENDENCY__DEPENDENCIES,
				 ISkillsFactory.eINSTANCE.createOrDependency()));

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CONTAINED_DEPENDENCY__DEPENDENCIES,
				 ISkillsFactory.eINSTANCE.createNotDependency()));

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CONTAINED_DEPENDENCY__DEPENDENCIES,
				 ISkillsFactory.eINSTANCE.createDelayedDependency()));

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CONTAINED_DEPENDENCY__DEPENDENCIES,
				 ISkillsFactory.eINSTANCE.createCustomDependency()));

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CONTAINED_DEPENDENCY__DEPENDENCIES,
				 ISkillsFactory.eINSTANCE.createSequenceDependency()));

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CONTAINED_DEPENDENCY__DEPENDENCIES,
				 ISkillsFactory.eINSTANCE.createCompleteIncludedTasksDependency()));

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CONTAINED_DEPENDENCY__DEPENDENCIES,
				 ISkillsFactory.eINSTANCE.createTaskDependency()));

		newChildDescriptors.add
			(createChildParameter
				(ISkillsPackage.Literals.CONTAINED_DEPENDENCY__DEPENDENCIES,
				 ISkillsFactory.eINSTANCE.createSkillDependency()));
	}

}
